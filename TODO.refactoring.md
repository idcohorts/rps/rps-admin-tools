## step 1
- [X] Bridge base class
- [X] General Sync Loops in Bridge-Base Class
- [X] Flatten structure of rps_sync_services package (only one folder with everything)
- [X] Move mailsender in seperate Python package rps_mail_sender
- [X] remove Budibase code
- [X] remove Alerta Bridge

## step 2
- [ ] Move sync functions in respective bridge classes
  - [x] for Nextcloud @Elias
  - [ ] for Matrix @Vasily
  - [ ] for Discourse @Tom
  - [ ] for Permission Sync @Elias

## step 3
- [ ] Make it work and test on RPS test servers

## step 4
- [ ] Tests
- [ ] Add Alerta Monitoring to Sync Class
- [ ] Mailsender themable with RPS default theme
