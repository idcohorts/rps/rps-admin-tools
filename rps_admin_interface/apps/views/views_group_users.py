from flask import current_app, Blueprint, request, render_template, url_for, send_file, Response
import pandas as pd
import io
import tempfile

gu_bp = Blueprint(
    'group_users_blueprint',
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@gu_bp.route('/list-group-users', methods=['GET','POST'])
def index():
    if request.method == 'GET':
        keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]
        groups = keycloak_bridge.get_groups(ignore_members=True)

        return render_template('home/list-group-users.html', 
                        title="List Groups' Users",
                        groups=groups)

@gu_bp.route('/api-list-group-users', methods=['POST'])
def api_list_group_users():
    if request.method == 'POST':
        data = request.get_json()
        # For showing in interface so the admin can check the users that are to be imported
        keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]

        index_dict = {}
        index = 0
        group_members = []

        # Get all users in groups
        for groupname in data["groups"]:
            group = keycloak_bridge.get_group_by_name(groupname)
            for member in group.members:
                if member not in group_members:
                    member.add_group(groupname)
                    group_members.append(member)
                    
                    index_dict[member.username] = index
                    index += 1
                else:
                    # some members can be at other groups selected
                    group_members[index_dict[member.username]].add_group(groupname)

        # Change groups member to dictionary instead of User Object
        group_members = [dict(member) for member in group_members]

        ## Context
        # Flex table headers
        flex_table_headers = {
            "email": "Email",
            "username": "Username",
            "keycloak_id": "ID",
            "first_name": "First Name",
            "last_name": "Last Name",
            "groups": "Groups"
        }

        download_id = "download-group-users"
        # api_download_group_users
        api_download = url_for('group_users_blueprint.api_download_group_users')

        context = {
            "html": render_template('includes/flex-table.html',
                            table_id="group-users-table",
                            download_id=download_id,
                            api_download=api_download,
                            flex_table_headers=flex_table_headers,
                            users=group_members),
            "flex_table_headers": flex_table_headers,
            "data": group_members
        }

        return context
    
@gu_bp.route('/api-download-group-users', methods=['POST'])
def api_download_group_users():
    if request.method == 'POST':
        data = request.get_json()

        df = pd.DataFrame(data["data"])

        excel_data = io.BytesIO()
        writer = pd.ExcelWriter(excel_data, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Sheet1')
        writer.save()
        excel_data.seek(0)

        # Set the Content-Type header to the appropriate MIME type
        EXCELMIME="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response = send_file(excel_data, download_name='data.xlsx', as_attachment=True, 
                         mimetype=EXCELMIME)
        # Return the Excel file to the user for download
        return response
    

