from flask import Blueprint, current_app, render_template

home_bp = Blueprint(
    'home_blueprint', 
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@home_bp.route('/')
def index():
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]

    version = current_app.config["VERSION"]

    registered_users = keycloak_bridge.get_new_users()
    
    users = []
    for user in registered_users:
        if not "approved" in user.roles:
            if user.enabled:
                users.append(user)
            
    pending = len(users)
    
    keycloak_url = current_app.config["KEYCLOAK_URL"]
    keycloak_realm = current_app.config["KEYCLOAK_REALM"]
    keycloak_console_url = f"{keycloak_url}/admin/{keycloak_realm}/console"
    
    return render_template('home/index.html', pending=pending, version=version, keycloak_console_url=keycloak_console_url)
