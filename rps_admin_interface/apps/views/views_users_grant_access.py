from flask import current_app, Blueprint, request, redirect, jsonify, render_template, url_for, flash
from pathlib import Path

from ..scripts.review_users import enable_user, disable_user

uga_bp = Blueprint(
    'users_grant_access',
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@uga_bp.route('/review-user-access', methods=['GET', 'POST'])
def index():
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]
    approved_users = keycloak_bridge.get_approved_users()
    # portal_roles =[
    #     "Forschungsportal",
    #     "Infrastrukturportal",
    #     "Drittmittelverwaltung",
    #     "LoKs-Portal",
    #     "Gremienportal",
    #     "NUM-Portal",
    #     "Koordinierungsstelle",
    #     "Development"
    # ]

    portal_roles = keycloak_bridge.get_all_roles()

    approved_users = sorted(approved_users, key=lambda user: user.timestamp, reverse=True)

    print(approved_users)

    if request.method == 'GET':
        return render_template('home/view-user-access.html',
                               title="Grant User Access",
                               approved_users=approved_users,
                               portal_roles=portal_roles)
    
@uga_bp.route('/grant-user-access', methods=['POST'])
def api_enable_user():
    if request.method == "POST":
        user_dict = request.get_json()

        user_obj = enable_user(user_dict)

        # Send email to user
        if user_obj:
            flash("User enabled {}".format(user_obj.username), 'success')

        return jsonify({"message": "success"})
    
@uga_bp.route('/revoke-user-access', methods=['POST'])
def api_disable_user():
    if request.method == "POST":
        user_dict = request.get_json()

        user_obj = disable_user(user_dict)
        if user_obj:
            flash("User registration removed {}".format(user_obj.username), 'success')
            
        return jsonify({"message": "success"})