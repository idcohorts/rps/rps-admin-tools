from flask import flash,redirect, current_app, Blueprint, request, jsonify, render_template, send_from_directory, send_file, Response
from pathlib import Path
from rps_admin_cli.tools import export_users
import os
from ..scripts.import_users import read_imported_users, import_users
from pathlib import Path
from datetime import datetime
from threading import Thread



ur_bp = Blueprint(
    'users_export_blueprint', 
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@ur_bp.route('/export-users', methods=['GET'])
def index():
    export_running = export_thread.is_alive()
    timestamp = get_export_file('./exports')
    if request.method == 'GET':
        return render_template('home/export-users.html', 
                        title="Export Users",
                        timestamp=timestamp,
                        export_running=export_running)

@ur_bp.route("/generate-user-export" , methods = ["POST"] ) 
def gen_export():
        
        run_export_on_thread()
        return redirect("/export-users")


@ur_bp.route("/download-user-export" , methods = ["GET"] ) 
def download_export():
        csv = load_export('./exports/export.csv')
        return Response(
                csv,
                mimetype="text/csv",
                headers={"Content-disposition":
                        "attachment; filename=export.csv"})

def generate_export():
    csv = export_users(verbose = False, as_string = True) 
    save_export('./exports/export.csv', csv)

export_thread:Thread = Thread(target=generate_export, name='export-gen-thread')
def run_export_on_thread():
    global export_thread
    if export_thread.is_alive():
        return
    export_thread = Thread(target=generate_export, name='export-gen-thread')
    export_thread.start()




def check_folder(path:str):
    folder = Path(path)
    if not folder.exists():
        return False
    
    if not folder.is_dir():
        return False
    
    return True
    

def get_export_file(path:str):
    folder = Path(path)
    if not check_folder(path):
        folder.mkdir()
        return []
    
    file = Path(f"{path}/export.csv")
    
    if not file.exists():
        return None
    
    meta_data = file.stat()
    
    timestamp = datetime.fromtimestamp(meta_data.st_atime).strftime('%Y-%m-%d %H:%M:%S')
    
    return timestamp
    
def save_export(path:str, content):
    file = Path(path)
    file.touch()
    file.write_text(content)
    
def load_export(path:str):
    file = Path(path)
    if not file.exists():
        return ""
    
    return file.read_text()