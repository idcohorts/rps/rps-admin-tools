from flask import flash, current_app, Blueprint, request, jsonify, render_template, send_from_directory, send_file
from pathlib import Path
import os

from ..scripts.import_users import read_imported_users, import_users

ui_bp = Blueprint(
    'users_import_blueprint', 
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@ui_bp.route('/import-users', methods=['GET','POST'])
def index():
    api_download = ui_bp.name + ".api_download_users_example"
    api_show = ui_bp.name + ".index"

    if request.method == 'GET':
        return render_template('home/import-users.html', 
                        title="Import Users",
                        data=[],
                        api_download=api_download, api_show=api_show)
    
    elif request.method == 'POST':
        # Show imported excel file
        file = request.files['file']

        # For showing in interface so the admin can check the users that are to be imported
        users_list, users_info = read_imported_users(file=file, config=current_app.config)

        import_allowed = all([user["user_valid"] for user in users_info])

        # roles = ["loks-l", "loks-m", "groups-management", "napkon", "num-portal-access"]
        
        # If no users are found in the file, show error message
        if not users_list:
            flash("No data found in file", 'danger')
            return render_template('home/import-users.html',
                                   data=[],
                                   api_show=api_show)

        return render_template('home/import-users.html', 
                               title="Import Users",
                               api_show=api_show,
                               file=file,
                               data=users_list,
                               users_info=users_info,
                            #    roles=roles,
                               import_allowed=import_allowed)

@ui_bp.route('/import-users/submit', methods=['POST'])
def api_submit_users():
    if request.method == "POST":

        data = request.get_json()
        if not data:
            return
        
        imported_users_list = data.get('user', [])

        if not imported_users_list:
            return

        overwrite = data.get('overwrite', False)

        print("Importing Users")
        import_users(imported_users_list, overwrite)

        return jsonify({"message": "success"})

