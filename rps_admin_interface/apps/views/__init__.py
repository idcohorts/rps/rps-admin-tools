from .views_home import home_bp
from .views_users_import import ui_bp
from .views_users_registration_review import urr_bp
from .views_group_users import gu_bp
from .views_users_grant_access import uga_bp
from .views_users_export import ur_bp