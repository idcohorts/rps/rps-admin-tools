from flask import current_app, Blueprint, request, jsonify, render_template, flash
from pathlib import Path
import logging, sys
from ..scripts.review_users import enable_user, disable_user, create_group_user_mapping

urr_bp = Blueprint(
    'users_registration_review_blueprint',
    __name__,
    url_prefix="",
    template_folder='/templates',
    static_folder='/static', static_url_path='/assets'
)

@urr_bp.route('/review-users', methods=['GET', 'POST'])
def index():
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]
    registered_users = keycloak_bridge.get_new_users()
    registered_users = sorted(registered_users, key=lambda user: user.timestamp, reverse=True)

    # get GROUPS_OPT_IN from env
    groups_opt_in = current_app.config["GROUPS_OPT_IN"]

    roles = current_app
    print("roles", roles)
    print("---------")
    
    users = []
    for user in registered_users:
        if not "approved" in user.roles:
            if user.enabled:
                user.keycloak_attributes = user.attributes.to_dict()
                users.append(user)
                
    keycloak_url = current_app.config["KEYCLOAK_URL"]
    keycloak_realm = current_app.config["KEYCLOAK_REALM"]
    keycloak_console_url = f"{keycloak_url}/admin/{keycloak_realm}/console"

    if request.method == 'GET':
        return render_template('home/review-users-registration.html',
                               title="Review Users Registration",
                               registered_users=users,
                               roles=current_app,
                               groups_opt_in=groups_opt_in,
                               keycloak_console_url=keycloak_console_url)


@urr_bp.route('/enable-user', methods=['POST'])
def api_enable_user():
    if request.method == "POST":
        user_dict = request.get_json()
        
        user_obj = enable_user(user_dict)

        if user_obj:
            flash("User enabled {}".format(user_obj.username),'success')

        return jsonify({"message": "success"})

@urr_bp.route('/disable-user', methods=['POST'])
def api_disable_user():
    if request.method == "POST":
        user_dict = request.get_json()

        user_obj = disable_user(user_dict)
        if user_obj:
            flash("User registration removed {}".format(user_obj.username), 'success')

        return jsonify({"message": "success"})
    
