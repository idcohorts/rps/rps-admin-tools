import datetime
from flask import current_app


def getInMapping(attribute):  
    if not current_app.config["ATTRIBUTE_MAPPING"]:
        return None
    
    config = current_app.config["ATTRIBUTE_MAPPING"]
    print(attribute)
    for mapping in config:
        if mapping["value"] == attribute:
            print("mapping", mapping)
            return mapping
        
    return None
    

def getDateFromTimeStamp(timestamp):
    time_date= datetime.datetime.fromtimestamp(timestamp/1000)
    return time_date.strftime("%d/%m/%Y, %H:%M")

def getAccess(member):
    if not current_app.config["ROLE_ATTRIBUTE_MAPPING"]:
        return []
    
    role_attribute_mapping = current_app.config["ROLE_ATTRIBUTE_MAPPING"]
    attributes = member.attributes.to_dict().items()
    if not attributes:
        return roles
    
    roles = []
    for role in role_attribute_mapping:
        
        for condition in role["when"]:
            
            if condition['attribute'] in member.attributes and member.attributes[condition['attribute']] == condition['value']:
                
                roles.append({'key': role, 
                              'value-group': role["group"], 
                              'value-role': role["role"], 
                              'name': role["title"], 
                              'checked': "checked", 
                              'input': 'roles'})
                break
    return roles
    

def getRoles(member):
    roles = []
    # get attributes of member
    attributes = member.attributes.to_dict().items()
    if not attributes:
        return roles
    for attribute, value in attributes:
        checked = ""
        if any(group in attribute for group in ["roles"]):
            # remove num-roles from attribute name
            # p.s when Attributes to_dict() is called, the value is a list thus value[0]
            mapping_value = attribute + "=" + value[0]
            roles.append({'key': attribute, 'value': mapping_value, 'name': attribute, 'checked': "checked", 
                          'input': 'roles'})
    return roles

def getGroups(member):
    groups = []
    # get attributes of member
    attributes = member.attributes.to_dict().items()
    if not attributes:
        return groups
    for attribute, value in attributes:
        if any(group in attribute for group in ["projects", "fosa"]):
            # remove num-project or num-fosa from attribute name
            mapping_value = attribute + "=" + value[0]
            groups.append({'key': attribute, 'value': mapping_value, 'name': attribute, 'checked': "checked", 
                           'input': 'groups'})
    return groups

def show_all_attrs(value):
    res = []
    for k in dir(value):
        res.append('%r %r\n' % (k, getattr(value, k)))
    return '\n'.join(res)
