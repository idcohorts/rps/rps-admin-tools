from flask import Blueprint, render_template

# Error Blueprint Handlers
error_bp = Blueprint('errors',
               __name__,
               template_folder='templates',
               static_folder='static', )

@error_bp.app_errorhandler(404)
def handle_404(error):
    return render_template('default/page-404.html')