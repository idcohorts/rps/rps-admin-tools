import pandas as pd
import re
from flask import current_app

from rps_sync_services.data_model import User, Attributes
from rps_sync_services.keycloak import KeycloakAdmin, KeycloakBridge


def read_imported_users(file, config):
    # Read Excel File and show all users written in Interface
    # Return Users' Information and their Groups

    # OUTPUT
    # List of imported users
    users_list = []
    # Verbose information of imported users
    users_info = []

    # Read Excel File and format it into dictionary
    users_df = pd.read_excel(file)
    
    users_df.dropna(how = 'all', inplace=True)
    users_df.fillna("",inplace=True)
    users_data = users_df.to_dict(orient='records')

    for user_data in users_data:
        # Parameters from Excel might have different names than the ones in Keycloak
        username = user_data.get(config["USER_USERNAME_LABEL"], "")
        email = user_data.get(config["USER_EMAIL_LABEL"], "")
        first_name = user_data.get(config["USER_FIRST_NAME_LABEL"], "")
        last_name = user_data.get(config["USER_LAST_NAME_LABEL"], "")
        groups_list = []
        if "USER_IMPORT_GROUPS_LABEL" in config:
            groups = user_data.get(config["USER_IMPORT_GROUPS_LABEL"], "")
            groups_list = list(filter(None,re.split(r"[,;]", groups)))
            groups_list = [x.strip(' ') for x in groups_list]
        
        role_list = []
        if "USER_IMPORT_ROLE_LABEL" in config:            
            roles = user_data.get(config["USER_IMPORT_ROLE_LABEL"], "")
            role_list = list(filter(None, re.split(r"[,;]", roles)))
            role_list = [x.strip(' ') for x in groups_list]

        # Attributes
        attribute_dict = {}
        for attribute in config.get("USER_ATTRIBUTES",[]):
            label = attribute["label"]
            value = user_data.get(label, "")
            attribute_dict[label] = value

        user = {
            "username": username,
            "email": email,
            "first_name": first_name,
            "last_name": last_name,
            "attributes": attribute_dict,
            "groups": groups_list,
            "roles" : role_list
        }

        users_list.append(user)

        # Check whether the user is valid or not
        user_validity_info = check_user_validity(user)
        users_info.append(user_validity_info)

    return users_list, users_info

def check_user_validity(user_dict):
    # Check whether the imported users are valid or not
    # Return same length list with validity
    keycloak_bridge: KeycloakBridge = current_app.config["KEYCLOAK_BRIDGE"]
    # Change it to User Object (needed for keycloak bridge)
    user = User(
        username=user_dict["username"],
        email=user_dict["email"],
    )

    # User validity DEFAULT: True
    user_valid = False
    # Username Exist
    username_exist = keycloak_bridge.keycloak_client.get_user_id(username=user.username)
    # Email Exist
    email_exist = keycloak_bridge.check_email_exists(user.email)
    # User Exist then Update
    user_update = keycloak_bridge.check_user(user)

    # Default (Innocent until proven guilty)
    username_valid = True
    email_valid = True
    # CHECK Username / Email / User Update
    if (not username_exist and not email_exist) or user_update:
        # if username and email does not exist and no user with the (username, email)
        # or if user already exist and is updating.
        username_valid = True
        email_valid = True
    else:
        if username_exist:
            # username is already taken
            username_valid = False
        if email_exist:
            # email is already taken
            email_valid = False

    # CHECK User Validity
    user_valid = all([username_valid, email_valid])

    user_validity_verbose = {
        "user_valid": user_valid,
        "user_update": user_update,
        "username_valid": username_valid,
        "email_valid": email_valid
    }

    return user_validity_verbose

def import_users(users_list, overwrite = False):
    keycloak_bridge: KeycloakBridge = current_app.config["KEYCLOAK_BRIDGE"]

    for imported_user in users_list:
        print("Importing:", imported_user["username"])
        
        user: User | None = keycloak_bridge.get_user_by_name(imported_user["username"],ignore_groups=False, ignore_roles=False, use_cache=False)
        
        if user:
            attr = user.attributes.to_dict()
            attr.update(imported_user["attributes"])
            user.attributes = Attributes(attr)
        

        # If the user does not exist or is to be overwritte create a new Object
        if not user or overwrite:
            
        
            # Groups should be separatedly added
            user = User(
                username=imported_user["username"],
                email=imported_user["email"],
                first_name=imported_user["first_name"],
                last_name=imported_user["last_name"],
                attributes=Attributes(imported_user["attributes"])
            )
            
        # set groups
        
        for group in imported_user["groups"]:
            user.add_group(group)
        
        user.add_group("/All Users")
        
        for role in imported_user["roles"]:
            user.add_role(role)
        
        user.add_role("approved")
        
        setattr(user.attributes, "send_after_registration_mail", 'true')

        user_obj = keycloak_bridge.sync_user(user, ignore_roles=False, ignore_groups=False)
        print(user_obj)
        # keycloak_bridge.send_verify_email(user_obj)




