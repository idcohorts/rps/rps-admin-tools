from flask import current_app
from datetime import datetime
from rps_sync_services.data_model import *
from rps_mail_sender import KeycloakMailSender

mail_sender = KeycloakMailSender(dry_run=False,verbose=True)
def create_group_user_mapping():
    # Get all groups and their mapping-group-user attributes
    # create a dictionary where the key is the mapping-group-user attribute and the value is a list of groups that
    # contain that key
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]
    kc_groups = keycloak_bridge.get_groups(ignore_members=True, ignore_children=False)
    
    group_dict = {}

    group_dict = create_group_dict(group_dict, kc_groups)

    return group_dict

def create_group_dict(group_dict, groups):
    for group in groups:
        print(group.group_name)
        group_dict[group.group_name] = group
        
        # if not group.attributes:
        #     continue
        # change to Python dict
        # group_attributes = group.attributes.to_dict()
        # mapping_group_user = group_attributes.get("mapping-group-user", None)
        # If the group has a mapping-group-user attribute, add it to the group dictionary
        # if mapping_group_user:
        #     if type(mapping_group_user) == list:
        #         for mapping_group_user_item in mapping_group_user:
        #             group_dict.setdefault(mapping_group_user_item, [])
        #             group_dict[mapping_group_user_item].append(group.group_name)
        #     else:
        #         # attribute is a string (see data_model.py Attributes class )
        #         group_dict.setdefault(mapping_group_user, [])
        #         group_dict[mapping_group_user].append(group.group_name)
        
        # Recurse if there are child groups
        if group.child_groups:
            group_dict = create_group_dict(group_dict, group.child_groups)
    
    return group_dict

def enable_user(user_dict):
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]
    user = keycloak_bridge.get_user_by_id(user_dict["keycloak_id"], ignore_roles=False, ignore_groups=False)

    user.add_role("approved")


    for group in user_dict["groups"]:
        user.add_group(group)
        
    new_users_path = keycloak_bridge.config.get("NEW_USERS_GROUP", "/NewUsers")
    group_name = new_users_path.replace("/","")
    user.remove_group(group_name)
    
    timestamp = datetime.timestamp(datetime.now())
    if not user.attributes:
        user.attributes = Attributes()
    setattr(user.attributes, "admin_interface_approval_timestamp", timestamp)
    user_obj = keycloak_bridge.sync_user(user, ignore_roles=False)
    mail_sender.send_accepted_registration_mail(user_obj)
    return user_obj

def disable_user(user_dict):
    keycloak_id = user_dict["keycloak_id"]
    reason_for_decline = user_dict["reason_for_decline"]
    print(keycloak_id)
    keycloak_bridge = current_app.config["KEYCLOAK_BRIDGE"]

    user = keycloak_bridge.get_user_by_id(keycloak_id, ignore_roles=False, ignore_groups = False)
    user.disable()

    new_users_path = keycloak_bridge.config.get("NEW_USERS_GROUP", "/NewUsers")
    group_name = new_users_path.replace("/","")
    user.remove_group(group_name)
    
    if not user.attributes:
        user.attributes = Attributes()
    setattr(user.attributes, "reason_for_decline", reason_for_decline)
    timestamp = datetime.timestamp(datetime.now())
    setattr(user.attributes, "admin_interface_decline_timestamp", timestamp)
    user_obj = keycloak_bridge.sync_user(user, ignore_roles=False)
    mail_sender.send_rejected_registration_mail(user_obj)
    return user_obj
