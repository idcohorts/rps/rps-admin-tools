def get_user_variables(config):
    # In the imported Excel File, the column names might be different from what is required
    # to be imported into Keycloak. Therefore we need to map the column names to the
    # required column names.
    user_variables_dict = {
        "email":config["USER_EMAIL_LABEL"],
        "username":config["USER_USERNAME_LABEL"],
        "firstName":config["USER_FIRST_NAME_LABEL"],
        "lastName":config["USER_LAST_NAME_LABEL"],
        "attributes":{},
        "group_membership":config["USER_IMPORT_GROUP_MEMBERSHIP_LABEL"],
        "group_owner":config["USER_IMPORT_GROUP_OWNER_LABEL"]
    }

    for attribute in config["user_attributes"]:
        temp_key = attribute["key"].lower()
        temp_label = attribute["label"]
        user_variables_dict["attributes"][temp_key] = temp_label
        
    return user_variables_dict
