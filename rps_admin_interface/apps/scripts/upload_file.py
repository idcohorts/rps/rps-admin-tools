import os
import pandas as pd
from flask import flash, current_app
from datetime import datetime
from werkzeug.utils import secure_filename

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in current_app.config["UPLOAD_EXTENSIONS"]

def show_upload_file(file):
    # Show result in dictionary format
    df = pd.read_excel(file)
    
    df.dropna(how = 'all', inplace=True)
    df.fillna("",inplace=True)
    data = df.to_dict(orient='records')

    return data

def upload_file(file):
    # Upload imported file into folder then show result at interface
    UPLOAD_FOLDER_FILE = current_app.config['UPLOAD_FOLDER']

    if not os.path.exists(UPLOAD_FOLDER_FILE):
        os.mkdir(UPLOAD_FOLDER_FILE)

    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        flash('No selected file')
        return []
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filename = unique_datetime_filename(filename)
        file.save(os.path.join(UPLOAD_FOLDER_FILE, filename))
        return show_upload_file(file)
    return []

def unique_datetime_filename(filename):
    now = datetime.now()
    dt_string = now.strftime("-%Y%m%d-%H%M%S")
    file_name , file_extension = os.path.splitext(str(filename))
    unique_filename =  file_name + dt_string + file_extension
    return unique_filename


