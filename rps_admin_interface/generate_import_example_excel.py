import pandas as pd

def generate_import_example_excel(config: dict, filename: str):

    # get labels for columns from config
    user_last_name_label = config['USER_LAST_NAME_LABEL']
    user_first_name_label = config['USER_FIRST_NAME_LABEL']
    user_username_label = config['USER_USERNAME_LABEL']
    user_email_label = config['USER_EMAIL_LABEL']
    user_import_roles_label = config['USER_IMPORT_ROLES_LABEL']
    user_import_groups_label = config['USER_IMPORT_GROUPS_LABEL']

    # generate excel file
    df = pd.DataFrame(columns=[user_first_name_label, user_last_name_label, user_username_label, user_email_label, user_import_roles_label, user_import_groups_label])
    df.to_excel(filename, index=False)
