import os
import sys
import os
from pathlib import Path
import debugpy
import json

# add project repo to sys.path
# debugpy.listen(("0.0.0.0", 5000))
BASE_REPO_DIR = Path(os.path.dirname(__file__)).resolve().parent
sys.path.append(str(BASE_REPO_DIR))

from flask import Flask

from .apps.default import error_bp
from .apps.views import home_bp, ui_bp, urr_bp, gu_bp, ur_bp ,uga_bp
from .apps.filters import getDateFromTimeStamp, show_all_attrs, getRoles, getGroups, getAccess, getInMapping

from rps_sync_services.keycloak import KeycloakBridge

# -*- encoding: utf-8 -*-

import os
import sys
from pathlib import Path

import json

from .generate_import_example_excel import generate_import_example_excel

from ._version import version

def strtobool(string:str) -> bool:
    return string.lower() == 'true'

template_folder = os.path.join(os.path.dirname(__file__), "apps/templates/")
print("template_folder", template_folder)

static_folder = os.path.join(os.path.dirname(__file__), "apps/static/")
print("static_folder", static_folder)

if strtobool(os.getenv("GENERATE_IMPORT_EXAMPLE_EXCEL", "True")):
    generate_import_example_excel(os.environ, static_folder + "/assets/import_users_example.xlsx")

app = Flask(__name__, template_folder=template_folder, static_folder=static_folder, static_url_path='/static')

app.config['VERSION'] = version

# Add the configuration to the app
app.config['DEBUG'] = strtobool(os.getenv("DEBUG", "False"))
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")

app.config['ASSETS_ROOT'] = os.getenv("ASSETS_ROOT", "/static/assets")

app.config['UPLOAD_FOLDER'] = "uploads"
app.config['UPLOAD_EXTENSIONS'] = ["xlsx", "csv", "xls"]

app.config['USER_EMAIL_LABEL'] = os.getenv("USER_EMAIL_LABEL", "E-Mail")
app.config['USER_FIRST_NAME_LABEL'] = os.getenv("USER_FIRST_NAME_LABEL", "First Name")
app.config['USER_LAST_NAME_LABEL'] = os.getenv("USER_LAST_NAME_LABEL", "Last Name")
app.config['USER_USERNAME_LABEL'] = os.getenv("USER_USERNAME_LABEL", "Username")
app.config['USER_IMPORT_ROLES_LABEL'] = os.getenv("USER_IMPORT_ROLE_LABEL", "Roles")
app.config['USER_IMPORT_GROUPS_LABEL'] = os.getenv("USER_IMPORT_GROUP_MEMBERSHIP_LABEL", "Groups")

app.config['KEYCLOAK_URL'] = os.getenv("KEYCLOAK_URL")
app.config['KEYCLOAK_REALM'] = os.getenv("KEYCLOAK_REALM")

app.config['ATTRIBUTE_ROLE_MAPPING'] = []
app.config['ATTRIBUTE_GROUP_MAPPING'] = []

app.config['ROLE_ATTRIBUTE_MAPPING'] = []
if os.getenv("ROLE_ATTRIBUTE_MAPPING"):
    # read the json string and convert it to a list
    app.config['ROLE_ATTRIBUTE_MAPPING'] = json.loads(os.getenv("ROLE_ATTRIBUTE_MAPPING"))

app.config['ATTRIBUTE_MAPPING'] = []
if os.getenv("ATTRIBUTE_MAPPING"):
    # read the json string and convert it to a list
    app.config['ATTRIBUTE_MAPPING'] = json.loads(os.getenv("ATTRIBUTE_MAPPING"))

if os.getenv("GROUPS_OPT_IN"):
    app.config['GROUPS_OPT_IN'] = json.loads(os.getenv("GROUPS_OPT_IN"))

# Can be accessed anywhere in views functions
app.config["KEYCLOAK_BRIDGE"] = KeycloakBridge()

# Register the default errors blueprint
app.register_blueprint(error_bp)

app.register_blueprint(home_bp)
app.register_blueprint(ui_bp)
app.register_blueprint(ur_bp)
app.register_blueprint(urr_bp)
app.register_blueprint(gu_bp)
app.register_blueprint(uga_bp)

# Register the custom filter
app.jinja_env.filters["getDateFromTimeStamp"] = getDateFromTimeStamp
app.jinja_env.filters['show_all_attrs'] = show_all_attrs
app.jinja_env.filters['getRoles'] = getRoles
app.jinja_env.filters['getGroups'] = getGroups
app.jinja_env.filters['getAccess'] = getAccess
app.jinja_env.filters['getInMapping'] = getInMapping

def rps_admin_interface():

    host = os.getenv("HOST","::1")
    port = int(os.getenv("PORT", "5000"))

    if (app.config["DEBUG"]):
        print("Debugging is enabled")
        print(app.config)

    app.run(host=host, port=port, debug=app.config["DEBUG"])

    if app.config["DEBUG"]:
        print("Waiting for client to attach...")
        debugpy.wait_for_client()
