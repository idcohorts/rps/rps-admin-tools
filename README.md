# RPS Admin Tools

## Development

### Local setup

Setup your virtual environment:

```
python3 -m venv .venv
source .venv/bin/activate
pip install -e .
```

Start the RPS Admin Interface

```
rps-admin interface
```

with config.yaml

```
rps-admin --config-file path/to/config.yaml interface
rps-admin interface
```

Start Nextcloud sync
```
rps-admin sync nextcloud
```

Start Discourse sync
```
rps-admin sync discourse
```

Start Budibase sync
```
rps-admin sync budibase
```

Start Registration sync
```
rps-admin sync registration
```

### Developer Commands

Create Fake Users
```
rps-admin tools create-fake-users
```

### Local development environment with docker
Run the following to start local development server for flask admin interface

```

docker-compose up --build

```

Go to:
- Admin Interface: http://localhost:5000
- Keycloak: http://localhost:5009

#### Update Keycloak Test Realm
To update the keycloak realm configuration, you can use the keycloak admin cli.
```

docker-compose exec keycloak /opt/keycloak/bin/kc.sh export --dir /opt/keycloak/data/import --realm rps

```
This will update your local in local-dev-env/keycloak.

## Used libraries

- keycloak
  - https://pypi.org/project/python-keycloak/
  - https://python-keycloak.readthedocs.io/
- discourse
  - https://pypi.org/project/pydiscourse/
  - https://github.com/bennylope/pydiscourse/blob/master/pydiscourse/client.py
  - https://docs.discourse.org/
- nextcloud
  - https://pypi.org/project/pyocclient/
- Faker
  - https://github.com/joke2k/faker/
  - https://faker.readthedocs.io/en/master/locales.html


## ToDos

Todos for Interface:
- [] Test Import Excel Example Download (from Cloud or local?)
