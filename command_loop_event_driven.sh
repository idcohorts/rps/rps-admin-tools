#!/bin/bash

echo "[rps-admin command loop] show last git commit"
git show --summary

if [ -z "${FULL_SYNC_TIME}" ]
then
        export FULL_SYNC_TIME=2
fi 
echo ${FULL_SYNC_TIME}


while echo "[rps-admin command loop] running rps-admin with parameters $@";
do

        case $(date +%H:%M) in
                (${FULL_SYNC_TIME}:*)        export FULL_SYNC=true;;
                (*)                          export FULL_SYNC=false;;
        esac
        if [ "${FULL_SYNC}" == "false" ]
        then
                echo "Starting event driven sync";
                rps-admin $@ --event-driven;
        else
                echo "Starting full sync";
                rps-admin $@
        fi

        echo "[rps-admin command loop] program finished, waiting sleeping ${SLEEP_TIME}"
        sleep ${SLEEP_TIME}

done
echo "[rps-admin command loop] program failed"
exit 1
