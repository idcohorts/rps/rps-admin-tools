Python Library Documentation: class DiscourseClient in module pydiscourse.client

class DiscourseClient(builtins.object)
 |  DiscourseClient(host, api_username, api_key, timeout=None)
 |  
 |  Discourse API client
 |  
 |  Methods defined here:
 |  
 |  __init__(self, host, api_username, api_key, timeout=None)
 |      Initialize the client
 |      
 |      Args:
 |          host: full domain name including scheme for the Discourse API
 |          api_username: username to connect with
 |          api_key: API key to connect with
 |          timeout: optional timeout for the request (in seconds)
 |      
 |      Returns:
 |  
 |  activate(self, user_id)
 |  
 |  add_group_member(self, groupid, username)
 |      Add a member to a group by username
 |      
 |      Args:
 |          groupid: the ID of the group
 |          username: the new member usernmae
 |      
 |      Returns:
 |          JSON API response
 |      
 |      Raises:
 |          DiscourseError if user is already member of group
 |  
 |  add_group_members(self, groupid, usernames)
 |      Add a list of members to a group by usernames
 |      
 |      Args:
 |          groupid: the ID of the group
 |          usernames: the list of new member usernames
 |      
 |      Returns:
 |          JSON API response
 |      
 |      Raises:
 |          DiscourseError if any of the users is already member of group
 |  
 |  add_group_owner(self, groupid, username)
 |      Add an owner to a group by username
 |      
 |      Args:
 |          groupid: the ID of the group
 |          username: the new owner usernmae
 |      
 |      Returns:
 |          JSON API response
 |  
 |  add_user_to_group(self, groupid, userid)
 |      Add a member to a group by with user id.
 |      
 |      Args:
 |          groupid: the ID of the group
 |          userid: the member id
 |      
 |      Returns:
 |          JSON API response
 |      
 |      Raises:
 |          DiscourseError if user is already member of group
 |  
 |  approve(self, user_id)
 |  
 |  badges(self, **kwargs)
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  block(self, user_id, **kwargs)
 |      Prevent user from creating topics or replying to posts.
 |      To prevent users logging in use suspend()
 |      
 |      Args:
 |          userid:
 |      
 |      Returns:
 |  
 |  by_external_id = user_by_external_id(self, external_id)
 |  
 |  categories(self, **kwargs)
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  category(self, name, parent=None, **kwargs)
 |      Args:
 |          name:
 |          parent:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  category_topics(self, category_id, **kwargs)
 |      Returns a list of all topics in a category.
 |      
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |          JSON API response
 |  
 |  color_schemes(self, **kwargs)
 |      List color schemes in site
 |      
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  create_category(self, name, color, text_color='FFFFFF', permissions=None, parent=None, **kwargs)
 |      Args:
 |          name:
 |          color:
 |          text_color: hex color without number symbol
 |          permissions: dict of 'everyone', 'admins', 'moderators', 'staff' with values of ???
 |          parent: name of the category
 |          parent_category_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  create_color_scheme(self, name, enabled, colors, **kwargs)
 |      Create new color scheme
 |      
 |      Args:
 |          name:
 |          enabled:
 |          colors:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  create_group(self, name, title='', visible=True, alias_level=0, automatic_membership_retroactive=False, primary_group=False, automatic=False, automatic_membership_email_domains='', grant_trust_level=1, flair_url=None, flair_bg_color=None, flair_color=None, **kwargs)
 |      Args:
 |      
 |          name: name of the group
 |          title: "" (title of the member of this group)
 |          visible: true
 |          alias_level: 0
 |          automatic_membership_retroactive: false
 |          primary_group: false
 |          automatic: false
 |          automatic_membership_email_domains: ""
 |          grant_trust_level: 1
 |          flair_url: Avatar Flair Image
 |          flair_bg_color: Avatar Flair Background Color
 |          flair_color: Avatar Flair Color
 |  
 |  create_post(self, content, category_id=None, topic_id=None, title=None, tags=[], **kwargs)
 |      Args:
 |          content:
 |          category_id:
 |          topic_id:
 |          title:
 |          tags:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  create_site_customization(self, name, enabled, stylesheet, **kwargs)
 |      Add a new Theme
 |      
 |      Args:
 |          name:
 |          enabled:
 |          stylesheet:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  create_user(self, name, username, email, password, **kwargs)
 |      Create a Discourse user
 |      
 |      Set keyword argument active='true' to avoid sending activation emails
 |      
 |      TODO: allow optional password and generate a random one
 |      
 |      Args:
 |          name: the full name of the new user
 |          username: their username (this is a key... that they can change)
 |          email: their email, will be used for activation and summary emails
 |          password: their initial password
 |          **kwargs: ???? what else can be sent through?
 |      
 |      Returns:
 |          ????
 |  
 |  customize_site_texts(self, site_texts, **kwargs)
 |      Set Text Content for site
 |      
 |      Args:
 |          site_texts:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  deactivate(self, user_id)
 |  
 |  delete_category(self, category_id, **kwargs)
 |      Remove category
 |      
 |      Args:
 |          category_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  delete_group(self, groupid)
 |      Deletes a group by its ID
 |      
 |      Args:
 |          groupid: the ID of the group
 |      
 |      Returns:
 |          JSON API response
 |  
 |  delete_group_member(self, groupid, userid)
 |      Deletes a member from a group by user ID
 |      
 |      Does not delete the user from Discourse.
 |      
 |      Args:
 |          groupid: the ID of the group
 |          userid: the ID of the user
 |      
 |      Returns:
 |          JSON API response
 |  
 |  delete_group_owner(self, groupid, userid)
 |      Deletes an owner from a group by user ID
 |      
 |      Does not delete the user from Discourse.
 |      
 |      Args:
 |          groupid: the ID of the group
 |          userid: the ID of the user
 |      
 |      Returns:
 |          JSON API response
 |  
 |  delete_topic(self, topic_id, **kwargs)
 |      Remove a topic
 |      
 |      Args:
 |          category_id:
 |          **kwargs:
 |      
 |      Returns:
 |          JSON API response
 |  
 |  delete_user(self, userid, **kwargs)
 |          block_email='true'
 |          block_ip='false'
 |          block_urls='false'
 |      
 |      Args:
 |          userid:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  generate_api_key(self, userid, **kwargs)
 |      Args:
 |          userid:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  grant_badge_to(self, username, badge_id, **kwargs)
 |      Args:
 |          username:
 |          badge_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  group(self, group_name)
 |      Get all infos of a group by group name
 |  
 |  group_members(self, group_name, offset=0, **kwargs)
 |      Get all members of a group by group name
 |  
 |  group_owners(self, group_name)
 |      Get all owners of a group by group name
 |  
 |  groups(self, **kwargs)
 |      Returns a list of all groups.
 |      
 |      Returns:
 |          List of dictionaries of groups
 |      
 |              [
 |                {
 |                  'alias_level': 0,
 |                  'automatic': True,
 |                  'automatic_membership_email_domains': None,
 |                  'automatic_membership_retroactive': False,
 |                  'grant_trust_level': None,
 |                  'has_messages': True,
 |                  'id': 1,
 |                  'incoming_email': None,
 |                  'mentionable': False,
 |                  'name': 'admins',
 |                  'notification_level': 2,
 |                  'primary_group': False,
 |                  'title': None,
 |                  'user_count': 9,
 |                  'visible': True
 |                },
 |                {
 |                  'alias_level': 0,
 |                  'automatic': True,
 |                  'automatic_membership_email_domains': None,
 |                  'automatic_membership_retroactive': False,
 |                  'grant_trust_level': None,
 |                  'has_messages': False,
 |                  'id': 0,
 |                  'incoming_email': None,
 |                  'mentionable': False,
 |                  'name': 'everyone',
 |                  'notification_level': None,
 |                  'primary_group': False,
 |                  'title': None,
 |                  'user_count': 0,
 |                  'visible': True
 |                }
 |              ]
 |  
 |  hot_topics(self, **kwargs)
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  invite(self, email, group_names, custom_message, **kwargs)
 |      Invite a user by email to join your forum
 |      
 |      Args:
 |          email: their email, will be used for activation and summary emails
 |          group_names: the group names
 |          custom_message: message to include
 |          **kwargs: ???? what else can be sent through?
 |      
 |      Returns:
 |          API response body (dict)
 |  
 |  invite_link(self, email, group_names, custom_message, **kwargs)
 |      Generate an invite link for a user to join your forum
 |      
 |      Args:
 |          email: their email, will be used for activation and summary emails
 |          group_names: the group names
 |          custom_message: message to include
 |          **kwargs: ???? what else can be sent through?
 |      
 |      Returns:
 |          Invite link
 |  
 |  invite_user_to_topic(self, user_email, topic_id)
 |      Args:
 |          user_email:
 |          topic_id:
 |      
 |      Returns:
 |  
 |  latest_topics(self, **kwargs)
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  list_users(self, type, **kwargs)
 |      optional user search: filter='test@example.com' or filter='scott'
 |      
 |      Args:
 |          type:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  log_out(self, userid)
 |      Args:
 |          userid:
 |      
 |      Returns:
 |  
 |  new_topics(self, **kwargs)
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  pick_avatar(self, username, gravatar=True, generated=False, **kwargs)
 |      Args:
 |          username:
 |          gravatar:
 |          generated:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  post(self, topic_id, post_id, **kwargs)
 |      Args:
 |          topic_id:
 |          post_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  posts(self, topic_id, post_ids=None, **kwargs)
 |      Get a set of posts from a topic
 |      
 |      Args:
 |          topic_id:
 |          post_ids: a list of post ids from the topic stream
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  private_messages(self, username=None, **kwargs)
 |      Args:
 |          username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  private_messages_unread(self, username=None, **kwargs)
 |      Args:
 |          username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  search(self, term, **kwargs)
 |      Args:
 |          term:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  set_preference(self, username=None, **kwargs)
 |      Args:
 |          username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  site_settings(self, **kwargs)
 |      Args:
 |          settings:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  suspend(self, userid, duration, reason)
 |      Suspend a user's account
 |      
 |      Args:
 |          userid: the Discourse user ID
 |          duration: the length of time in days for which a user's account
 |                  should be suspended
 |          reason: the reason for suspending the account
 |      
 |      Returns:
 |          ????
 |  
 |  sync_sso(self, **kwargs)
 |      expect sso_secret, name, username, email, external_id, avatar_url,
 |      avatar_force_update
 |      
 |      Args:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  tag_group(self, name, tag_names, parent_tag_name=None, **kwargs)
 |      Create a new tag group
 |      
 |      Args:
 |          name:
 |          tag_names:
 |          parent_tag_name:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  toggle_gravatar(self, username, state=True, **kwargs)
 |      Args:
 |          username:
 |          state:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  topic(self, slug, topic_id, **kwargs)
 |      Args:
 |          slug:
 |          topic_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  topic_posts(self, topic_id, **kwargs)
 |      Args:
 |          topic_id:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  topic_timings(self, topic_id, time, timings={}, **kwargs)
 |      Set time spent reading a post
 |      
 |      A side effect of this is to mark the post as read
 |      
 |      Args:
 |          topic_id: { post_number: ms }
 |          time: overall time for the topic (in what unit????)
 |          timings:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  topics_by(self, username, **kwargs)
 |      Args:
 |          username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  trust_level(self, userid, level)
 |      Args:
 |          userid:
 |          level:
 |      
 |      Returns:
 |  
 |  trust_level_lock(self, user_id, locked, **kwargs)
 |      Lock user to current trust level
 |      
 |      Args:
 |          user_id:
 |          locked:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  unsuspend(self, userid)
 |      Unsuspends a user's account
 |      
 |      Args:
 |          userid: the Discourse user ID
 |      
 |      Returns:
 |          None???
 |  
 |  update_avatar(self, username, url, **kwargs)
 |      Args:
 |          username:
 |          url:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_avatar_from_url(self, username, url, **kwargs)
 |      Args:
 |          username:
 |          url:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_avatar_image(self, username, img, **kwargs)
 |      Specify avatar using a URL
 |      
 |      Args:
 |          username:
 |          img:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_email(self, username, email, **kwargs)
 |      Args:
 |          username:
 |          email:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_post(self, post_id, content, edit_reason='', **kwargs)
 |      Args:
 |          post_id:
 |          content:
 |          edit_reason:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_topic(self, topic_url, title, **kwargs)
 |      Update a topic
 |      
 |      Args:
 |          topic_url:
 |          title:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_topic_status(self, topic_id, status, enabled, **kwargs)
 |      Open or close a topic
 |      
 |      Args:
 |          topic_id:
 |          status:
 |          enabled:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_user(self, username, **kwargs)
 |      Args:
 |          username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  update_username(self, username, new_username, **kwargs)
 |      Args:
 |          username:
 |          new_username:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  upload_image(self, image, type, synchronous, **kwargs)
 |      Upload image or avatar
 |      
 |      Args:
 |          name:
 |          file:
 |          type:
 |          synchronous:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  user(self, username)
 |      Get user information for a specific user
 |      
 |      TODO: include sample data returned
 |      TODO: what happens when no user is found?
 |      
 |      Args:
 |          username: username to return
 |      
 |      Returns:
 |          dict of user information
 |  
 |  user_actions(self, username, filter, offset=0, **kwargs)
 |      List all possible user actions
 |      
 |      Args:
 |          username:
 |          filter:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  user_all(self, user_id)
 |      Get all user information for a specific user, needs to be admin
 |      
 |      Args:
 |          user_id: id of the user to return
 |      Returns:
 |          dict of user information
 |  
 |  user_badges(self, username, **kwargs)
 |      Args:
 |          username:
 |      
 |      Returns:
 |  
 |  user_by_external_id(self, external_id)
 |      Args:
 |          external_id:
 |      
 |      Returns:
 |  
 |  user_emails(self, username, **kwargs)
 |      Retrieve list of users email addresses
 |      
 |      Args:
 |          username:
 |      
 |      Returns:
 |  
 |  users(self, filter=None, **kwargs)
 |      Args:
 |          filter:
 |          **kwargs:
 |      
 |      Returns:
 |  
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |  
 |  __dict__
 |      dictionary for instance variables (if defined)
 |  
 |  __weakref__
 |      list of weak references to the object (if defined)
