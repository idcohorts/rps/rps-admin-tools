
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html), 
with the exception that major version v0 is considered unstable with breaking changes.

## [Unreleased]

## v0.3.6
### Fixed
- fixed KeycloakEventBridge timing out due to a too broad query
- fixed KeycloakBridge recursion error

## v0.3.5
### Fixed
- fixed review-count in AdminInterface
- fixed KeycloakBridge returning names instead paths when calling get_user_groups

### Added
- added option to overwrite user instead of always overwriting

### Removed
- removed deprecated dependency (distutils.util)


## v0.3.4
### Fixed
- fixed AdminInterface using deprecated methods
- fixed wrong parent group for inherited groups

## v0.3.3
### Fixed
- fixed returning duplicates of groups (Way higher runtime)
## v0.3.2
### Fixed
- fixed export timeout for admin-interface

### Added
- Added download lastest export function in admin-interface

### Change
- Changed email-subject to use env vars

## v0.3.1
### Fixed
- fixed tls option for mail_sender

## v0.3.0
### Added
- added KeycloakEventBridge
- added event-driven sync for all refactored syncs (Cloud,Matrix,Permissions)
- added a link to the Keycloak Console in the Admin-Interface

### Removed
- removed old dependency on a custom fork (python-keycloak)

### Changed
- refactored PermissionSync

## v0.2.11
### Fixed
- fixed sync_user check for type not if not empty
- fixed Not getting roles when calling get_new_users
### Added
- added default-mail-template

### Changed
- call send_mail directly from admin-interface

## v0.2.10
### Fixed
- fixed slow admin interface
- fixed alerta alerts for sync scripts
### Added
- added group-opt-in in admin interface (assign groups on approval)

## v0.2.9
### Fixed
- fixed NextcloudBridge crash due to racing condition when a new user registers

## v0.2.8
### Fixed
- fixed NextcloudBridge guest groups getting 31 permission on firtst sync
- fixed mail_sync template_path and logic for new_users


## v0.2.7
### Fixed
- Bugs in MatrixBridge (error in __update_user if user is not yet created, overriding the var group in method __update_room)

## v0.2.6

### Changed
- Some refactoring (naming conventions)
- Adjusted logger levels

### Fixed
- Resolve the inconsistency of name values in NextcloudBridge
 

## v0.2.5

### Changed
- Some refactoring (logging, cleanup)

### Fixed
- dependency fix

## v0.2.4

### Changed
- Improved logging and error handling.

### Added
- Also added support for regex-pattern to exclude users

## v0.2.3
### Removed
- Automated User deletion

## v0.2.2
### Added
- Added improved logging of most NextcloudBridge methods

## v0.2.1
### Changed
- Improved build pipeline

## v0.2.0

First versions with the newly refactored classes.

## v0.1.0

Version prior to refactoring with base classes