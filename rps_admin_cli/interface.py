from .cli import rps_admin_cli

@rps_admin_cli.command(help="Start the RPS Admin Interface")
def interface():
    # lazy import to avoid loading the interface when not needed
    from rps_admin_interface import rps_admin_interface
    rps_admin_interface()
