from rps_mail_sender.keycloak_mail_sender import KeycloakMailSender
from rps_sync_services import Bridge
from rps_sync_services import KeycloakBridge
from rps_sync_services import NextcloudBridge
from rps_sync_services import MatrixBridge
from rps_sync_services import KeycloakEventBridge
# from rps_sync_services import DiscourseSync
from rps_sync_services import PermissionBridge
import click
from .cli import rps_admin_cli


@rps_admin_cli.group(help="User sync services")
def sync():
    pass


@sync.command(help="Sync users from Keycloak to Nextcloud")
@click.option("--event-driven", is_flag=True, default=False, 
              help="Use the Keycloak event api to only sync changes")
def nextcloud(event_driven):
    bridge_base = Bridge()
    try:
        # Check if sync should run based on keycloak events
        nc_bridge = NextcloudBridge()
        if event_driven:
            kc_bridge = KeycloakEventBridge()
            nc_bridge.sync_with_bridge(kc_bridge, cleanup=False)
        else:
            kc_bridge = KeycloakBridge()
            nc_bridge.sync_with_bridge(kc_bridge, cleanup=True)
            
        # Send Alert when finished
        bridge_base.send_alert( severity   = "ok",
                                status     = "closed",
                                text_field = f"\nNextcloud <-> Keycloak sync\n"
                                )
    except Exception as e:
        bridge_base.send_alert( severity   = "major",
                                status     = "open",
                                text_field = f"\nFailed: Nextcloud <-> Keycloak sync: \n{e}\n"
                                )
        raise e

@sync.command(help="Sync users from Keycloak to Disourse")
@click.option(
    "--dry-run", is_flag=True, default=False, help="Do not actually sync users"
)
@click.option("--verbose", is_flag=True, default=False, help="Show more information")
def discourse(dry_run, verbose):
    raise NotImplementedError("Discourse sync is not implemented")
    # discourse_sync = DiscourseSync(dry_run, verbose)
    # discourse_sync.sync_users()
    # discourse_sync.sync_categories()
    # pass


@sync.command(help="Sync users and groups to Matrix Chat")
@click.option("--event-driven", is_flag=True, default=False, 
              help="Use the Keycloak event api to only sync changes")
def matrix(event_driven):
    bridge_base = Bridge()
    try:
        matrix_sync = MatrixBridge()
        if event_driven:
            kc_bridge = KeycloakEventBridge()
            matrix_sync.sync_with_bridge(kc_bridge, cleanup=False)
        else:
            kc_bridge   = KeycloakBridge()
            matrix_sync.sync_with_bridge(kc_bridge,cleanup=True)
        # Send Alert when finished
        bridge_base.send_alert ( severity   = "ok",
                                    status     = "closed",
                                    text_field = f"\nMatrix <-> Keycloak sync\n"
                            )
    except Exception as e:
        bridge_base.send_alert( severity   = "major",
                                status     = "open",
                                text_field = f"\nFailed: Matrix <-> Keycloak sync: \n{e}\n"
                                )
        raise e
                   


@sync.command(help="Set Keycloak policies and permission based on the group attributes")
@click.option("--event-driven", is_flag=True, default=False, 
              help="Use the Keycloak event api to only sync changes")
def permissions(event_driven):
    bridge_base = Bridge()
    try:
        perm_bridge = PermissionBridge()
        if event_driven:
            kc_bridge = KeycloakEventBridge()
            perm_bridge.sync_with_bridge(kc_bridge, cleanup=False)
        else:
            kc_bridge = KeycloakBridge()
            perm_bridge.sync_with_bridge(kc_bridge, cleanup=False)
            
        # Send Alert when finished
        bridge_base.send_alert ( severity   = "ok",
                                    status     = "closed",
                                    text_field = f"\nPermission Sync <-> Keycloak sync\n"
                            )
    except Exception as e:
        bridge_base.send_alert( severity   = "major",
                                status     = "open",
                                text_field = f"\nFailed: Permission Sync <-> Keycloak sync: \n{e}\n"
                                )
        raise e
