import os
import click
import yaml
from dotenv import load_dotenv

from ._version import version

def echo_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo(f"RPS Admin Tools version {version}")
    ctx.exit()

@click.group()
@click.option('--version', is_flag=True, callback=echo_version, expose_value=False, is_eager=True, help='Show the version and exit.')

def rps_admin_cli():
    load_dotenv()

@rps_admin_cli.command(help="Show the current environment variables")
def env():
    print(yaml.dump(dict(os.environ), default_flow_style=False))
