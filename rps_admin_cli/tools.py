from .cli import rps_admin_cli

import click
import json
import yaml
from rps_sync_services.keycloak import KeycloakBridge
from rps_sync_services.data_model import Group, User
from faker import Faker
import pandas as pd


@rps_admin_cli.group(help="Developer Tools")
def tools():
    pass


# @rps_admin_cli.command(help="Get new users")
# @click.option('--format', '-f', default="txt", help='Output format', type=click.Choice(['json', 'yaml', 'txt']))
# @click.pass_obj
# def get_new_users(rps_admin_cli, format):
#     new_users = rps_admin_cli.keycloak.get_new_users()
#     if format == "json":
#         click.echo(json.dumps(new_users, indent=4))
#     elif format == "yaml":
#         click.echo(yaml.dump(new_users))
#     elif format == "txt":
#         for user in new_users:
#             click.echo(user)

@tools.command(help="Create Fake Keycloak users using Faker")
@click.option('--count', '-c', default=10000, help='The ammount of users that will be created', type=int)         
@click.option('--verbose', '-v', is_flag=True, default=False, help='Print usernames', type=int)     
def create_fake_users(count, verbose):
    
    kc_bridge = KeycloakBridge()
    user = kc_bridge.get_users(ignore_roles=True, ignore_groups=True)
    fake = Faker(['it_IT', 'de_DE', 'de_AT', 'de_CH', 'en_US'])
    with click.progressbar(range(0,count), fill_char= '█', empty_char='░') as bar:
        for i in bar:

            first_name = fake.first_name()
            last_name = fake.last_name()
            username = f"{first_name.replace(' ','-').replace('.','')}.{last_name.replace(' ','-').replace('.','')}"
            bar.label = f"{i+1} / {count}"
            email = f"{username}@fake-user.de"
            new_user = User(username=username,email=email,first_name=first_name, last_name=last_name, enabled=True, email_verified=True)
            kc_bridge.create_user(new_user)
            if verbose:
                click.echo(username)
@tools.command(help="export users")    
@click.option('--verbose', '-v', is_flag=True, default=False, help='Print usernames', type=int)     
def export_users_cli(verbose):
    export_users(verbose)
    
def export_users(verbose = False, as_string = False):
    kc_bridge = KeycloakBridge()
    users =  kc_bridge.get_all_users(ignore_groups=False)
    formated_users = []
    for user in users :
        formated_user = user.to_keycloak_dict()
        for group in user.groups: 
            formated_user[group] = "True"
        formated_users.append(formated_user) 
    df = pd.DataFrame(formated_users)
    if as_string: 
        return df.to_csv(index=False)
    df.to_csv('out.csv', index=False)  
   
