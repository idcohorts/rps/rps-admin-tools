#!/bin/bash

echo "[rps-admin command loop] show last git commit"
git show --summary

PARAMETERS=$@

while echo "[rps-admin command loop] running rps-admin with parameters ${PARAMETERS}"; rps-admin ${PARAMETERS};
do
        echo "[rps-admin command loop] program finished, waiting sleeping ${SLEEP_TIME}"
        sleep ${SLEEP_TIME}
done
echo "[rps-admin command loop] program failed"
exit 1
