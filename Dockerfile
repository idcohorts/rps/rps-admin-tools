FROM python:3.11

WORKDIR /opt/rps-admin-tools

# install python dependencies
#RUN pip install --upgrade pip
COPY . .
RUN pip install -e .

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG FALSE

RUN chmod +x ./command_loop.sh ./command_loop_event_driven.sh
EXPOSE 5000

CMD rps-admin interface
