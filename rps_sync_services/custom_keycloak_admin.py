import json
from keycloak import KeycloakAdmin
from keycloak import urls_patterns
from keycloak.exceptions import (
    KeycloakPostError,
    KeycloakPutError,
    KeycloakGetError,
    raise_error_from_response,
)

URL_ADMIN_CLIENT_AUTHZ_GROUP_BASED_POLICY = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/group?max=-1"
URL_ADMIN_CLIENT_AUTHZ_GROUP_BASED_POLICY_UPDATE = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/group/{policy_id}" 
URL_ADMIN_CLIENT_AUTHZ_GROUP_BASED_POLICY = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/group?max=-1"
URL_ADMIN_CLIENT_AUTHZ_SCOPE_BASED_PERMISSION = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/permission/scope?max=-1"
URL_ADMIN_CLIENT_AUTHZ_ROLE_BASED_POLICY_UPDATE = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/role/{policy_id}"
URL_ADMIN_CLIENT_AUTHZ_PERMISSION_RESOURCES = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/{policy-id}/resources"
URL_ADMIN_CLIENT_AUTHZ_PERMISSION_SCOPES = urls_patterns.URL_ADMIN_CLIENT_AUTHZ + "/policy/{policy-id}/scopes"
class CustomKeycloakAdmin(KeycloakAdmin):
    
    

    
    """
        An extension to the KeycloakAdmin class, to support updating_client_authz_policies
    Args:
        KeycloakAdmin (_type_): _description_
    """
    
    
    def update_client_authz_role_based_policy(self, client_id, policy_id, payload):
        """Update role-based policy of client.
        Payload example::
            payload={
                "id": "policy_id"
                "type": "role",
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "name": "Policy-1",
                "roles": [
                    {
                    "id": id
                    }
                ]
            }
        :param client_id: id in ClientRepresentation
            https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_clientrepresentation
        :type client_id: str
        :param payload: No Document
        :type payload: dict
        :param skip_exists: Skip creation in case the object exists
        :type skip_exists: bool
        :return: Keycloak server response
        :rtype: bytes
        """
        params_path = {"realm-name": self.connection.realm_name, "id": client_id,"policy_id": policy_id}

        data_raw = self.connection.raw_put(
            URL_ADMIN_CLIENT_AUTHZ_ROLE_BASED_POLICY_UPDATE.format(**params_path),
            data=json.dumps(payload),
        )
        return raise_error_from_response(
            data_raw, KeycloakPostError, expected_codes=[201],
        )
    
    
    def update_client_authz_group_based_policy(self, client_id,policy_id, payload):
        """Update group-based policy of client.

        Payload example::

            payload={
                "id": "policy_id"
                "type": "group",
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "name": "Policy-1",
                "groups": [
                    {
                    "id": id
                    }
                ]
            }

        :param client_id: id in ClientRepresentation
            https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_clientrepresentation
        :type client_id: str
        :param payload: No Document
        :type payload: dict
        :param skip_exists: Skip creation in case the object exists
        :type skip_exists: bool
        :return: Keycloak server response
        :rtype: bytes

        """
        params_path = {"realm-name": self.connection.realm_name, "id": client_id, "policy_id": policy_id}

        data_raw = self.connection.raw_put(
            URL_ADMIN_CLIENT_AUTHZ_GROUP_BASED_POLICY_UPDATE.format(**params_path),
            data=json.dumps(payload),
        )
        return raise_error_from_response(
            data_raw, KeycloakPostError, expected_codes=[201]
        )
    
    
    
    def create_client_authz_group_based_policy(self, client_id, payload, skip_exists=False):
        """Create group-based policy of client.
        Payload example::
            payload={
                "type": "group",
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "name": "Policy-1",
                "groups": [
                    {
                    "id": id
                    }
                ]
            }
        :param client_id: id in ClientRepresentation
            https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_clientrepresentation
        :type client_id: str
        :param payload: No Document
        :type payload: dict
        :param skip_exists: Skip creation in case the object exists
        :type skip_exists: bool
        :return: Keycloak server response
        :rtype: bytes
        """
        params_path = {"realm-name": self.connection.realm_name, "id": client_id}

        data_raw = self.connection.raw_post(
            URL_ADMIN_CLIENT_AUTHZ_GROUP_BASED_POLICY.format(**params_path),
            data=json.dumps(payload),
        )
        return raise_error_from_response(
            data_raw, KeycloakPostError, expected_codes=[201], skip_exists=skip_exists
        )

    def update_resource_server(self, client_id, payload):
        """Update a client.
        :param client_id: Client id
        :type client_id: str
        :param payload: payload
        :type payload: dict
        :return: Http response
        :rtype: bytes
        """
        params_path = {"realm-name": self.connection.realm_name, "id": client_id}
        data_raw = self.connection.raw_put(
            urls_patterns.URL_ADMIN_CLIENT_AUTHZ.format(**params_path), data=json.dumps(payload)
        )
        return raise_error_from_response(data_raw, KeycloakPutError, expected_codes=[204])
    
    def create_client_authz_scope_based_permission(self, client_id, payload, skip_exists=False):
        """Create scope-based permission of client.
        Payload example::
            payload={
                "type": "resource",
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "name": "Permission-Name",
                "scopes": [
                    scope_id
                ],
                "policies": [
                    policy_id
                ]
        :param client_id: id in ClientRepresentation
            https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_clientrepresentation
        :type client_id: str
        :param payload: PolicyRepresentation
            https://www.keycloak.org/docs-api/18.0/rest-api/index.html#_policyrepresentation
        :type payload: dict
        :param skip_exists: Skip creation in case the object already exists
        :type skip_exists: bool
        :return: Keycloak server response
        :rtype: bytes
        """
        params_path = {"realm-name": self.connection.realm_name, "id": client_id}

        data_raw = self.connection.raw_post(
            URL_ADMIN_CLIENT_AUTHZ_SCOPE_BASED_PERMISSION.format(**params_path),
            data=json.dumps(payload),
        )
        return raise_error_from_response(
            data_raw, KeycloakPostError, expected_codes=[201], skip_exists=skip_exists
        )
        
    def get_client_authz_permission_resources(self, client_id, permission_id):
            """Get permission resources from client.

            :param client_id: id in ClientRepresentation
                https://www.keycloak.org/docs-api/24.0.2/rest-api/index.html#_clientrepresentation
            :type client_id: str
            :return: Keycloak server response
            :rtype: list
            """
            params_path = {"realm-name": self.connection.realm_name, "id": client_id, "policy-id": permission_id}
            data_raw = self.connection.raw_get(
                URL_ADMIN_CLIENT_AUTHZ_PERMISSION_RESOURCES.format(**params_path)
            )
            return raise_error_from_response(data_raw, KeycloakGetError)
        
        
    def get_client_authz_permission_scopes(self, client_id, permission_id):
            """Get permission resources from client.

            :param client_id: id in ClientRepresentation
                https://www.keycloak.org/docs-api/24.0.2/rest-api/index.html#_clientrepresentation
            :type client_id: str
            :return: Keycloak server response
            :rtype: list
            """
            params_path = {"realm-name": self.connection.realm_name, "id": client_id, "policy-id": permission_id}
            data_raw = self.connection.raw_get(
                URL_ADMIN_CLIENT_AUTHZ_PERMISSION_SCOPES.format(**params_path)
            )
            return raise_error_from_response(data_raw, KeycloakGetError)