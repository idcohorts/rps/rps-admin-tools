#########################
# Tulsky V.A. 2023-2024 #
# Hamacher E. 2023-2024 #
#########################

import os
import requests
from requests.exceptions import HTTPError
import random
from typing import Any, Coroutine, Dict, Optional, Sequence, Union

from nio import AsyncClient
from nio import Api
from nio.api import RoomVisibility
from nio.responses import RoomCreateError, RoomCreateResponse
from synadm.api import SynapseAdmin

from .data_model import User, Group
from .bridge_base import Bridge


class MatrixBridge(Bridge):
    """This Bridge is used to access the Matrix Synapse API
    API Docs: https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/index.html"""

    def __init__(self, config: dict|None = None):

        # Init BridgeBase
        super(MatrixBridge, self).__init__(config)

        self.service_name = "Matrix"

        # the domain of matrix synapse
        self.homeserver     = self.config["MATRIX_HOMESERVER"]    
          
        # the TAG of the matrix synapse server
        self.homeserver_tag = self.config["MATRIX_HOMESERVER_TAG"]  
        
        # in @user:homeserver_tag format
        self.user           = self.config["MATRIX_USERNAME"]        
        
        # token of an admin user that will send http requests
        self.token          = self.config["MATRIX_ACCESS_TOKEN"]    
        
        self.ssl            = self.config.get('MATRIX_SSL', None)
        self.proxy          = self.config.get('MATRIX_PROXY', None)
        self.admin_path     = self.config.get('SYNAPSE_ADMIN_PATH', '/_synapse/admin')

        # Init Matrix Synapse Client
        try:
            self.synapse_client = SynapseAdmin(self.logger, self.user, self.token,
                                           self.homeserver, self.admin_path,3000,False,True)
        except Exception as e:
            raise ConnectionError(e)
        
        self.server_version = self.synapse_client.version()
        self.server_version = self.server_version.get("server_version")
        self.logger.info("Initialized Bridge and established connection with Matrix Synapse server")
        self.logger.info("Matrix Synapse server version is : %s" ,self.server_version)

        # Used for caching
        self.user_cache: list[User] = []
        self.group_cache: list[Group] = []
        self.use_cache = self.config.get('USE_CACHE', 'True').lower() in ('true', '1')
        
        # if self.use_cache:
        #     self.logger.debug(f" Generating cache")
        #     for user in self.get_users():
        #         self.user_cache_append(user)
        #     for group in self.get_groups():
        #         self.group_cache_append(group)
        # 
        # FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
        # logging.basicConfig(format=FORMAT)

    def group_cache_append(self, group: Group):
        """Append a group to the group_cache

        Args:
            group (Group): The Group which will be added to the cache
        """
        
        if not self.use_cache:
            return
        if group not in self.group_cache:
            self.group_cache.append(group)
        #     self.logger.debug(f" Group added to self.group_cache: {group}")
        # else:
        #     self.logger.debug(f" Group already in self.group_cache: {group}")
        
    def user_cache_append(self, user: User):
        """Append a user to the user_cache

        Args:
            user (User): The User which will be added to the cache
        """
        
        if not self.use_cache:
            return
        
        cached_users = [cached_user for cached_user in self.user_cache 
                        if cached_user.username == user.username]
        if cached_users:
            self.user_cache.remove(cached_users[0])
        self.user_cache.append(user)


    ### Group Operations ###

    def get_groups(self):
        # self.logger.debug(" get_groups: returning a list of Rooms and Spaces")
        rooms = self.__get_rooms()
        # Ignoring Rooms without canonical alias
        rooms = [room for room in rooms if not room['canonical_alias'] == None]
        # Converting only Rooms to Groups, ignoring Spaces
        groups = [self.convert_to_group(room) for room in rooms if not 'space----' in room['canonical_alias']]
        return groups

    def __get_rooms(self):
        rooms = []
        offset = 0
        while True:
            # response = self.synapse_client.room_list(_from, limit, name, order_by, reverse)
            response = self.synapse_client.room_list(offset,None,None,None,None)
            if not response:
                raise ValueError('Response is None')
            more_rooms = response.get('rooms', [])
            if more_rooms == []:
                break
            offset += 100
            rooms.extend(more_rooms)

        for room in rooms:
            if room['creator'] != self.user:
                # Just flag the room an dont remove it from rooms,
                # to prevent index jumping
                room['ignore'] = True
                continue
            
            room['aliases'] = self.__get_room_aliases(room['room_id'])
            
            members = self.synapse_client.room_members(room['room_id'])
            if not members:
                continue

            room['members'] = members.get('members', [])
        rooms = [room for room in rooms if not room.get('ignore', False)]
        return rooms

    def get_group(self, group: str | Group):
        alias = None
        if isinstance(group, Group):
            if group.keycloak_id:
                alias = f"#{self.__room_alias_from_group(group)}"       
            if group.path:
                alias = f"#{group.path}"
        
        if isinstance(group, str):
            alias = f"#{group}"
        
        if not alias:
            raise ValueError("Could not get alias")
        
        room_id = self.__resolve_room_alias(alias)
        if not room_id:
            raise ValueError("Could not resolve alias")
        room = self.__get_room(room_id)
        room['aliases'] = self.__get_room_aliases(room_id)
        group = self.convert_to_group(room)
        
        return group

    def __get_room(self, room_id: str):
        if not room_id:
            raise ValueError('room_id can not be None')
        
        if not isinstance(room_id, str):
            raise TypeError('room_id should be of type str')
        members = self.synapse_client.room_members(room_id)
        room = self.synapse_client.room_details(room_id)
        if not room:
            raise ValueError('room can not be None')
        if members and isinstance(members, dict):
            room['members'] = members.get('members', [])
        return room


    def check_group(self, group: Group):

        if not isinstance(group, Group):
            raise TypeError('group can only be of type Group')

        # Check for Room
        room_alias = f"#{self.__room_alias_from_group(group)}"
        if not self.__check_room(room_alias):
            return False

        # Check for Space
        space_alias = f"#{self.__space_alias_from_group(group)}"
        if not self.__check_room(space_alias):
            return False

        return True

    def __check_room(self, room_alias):
        try:
            self.__resolve_room_alias(room_alias)
            return True
        # TODO: Improve error handling api could should return HTTP Error
        # this should only react on 404
        except Exception:
            return False


    def convert_to_group(self, room: dict):
        group = Group(room['name'])
        group.keycloak_id = room['canonical_alias'].removeprefix("#").removesuffix(f":{self.homeserver_tag}")
        group.path = self.__group_path_from_room(room)
        
        matrix_members = room.get('members', [])
        for member in matrix_members:
            username = member.replace("@","").replace(f":{self.homeserver_tag}", "")
            group.members.append(self.get_user(username))
        return group
    
    def __group_path_from_room(self, room: dict):
        aliases = room.get('aliases',[])
        self.logger.debug("room aliases: %s", aliases)
        group_path = None
        for alias in aliases:
            if not '/' in alias:
                continue
            group_path = alias.replace("#","").replace("__"," ").replace(f":{self.homeserver_tag}", "")
        return group_path

    def __path_alias_from_group(self, group: Group):
        path_alias = self.__path_alias_from_group_path(group.path)
        self.logger.debug("path_alias %s", path_alias)
        return path_alias

    def __path_alias_from_group_path(self, group_path: str):
        path_alias = f'{group_path.replace(" ","__")}:{self.homeserver_tag}'
        return path_alias

    def __room_alias_from_group(self, group: Group): # Part of the group human-readable address "#room_alias:homeserver_tag"
        try:
            room_alias = f"{group.keycloak_id}"
        except:
            room_alias = ''
        return room_alias
    
    def __space_alias_from_group(self, group: Group): # Part of the group human-readable address "#room_alias:homeserver_tag"
        space_alias = self.__room_alias_from_group(group)
        space_alias = "space----"+ (space_alias or '')
        return space_alias

    def __room_name_from_group(self, group: Group): # The name displayed for the user in the list of available rooms
        room_name = group.group_name # from "/group/subgroup/.../subsubgroup" get "subsubgroup"
        if not group.attributes:
            return room_name
        
        if not 'title' in group.attributes:
            return room_name
        
        if group.attributes.title != '':
            room_name = group.attributes.title
            
        return room_name
    
    def __room_topic_from_group(self, group: Group): # Description of a room. In Element Web it is visible when you open the room on the top
        
        if not group.path:
            raise ValueError("Group has no path")
        room_topic = group.path.removeprefix('/').replace("/", " -> ") # from "/group/sub 1/.../sub2/sub3" get "group -> sub-1 -> ... -> sub2 -> sub3"
        return room_topic



    def create_group(self, group: Group) -> None:
        # Check for Room
        room_alias = f"#{self.__room_alias_from_group(group)}"
        if not self.__check_room(room_alias):
            self.__create_room(group)
        # Check for Space
        space_alias = f"#{self.__space_alias_from_group(group)}"
        if not self.__check_room(space_alias):
            self.__create_room(group,'m.space')

    def __create_room(self, group: Group, room_type = None) -> None:
        
        if room_type == 'm.space':
            room_alias = self.__space_alias_from_group(group)
        if room_type == None:
            room_alias = self.__room_alias_from_group(group)
        
        if self.__check_room(f"#{room_alias}"):
            self.logger.warning("Will not create Room (room_type: %s) %s because it already exists", room_type, room_alias)
            return
        
        room_name = self.__room_name_from_group(group)
        topic = self.__room_topic_from_group(group)
        self.logger.info("creating Room (room_type: %s) for Group %s \n room_alias: %s", room_type, group.path, room_alias)
        api_call = Api.room_create(self.token,RoomVisibility.private, alias=room_alias, name=room_name, 
                                topic=topic, federate=False, room_type=room_type)
        self.__make_api_call(api_call)

    def __update_room(self, room:dict, group: Group):
        room_name  = self.__room_name_from_group(group)
        room_topic = self.__room_topic_from_group(group)
        
        room_type = room.get('room_type', None)
        if not room_type:
            room_type = 'm.room'

        
        if room["name"] != room_name:
            self.logger.info(f"__update room: {room['name']} --> {room_name }")
            content = { "name":  room_name }
            api_call = Api.room_put_state(self.token, room['room_id'], event_type=f"m.room.name", body=content)
            self.__make_api_call(api_call)
        else:
            self.logger.debug(f"__update room: room_name  OK: {room_name }")
        if room["topic"] != room_topic:
            self.logger.info(f"__update topic: {room['topic']} --> {room_topic}")
            content = { "topic": room_topic, 
                            "org.matrix.msc3765.topic": [{"body": room_topic, 
                                                        "mimetype": "text/plain"}]}
            api_call = Api.room_put_state(self.token, room['room_id'], event_type=f"m.room.topic", body=content)
            self.__make_api_call(api_call)
        else:
            self.logger.debug(f"__update room: room_topic OK: {room_topic }")
        
        room_members = room.get('members', [])
        group_members = group.members
        for key,value in group.permissions.items():
            # TODO use key for permissions
            if isinstance(value, list):
                for item in value:
                    if not isinstance(item,Group):
                        continue
                    group_members.extend(item.members)
                    
        deduplicated_list = []
        
        for member in group_members:
            if member not in deduplicated_list:
                deduplicated_list.append(member)
        
        group_members = deduplicated_list

        for member in group_members:
            member_name = f"@{member.username}:{self.homeserver_tag}"
            if member_name in room_members:
                room_members.remove(member_name)
                self.logger.debug("User %s already in Room %s", member.username, group.path)
                continue
            else:
                self.logger.info("Adding User %s to Room %s", member.username, group.path)
                self.synapse_client.room_join(room['room_id'], member_name)

        if room_members:
            for member in room_members:
                
                if member == self.user:
                    continue
                
                reason = f"User is no longer in the Group {group.group_name}"
                
                title = None
                if group.attributes:
                    if 'title' in group.attributes:
                        title = group.attributes.title
                if title:
                    reason = f"User is no longer in the Group {title}"
                        
                self.logger.info("Kicking User %s from Room %s", member, group.path)
                api_call = Api.room_kick(self.token,room['room_id'], member, reason)
                self.__make_api_call(api_call)


    def sync_group(self, group: Group) -> None:
        # Check if Resources (Room+Space) exists that is related to this group:Group
        self.logger.debug("Syncing group %s", group.group_name)
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group")
        # # Get list of Resources with relevant group.keycloak_id
        # if self.use_cache:
        #     resources = [resource for resource in    self.group_cache     if (resource.keycloak_id == group.keycloak_id)]
        # else:
        #     resources = [resource for resource in    self.get_groups()    if (resource.keycloak_id == group.keycloak_id)]
        # # If any needed Group object doesn't exist -- create it
        
        
        group_exists = self.check_group(group)
        if not group_exists:
            self.logger.info('Creating group: %s', group.path)
            self.create_group(group)
        else:
            self.logger.debug('Group already exists: %s', group.path)
        
        # Update Space
        space_alias = f"#{self.__space_alias_from_group(group)}"
        space_id = self.__resolve_room_alias(space_alias)
        if space_id:
            space = self.__get_room(space_id)
            path_alias = self.__path_alias_from_group(group)
            path_alias = f'#space----{path_alias}'
            aliases = self.__get_room_aliases(space_id)
            
            if not path_alias in aliases:
                self.logger.info("Adding alias %s to %s", path_alias, space_id)
                self.__put_alias(path_alias, space_id)
            else:
                self.logger.debug("All aliases for Space are set")
            if path_alias in aliases:
                aliases.remove(path_alias)
            aliases.remove(f"{space_alias}:{self.homeserver_tag}")

            for alias in aliases:
                self.logger.info("Removing old alias %s", alias)
                api_call = Api.delete_room_alias(self.token,alias)
                self.__make_api_call(api_call)
            # Update Space and fill it with users if a Group has Subgroups or if it is a Subgroup
            if group.child_groups or group.path.count('/') > 1:
                self.logger.debug('Updating Space %s', group.path)
                self.__update_room(space,group)
            # Add Space as a child to parent Space if a Group is a Subgroup
            if group.path.count('/') > 1:
                try:
                    parent_alias = self.__path_alias_from_group_path(group.path)
                    parent_alias = parent_alias.replace(f":{self.homeserver_tag}", "").removesuffix(f'/{group.group_name}')
                    parent_alias = f"#space----{parent_alias}"
                    # parent_alias = f"#space----{group.path.removesuffix(f'/{group.group_name}')}"
                    parent_id = self.__resolve_room_alias(parent_alias)
                    # Add Space to parent Space
                    self.__space_add_child(parent_id,space_id)
                except:
                    # If a parent Space is not found, maybe it will be generated in the next sync cycle
                    self.logger.warning('No parent Space found for alias %s', parent_alias)

        # Update Room
        room_alias = f"#{self.__room_alias_from_group(group)}"
        room_id = self.__resolve_room_alias(room_alias)
        if room_id:
            room = self.__get_room(room_id)
            path_alias = self.__path_alias_from_group(group)
            path_alias = f'#{path_alias}'
            aliases = self.__get_room_aliases(room_id)

            if not path_alias in aliases:
                self.logger.info("Adding alias %s to %s", path_alias, room_id)
                self.__put_alias(path_alias, room_id)
            else:
                self.logger.debug("All aliases for Room are set")
            if path_alias in aliases:
                aliases.remove(path_alias)
            aliases.remove(f"{room_alias}:{self.homeserver_tag}")

            for alias in aliases:
                self.logger.info("Removing old alias %s", alias)
                api_call = Api.delete_room_alias(self.token,alias)
                self.__make_api_call(api_call)
            self.logger.debug('Updating Room %s', group.path)
            self.__update_room(room,group)

            # Add Room as a child to Space if a Group has Subgroups or if it is a Subgroup
            if group.child_groups or group.path.count('/') > 1:
                if space_id:
                    self.__space_add_child(space_id,room_id)

    def remove_group(self, group: Group | str):
        room_alias = None
        space_alias = None
        if isinstance(group, Group):
            if group.keycloak_id:
                room_alias  = f"#{self.__room_alias_from_group(group)}"
                space_alias = f"#{self.__space_alias_from_group(group)}" 

        if room_alias:
            room_id = self.__resolve_room_alias(room_alias)
            message = f"Group {group.path} no longer exists. Removing the Room."
            self.logger.warning(message)
            self.synapse_client.room_delete(room_id, None, None, message, False, False, False)
        else:
            self.logger.warning("Could not get room_alias for Group %s", group.path)
        
        # Delete Space if it exists
        space_id = None
        if space_alias:
            try:
                space_id =self.__resolve_room_alias(space_alias)
            except HTTPError as e:
                if not e.response.status_code == 404:
                    raise e
            if space_id:
                message = f"Group {group.path} no longer exists. Removing the Space."
                self.logger.warning(message)
                self.synapse_client.room_delete(space_id, None, None, message, False, False, False)  
        else:
            self.logger.warning("Could not get space_alias for Group %s", group.path)
        
        

    def __space_add_child(self, space_id: str, room_id: str) -> None:
        room_parent = self.__get_room(space_id)
        room_child  = self.__get_room(room_id)
        content = {"via": [self.homeserver]}
        # Make GET request at /_synapse/admin/v1/rooms/{space_id}/state
        # state = self.synapse_client.query("get", f"v1/rooms/{space_id}/state")
        state = self.synapse_client.room_state(space_id)
        state = state.get("state")
        for event in state:
              if event.get("type") == "m.space.child":
                  if event.get("state_key") == room_id:
                      self.logger.debug(" Space %s already has child %s", room_parent.get("name"), room_child.get("name"))
                      return
        # If the event was not found in the state, a Room/Space should be added as a child to this Space
        self.logger.info(" Adding child to a Space: %s <- %s", room_parent.get("name"), room_child.get("name"))
        api_call = Api.room_put_state(self.token, space_id, event_type="m.space.child",state_key=room_id, body=content)
        self.__make_api_call(api_call)

    
    def __resolve_room_alias(self, alias: str) -> str | None:
        api_call = Api.room_resolve_alias(f"{alias}:{self.homeserver_tag}")
        response = self.__make_api_call(api_call)
        if isinstance(response,dict):
            return response.get('room_id', None)
        return None
    
    def __put_alias(self, alias: str, room_id: str):
        if not alias:
            raise ValueError('alias can not be None')
        if not room_id:
            raise ValueError('room_id can not be None')
        if not isinstance(alias,str):
            raise TypeError('alias can only be of type str')
        if not isinstance(room_id,str):
            raise TypeError('room_id can only be of type str')
        
        api_call = Api.room_put_alias(self.token,alias,room_id)
        self.__make_api_call(api_call)
    
    def __get_room_aliases(self, room_id:str) -> list:
    
        
        query_parameters = {"access_token": self.token}

        path = ["rooms", room_id, "aliases"]

        api_call = ("GET", Api._build_path(path, query_parameters))
        response = self.__make_api_call(api_call)
        
        if isinstance(response, dict):
            return response.get('aliases', [])
        else:
            raise TypeError('response should contain a dict')


    ### User Operations ###
    
    def get_users(self):
        # self.logger.debug(" get_users ")
        users = []
        # TBD
        return users
    
    def check_user(self, user: User | str):
        
        user_id = None
        
        if isinstance(user, User):
            username = user.username
        
        if isinstance(user, str):
            username = user
    
        if not username:
            raise ValueError('user_id is None maybe you passed the wrong user Type')

        response_dict = self.synapse_client.user_list(None,None,False,True,username,None)
        
        if not response_dict:
            return False
        
        user_list = response_dict.get('users', None)
        
        if not user_list:
            return False
        
        user_id = f"@{username}:{self.homeserver_tag}"
        
        return user_id in [user['name'] for user in user_list if user.get('name',None)]
        
    def sync_user(self, user: User) -> None:
        self.logger.debug(" Syncing %s", user.username)
        if not isinstance(user, User):
            raise TypeError("user can only be of type User")
        
        if not self.check_user(user):
            self.logger.info("User %s does not exist. Creating...", user.username)
            self.create_user(user)
        else:
            self.logger.debug("User %s already exists. Updating...", user.username)
            self.__update_user(user)
        
    
    def create_user(self, user: User):
        if not user:
            raise ValueError('user can not be None')
        
        if not isinstance(user, User):
            raise TypeError('use can only be of type User')  
        
        # The endpoint for updating users also creates them
        # Thats why __update_user is used
        self.__update_user(user)
        
    
    def get_user(self, user: str | User) -> User:
        
        username = None
        if not user:
            raise ValueError('user can not be None')
        
        if isinstance(user, User):
            username = user.username
            
        if isinstance(user, str):
            username = user
            
        if not username:
            raise ValueError('could not get username from user')
        
        matrix_id = f'@{username}:{self.homeserver_tag}'
        
        user_details = self.synapse_client.user_details(matrix_id)
        
        if not user_details:
            raise ValueError('response is empty')
        
        enabled = not user_details.get('deactivated', False)
        
        external_ids = user_details.get('external_ids')
        keycloak_id = None
        if external_ids:
            filtered_ids = [external_id for external_id in external_ids if external_id['auth_provider'] == 'oidc']
            if filtered_ids:
                keycloak_id = filtered_ids[0]['external_id']
        user = User(username, email=None,keycloak_id=keycloak_id, enabled=enabled)
        
        return user
        
    def __update_user(self, user: User):
        
        if not user:
            raise ValueError('user can not be None')
        
        if not isinstance(user, User):
            raise TypeError('use can only be of type User')
            
        threepid = [{
            "medium": "email",
            "address": user.email
            }]
        
        external_ids = [{
            "auth_provider": "oidc",
            "external_id" : user.keycloak_id
        }]
        
        user_id = f"@{user.username}:{self.homeserver_tag}"
        
        user_details = {'displayname': f"{user.first_name} {user.last_name}",
                        'threepids': threepid,
                        'external_ids' : external_ids
                        }

        matrix_user_details = self.synapse_client.user_details(user_id)

        if matrix_user_details:
            self.logger.debug("__update_user: %s \n User: %s \n matrix_user_details: %s \n", user.username, user, matrix_user_details)
            for key in user_details:
                self.logger.debug("__update_user  check   %s   for   %s", key, user.username)

                if not key in matrix_user_details:
                    matrix_user_details[key] = None
                    compare_result = False
                else:
                    if matrix_user_details[key] == None:
                        compare_result = False

                    if isinstance(matrix_user_details[key], str):
                        compare_result   =   user_details[key] == matrix_user_details[key]

                    if isinstance(matrix_user_details[key], list):
                        compare_result = True
                        for item in user_details[key]:
                            compare_result = compare_result * (item in matrix_user_details[key])
                        
                    if isinstance(matrix_user_details[key], dict):
                        compare_result   =   set(user_details[key].items()).issubset(set(matrix_user_details[key].items()))

                if not compare_result:
                    self.logger.debug("\n  %s \n  -> \n  %s\n\n", matrix_user_details[key], user_details[key])
                    #The modify api is also used to create Users
                    # src: https://matrix-org.github.io/synapse/latest/admin_api/user_admin_api.html#create-or-modify-account
                    self.synapse_client.query("put", "v2/users/{user_id}", data={key: user_details[key]}, user_id=user_id)


    ### General Operations
    
    def __make_api_call(self, api_call: tuple) -> dict:
        if not api_call:
            raise ValueError('api_call can not be None')
        
        # unpack the Tuple
        if len(api_call) == 2:
            method, endpoint = api_call
            data = None
        else:
            method, endpoint, data = api_call
        
        response = requests.request(method, self.homeserver + endpoint, data= data, timeout=10)
        
        response.raise_for_status() 
        if response.ok:
            try:
                json_data = response.json()
            except ValueError:
                self.logger.warning('api_call %s returned no json data', api_call)

            if json_data:
                return json_data
        
        return {}
    
