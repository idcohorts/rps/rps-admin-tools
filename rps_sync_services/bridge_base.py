#########################
# Tulsky V.A. 2023-2024 #
# Hamacher E. 2023-2024 #
#########################
"""This module provides the Brigde class"""
import logging
import os
import re
import json
import requests
from .data_model import Group, User, Role

# Init Root - Logger
logging.basicConfig()


class Bridge:
    """ This is the Bridge Base class which all Bridge implementations should inherit from
        to ensure compatability"""
    
    def __init__(self, config:dict|None = None):
        
        if not config:
            
            config = dict(os.environ)
        
        self.service_name = "ServiceName"

        self.config = config
        self.ignore_groups         = json.loads(self.config.get('SYNC_IGNORE_GROUPS', "[]"))
        self.ignore_users          = json.loads(self.config.get('SYNC_IGNORE_USERS', "[]"))
        self.ignore_user_patterns  = json.loads(self.config.get('SYNC_IGNORE_USER_REGEX_PATTERNS', "[]"))
        self.ignore_group_patterns = json.loads(self.config.get('SYNC_IGNORE_GROUP_REGEX_PATTERNS', "[]"))
        
        self.alerta_api_url           = self.config.get('ALERTA_API_URL', None)
        self.alerta_api_key           = self.config.get('ALERTA_API_KEY', None)
        self.alerta_alert_environment = self.config.get('ALERTA_ALERT_ENVIRONMENT', None)
        self.alerta_alert_service     = self.config.get('ALERTA_ALERT_SERVICE', None)
        self.alerta_alert_resource    = self.config.get('ALERTA_ALERT_RESOURCE', None)
        
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(config.get('LOGGING_LEVEL', logging.DEBUG))

    ### Creating alerts ###

    def send_alert(self, 
                   environment: str | None      = None,
                   severity: str                = "informational",
                   status: str                  = "closed",
                   event: str                   = "Sync",
                   service: list[str] | None    = None,
                   resource: str | None         = None,
                   text_field: str              = "") -> None:
        """Sending an HTTP POST request to the Alerta service to create an alert

        Args:
            environment (str, optional): Defaults to None, is set to self.alerta_alert_environment in code below.
            severity (str, optional): Defaults to "informational".
            status (str, optional): Defaults to "closed".
            service (list[str], optional): Defaults to None. If None, is set to [alerta_alert_service, self.service_name] in code below.
            resource (str, optional): Defaults to None. If None, is set to self.alerta_alert_resource in code below.
            text_field (str, optional): Defaults to "".

        Returns:
            None"
        """

        if environment is None:
            environment = self.alerta_alert_environment
        if service is None:
            service = [self.alerta_alert_service, self.service_name]
        if resource is None:
            resource = self.alerta_alert_resource

        json_payload = {
            "environment": environment,
            "severity":    severity,
            "status":      status,
            "event":       event,
            "service":     service,
            "resource":    resource,
            "text":        text_field
            }
        headers = {
            "Authorization": f"Key {self.alerta_api_key}",
            "Content-type": "application/json"
            }
        
        if not self.alerta_api_url:
            self.logger.warning("""Tried to send message to Alerta,
                                but ALERTA_API_URL is not configured""")
            return
        if not self.alerta_api_key:
            self.logger.warning("""Tried to send message to Alerta,
                                but ALERTA_API_KEY is not configured""")
            return
        if not self.alerta_alert_environment:
            self.logger.warning("""Tried to send message to Alerta,
                                but ALERTA_ALERT_ENVIRONMENT is not configured""")
            return
        if not self.alerta_alert_service:
            self.logger.warning("""Tried to send message to Alerta,
                                but ALERTA_ALERT_SERVICE is not configured""")
            return
        if not self.alerta_alert_resource:
            self.logger.warning("""Tried to send message to Alerta,
                                but ALERTA_ALERT_RESOURCE is not configured""")
            return    
    
        if severity in ["ok", "normal", "informational"]:
            self.logger.info("Sending request to Alerta")
        else:
            self.logger.warning("Sending request to Alerta")

        try:
            # response: some useful methods are ".json()", ".text()", ".status_code
            response = requests.post(self.alerta_api_url, headers=headers,
                                     json=json_payload, timeout=5)
            
        except requests.ConnectionError as e:
            self.logger.warning('Got an connection error for the alerta_api_url %s',
                                self.alerta_api_url)
            self.logger.error(e)
            return
        
        self.logger.debug("Alert request response.status_code: %s", response.status_code)
        if response.status_code != 201:
            self.logger.warning('self.alerta_api_url: %s', self.alerta_api_url)
            self.logger.warning('self.alerta_api_key: %s', self.alerta_api_key)
            self.logger.warning("Alert request response.status_code: %s", response.status_code)
            self.logger.warning("Alert request response.content: %s", response.content)

    ### Group Operations ###
   
    def get_groups(self) -> list[Group]:
        """ Get all groups from the service
            Returns:
                list[Group]: All groups of the service
        """
        raise NotImplementedError("The get_groups function is not implemented in your Bridge")
    
    def get_group(self) -> Group | None:
        """ Get a specific Group from the service
            Returns:
                Group: The group object
                None: if no group was found
        """
        raise NotImplementedError("The get_group function is not implemented in your Bridge")

    def check_group(self, group: Group) -> bool:
        """ Check if a Group exist
            Returns:
                bool: True if the group was found, false if not
        """
        raise NotImplementedError("The check_group function is not implemented in your Bridge")
    
    def create_group(self, group: Group) -> None:
        """ Create a Group inside service
            Returns: None
        """
        raise NotImplementedError("The create_group function is not implemented in your Bridge")

    def remove_group(self, group:Group) -> None:
        """ Remove a Group from service
            Returns: None
        """
        raise NotImplementedError("The remove_group function is not implemented in your Bridge")

    def sync_group(self, group) -> Group | None:
        """ Synchronise a Group with the Service
            Returns:
                Group: an updated Group object
        """
        raise NotImplementedError("The sync_group function is not implemented in your Bridge")
    
    ### User Operations ###
    
    def get_users(self) -> list[User]:
        """ Get all users from service
            Returns:
                list[User]: All users of the service
        """
        raise NotImplementedError("The get_users function is not implemented in your Bridge")
    
    def get_user(self) -> User | None:
        """ Get a specific user from service
            Returns:
                User: The user object
                None: if no user was found
        """
        raise NotImplementedError("The get_user function is not implemented in your Bridge")
    
    def check_user(self, user:User) -> bool:
        """ Check if a User exist
            Returns:
                bool: True if user was found, false if not
        """
        raise NotImplementedError("The check_user function is not implemented in your Bridge")
    
    def create_user(self, user:User) -> None:
        """ Create a User inside service
            Args:
                user (User): The user which will be created
            Returns: None
        """
        raise NotImplementedError("The create_user function is not implemented in your Bridge")
    
    def remove_user(self, user:User) -> None:
        """ Remove a User from service
            Args:
                user (User): The user which will be removed
            
            Returns: None
        """
        raise NotImplementedError("The remove_user function is not implemented in your Bridge")
    
    def sync_user(self, user) -> User | None:
        """ Synchronise a User with the Service
            Args:
                user (User): The user which will be synced
            Returns:
                User: an updated user object
        """
        raise NotImplementedError("The sync_user function is not implemented in your Bridge")
    
    ### Role Operations ###
    
    def get_roles(self) -> list[Role]:
        """Gets all roles from service

        Returns:
            list[Role]: All roles of the service
        """
        return []
    
    
    def get_role(self, role: str) -> Role | None:
        """Get specific role from service

        Args:
            role (str): name of the role

        Returns:
            Role: The Role object
            None: if no Role was found
        """
        return None
    
    
    def check_role(self, role: str) -> bool:
        """Check if a role exist in service

        Args:
            role (str): name of the role

        Returns:
            bool: True if role was found, False if not
        """
        return False
    
    def create_role(self, role: Role) -> None:
        """Create a role in service from Role object
        Args:
            role (Role): The role which will be created
        Returns: None
        """
        
        return None
    
    def remove_role(self, role: Role) -> None:
        """Remove a role from a service

        Args:
            role (Role): The role which will be removed

        Returns: None
        """
        
    def sync_role(self, role:Role) -> Role | None:
        """Synchronise a role with the Service

        Args:
            role (Role): The role which will be synced

        Returns: 
            Role: an updated version of the Role
        """
    
    
    ### Data Model Conversion ###
    
    def convert_to_user(self, user: dict) -> User | None:
        """ Converts a dict to User object
            Returns:
                User: The user object
        """
        return None
   
    def convert_to_group(self, group: dict) -> Group | None:
        """ Converts a dict to Group object
            Args:
                group (dict): The group as a dictionary
            Returns:
                Group: The group object
        """
        return None
    
    
    def convert_to_role(self, role: dict) -> Role | None:
        """_summary_

        Args:
            role (dict): The role as a dictionary

        Returns:
            Role: The role object
        """
        return None
    
    ### Sync ignore ###
    
    def check_ignore_user(self, username: str) -> bool:
        """Check if a user should be ignored

        Args:
            username (str): The username which will be checked

        Raises:
            ValueError: Raises a ValueError if username is None
            TypeError: Raises a TypeError if username is not of type str

        Returns:
            bool: True if the user should be ignored / False if not
        """
        if not username:
            raise ValueError('username can not be None')
        
        if not isinstance(username, str):
            raise TypeError('username can only be of type str')
        
        if username in self.ignore_users:
            return True
        
        for pattern in self.ignore_user_patterns:
            if not pattern or pattern == '':
                continue
            
            if re.search(pattern, username):
                return True
        
        return False
    
    
    ### Sync with other Bridge ###
    def sync_with_bridge(self, bridge: 'Bridge', cleanup:bool = True) -> None:
        """Sync current bridge with another bridge (Sync services)

        Args:
            bridge (Bridge): The second bridge to sync with
        """
        self.logger.info("---> Sync Users <---")
        self.sync_users_with_bridge(bridge, cleanup)
        
        self.logger.info("---> Sync Roles <---")
        self.sync_roles_with_bridge(bridge, cleanup)
        
        self.logger.info("---> Sync Groups <---")
        self.sync_groups_with_bridge(bridge, cleanup)


    def sync_users_with_bridge(self, bridge: 'Bridge', cleanup:bool = True) -> None:
        """Sync current bridge with another bridge (Sync services)
           Creates the users, deletes the users, does not change their memberships

        Args:
            bridge (Bridge): The second bridge to sync with
        """
        users = bridge.get_users()
        
        for user in users:
            self.sync_user(user)
        
        if cleanup:
            self.logger.info('Cleanup of users')
            self.cleanup_users(users_to_keep=users)

    def sync_groups_with_bridge(self, bridge: 'Bridge', cleanup:bool = True) -> None:
        """Sync current bridge with another bridge (Sync services)
           Creates Resources, adds users to them, deletes users from them, deletes Recources

        Args:
            bridge (Bridge): The second bridge to sync with
        """
        groups = bridge.get_groups()
        for group in groups:
            self.sync_group(group)
        
        if cleanup:
            self.logger.info('Cleanup of groups')
            self.cleanup_groups(groups_to_keep=groups)
        
    def sync_roles_with_bridge(self, bridge: 'Bridge', cleanup:bool = True)-> None:
        """Sync Roles from another Bridge(Service) to this one.
        Args:
            bridge (Bridge): The second bridge to sync with
        """
        roles = bridge.get_roles()
        for role in roles:
            self.sync_role(role)
        
        if cleanup:
            self.logger.info('Cleanup of roles')
            self.cleanup_roles(roles_to_keep=roles)
    
    def cleanup_users (self, users_to_keep:list[User]) -> None:
        current_users:list[User] = self.get_users()
        usernames: list [str] = [user.username for user in users_to_keep]
        
        
        users_to_remove = [user for user in current_users if not user.username in usernames]

        users_to_remove_formatted = "\n".join(user.username for user in users_to_remove)
        self.logger.info("These users will be removed: %s", users_to_remove)
        if users_to_remove:
            message = f"There are users in {self.service_name} but not in Keycloak: \n{users_to_remove_formatted}"
            # self.send_alert(severity = "major", status = "open", text_field = message)
            # Raise error and exit the process
            raise RuntimeError(message)
        # for user in users_to_remove:
        #     self.remove_user(user)

    def cleanup_groups(self, groups_to_keep: list[Group]) -> None:
        """Remove the Resources from the Service that has no relevant Groups to be synchronized with

        Args:
            groups_to_keep list[Group]: List of groups to ignore in the cleanup
        """
        current_groups:list[Group] = self.get_groups()
        
        groups_to_keep_paths:list[str] = [group.path for group in groups_to_keep if group.path]
        
        groups_to_remove = [group for group in current_groups if not group.path in groups_to_keep_paths]
        self.logger.info("These groups will be removed: %s", groups_to_remove)
        
        for group in groups_to_remove:
            self.remove_group(group)
    
    def cleanup_roles(self, roles_to_keep: list[Role]) -> None:
        """Remove the roles that not present in roles_to_keep

        Args:
            roles_to_keep (list[Role]): List of Roles to keep
        """
        current_roles: list[Role] = self.get_roles()
        
        rolenames: list[str] = [role.name for role in roles_to_keep if role]
        
        roles_to_remove = [role for role in current_roles if not role.name in rolenames]
        
        self.logger.info("These roles will be removed: %s",roles_to_remove)
        
        for role in roles_to_remove:
            self.remove_role(role)
