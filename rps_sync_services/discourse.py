import random
from functools import reduce
import requests
import time
import itertools


from pydiscourse import DiscourseClient, exceptions

from .data_model import Group, User

import os

class DiscourseBridge:
    GROUP_KEYS = ["id", "name", "display_name", "members"]
    IGNORE_GROUPS = ["trust_level_0", "trust_level_1", "trust_level_2", "trust_level_3", "trust_level_4", "staff",
                     "moderators", "admins"]
    IGNORE_USERS = ["discobot", "system", "admin", "admin1", "dev"]

    def __init__(self, config: dict = os.environ):
        
        self.service_name = "Discourse"

        self.discourse_url = config.get('DISCOURSE_URL')
        self.discourse_username = config.get('DISCOURSE_USERNAME')
        self.discourse_api_key = config.get('DISCOURSE_API_KEY')
        
        discourse_client = DiscourseClient(
            self.discourse_url,
            api_username=self.discourse_username,
            api_key=self.discourse_api_key)

        self.discourse_client = discourse_client
        self.users = {}
    
    # GROUPS

    def get_groups(self, verbose=False, ignore_members=True):
        try:
            # Get all groups from discourse
            groups = self.discourse_client.groups()

            discourse_groups = {}
            for group in groups:
                # Skip Discourse Defaults' Auto Groups
                if group["name"] in self.IGNORE_GROUPS:
                    continue
                # Get only useful keys and their values

                discourse_group = group
                if not verbose:
                    discourse_group = {
                        k: v for k, v in group.items() if k in self.GROUP_KEYS}

                # Get members from the group
                if not ignore_members:
                    members = self.get_members_from_group(group['name'])
                    discourse_group["members"] = members

                discourse_groups[group["name"]] = discourse_group

            return discourse_groups
        except Exception as e:
            print(e)
            return []


    def sync_role(self, source_group, role_name, members, verbose=False):
        group_name = ((source_group.path[1:]).replace("/", "-")).replace(" ", "")
        group_name = f"{group_name}_{role_name}"
        # Get Discourse Group
        discourse_group = None
        try:
            discourse_group = self.get_group_by_name(role_name)
        except Exception as e:
            if verbose:
                print("[Discourse-Sync] Group for role {} doesn't exist in Discourse".format(
                    role_name))
                print(e)
            pass

        if not discourse_group:
            # Create group in Discourse from Source
            self.discourse_client.create_group(name=role_name)
            if verbose:
                print("[Discourse-Sync] Create group for role {} in discourse".format(
                    role_name))
            discourse_group = self.get_group_by_name(role_name)

        # get current members
        current_members = self.get_members_from_group(role_name)

        for user in members:
            if verbose:
                print("[Discourse-Sync] Add {} to role {}".format(
                    user.username, role_name))
            if user.username not in current_members:
                self.add_member_to_group(discourse_group["id"], user.username)
                if verbose:
                    print("[Discourse-Sync] Added {} to role {}".format(
                    user.username, role_name))
            else:
                if verbose:
                    print("[Discourse-Sync] {} already member of role {}".format(
                    user.username, role_name))


    def sync_group(self, source_group, verbose=False):
        # Compare Discourse group with the given Source Group
        # If the group doesn't exist in Discourse Group then Create the group
        # try:
        # Get groupname from Group Object which is only required for discourse.
        group_name = ((source_group.path[1:]).replace("/", "-")).replace(" ", "")

        # Get Discourse Group
        discourse_group = None
        try:
            discourse_group = self.get_group_by_name(group_name)
        except Exception as e:
            if verbose:
                print("[Discourse-Sync] Group {} doesn't exist in Discourse".format(
                    group_name))
                print(e)
                
            pass
        # Create Discourse Group from Keycloak Group if the group doesn't exist!
        if not discourse_group:
            # Create group in Discourse from Source
            self.discourse_client.create_group(name=group_name)
            if verbose:
                print("[Discourse-Sync] Create group {} in discourse".format(
                    group_name))
            discourse_group = self.get_group_by_name(group_name)

        # Get members_to_add or to_remove from discourse group!
        diff_dict = self.compare_members_with_source_group(
            discourse_group, source_group)

        for username in diff_dict["Add"]:
            if verbose:
                print("[Discourse-Sync] Add {} to group {}".format(
                    username, group_name))
            try:
                self.add_member_to_group(discourse_group["id"], username)
            except Exception as e:
                if verbose:
                    print("[Discourse-Sync] Failed to add {} to group {} ({})".format(
                        username, group_name, discourse_group))
                    print(e)
                pass

        for username in diff_dict["Delete"]:
            if verbose:
                print("[Discourse-Sync] Delete {} from group {}".format(
                    username, group_name))
            try:
                self.remove_member_from_group(discourse_group["id"], username)
            except Exception as e:
                if verbose:
                    print("[Discourse-Sync] Failed to delete {} from group {}".format(
                        username, group_name))
                    print(e)
                pass

        return diff_dict

        # except Exception as error:
        #    raise error
        #    return None

    def get_group_by_name(self, group_name, ignore_members=False) -> dict:
        try:
            discourse_group = self.discourse_client.group(group_name)
            # it returns a dictionary with "group" "extras" as keys
            # get group only
            group = discourse_group["group"]

            # Flag for getting user from the group
            members = []
            if not ignore_members:
                members = self.get_members_from_group(group_name)
                group["members"] = members

            return group
        except Exception as error:
            print(error)
            raise error
            return {}

    def get_members_from_group(self, group_name):
        if group_name:
            try:
                discourse_members = {}
                offset = 0
                while True:
                    offset_members = self.discourse_client.group_members(group_name, offset=offset)
                    for member in offset_members:
                        discourse_members[member["username"]] = member["id"]
                    if len(offset_members) < 50:
                        break
                    offset += 50  # increment offset by 50 each time
                return discourse_members
            except Exception as e:
                print(e)
                return []
        return []

    def add_member_to_group(self, group_id, username, verbose=False):
        try:
            if not self.users:
                print("[Discourse-Sync] get all users")
                self.get_all_discourse_users()
            user_id = self.users[username]
            
            if verbose:
                print("[Discourse-Sync] add member to group", group_id, username)
                
            # User should be a Group Object (from Keycloak)
            # where group is just a discourse group
            if user_id:
                json = self.discourse_client.add_group_members(
                    groupid=group_id,
                    usernames=[username]
                )
                
                if verbose:
                    print("[Discourse-Sync] add member to group client feedback:", json)
                
                return True
            return False
        except Exception as error:
            print(error)
            raise error
            return False


    def remove_member_from_group(self, group_id, username):
        try:
            if not self.users:
                self.get_all_discourse_users()
            user_id = self.users[username]
            if user_id:
                self.discourse_client.delete_group_member(
                    groupid=group_id,
                    userid=user_id
                )
                return True
        except Exception as error:
            print(error)
            raise error
            return False



    def compare_members_with_source_group(self, discourse_group, source_group) -> dict:
        # Compare Discourse Group members with Source (Keycloak) Group members
        # Create a "to be added" and "to be removed" members list for discourse group
        discourse_members = discourse_group["members"]

        if type(source_group) is Group:
            # source group as Group Class
            source_members = source_group.get_members(username_id_pair=True)
            source_members = [username for username in source_members.keys()]
        else:
            # source group as List
            source_members = source_group["members"]

        # Members to be added into Discourse Group
        members_to_add = [
            source_username
            for source_username in source_members if source_username not in discourse_members.keys()
        ]

        members_to_remove = [
            discourse_username
            for discourse_username in discourse_members if discourse_username not in source_members
        ]

        return {
            "Add": members_to_add,
            "Delete": members_to_remove
        }


    def send_discourse_api_get(self, path):
        url = self.discourse_url + path
        url = url.strip()

        headers = {
            "Api-Key": self.discourse_api_key,
            "Api-Username": self.discourse_username,
        }
        
        retry_count = 4

        print("Discourse request URL:", url, headers)
        while retry_count > 0:
            print("request number", retry_count)
            response = requests.request("GET", url, headers=headers)
            if response.ok:
                print("request alright")
                break
            else:
                retry_count = retry_count-1
                
                msg=""
                try:
                    msg = u",".join(response.json()["errors"])
                except (ValueError, TypeError, KeyError):
                    if response.reason:
                        msg += response.reason
                        msg += u"{0}: {1}".format(response.status_code, response.text)
                        
                    else:
                        msg = u"{0}: {1}".format(response.status_code, response.text)
                    print(msg)
                time.sleep(retry_count)

        try:
            decoded = response.json()
        except Exception as e:
            print("Get discourse users error", e)
        
        return decoded

    
    # USERS
    def get_all_discourse_users(self):
        users = []
        for page in itertools.count(start=0):
            userlist = self.send_discourse_api_get(f"/admin/users/list/new.json?page={page}")
            print(userlist)
            if not userlist:
                break
            for user in userlist:
                users.append(user)
                print(f"{user['id']:5} {user['username']}")
        
        users = {user["username"]: user["id"]
                 for user in users if user["username"] not in self.IGNORE_USERS}
        # Save all discourse users in instances
        self.users = users
        
        return users

    def get_user(self, username):
        try:
            discourse_user = self.discourse_client.user(username)
            return discourse_user
        except:
            print("User {} doesn't exist in Discourse!".format(username))
            return {}

    def create_or_update_user(self, user, verbose=False, update_user=False):
        if user:
            try:
                discourse_user = None
                try:
                    discourse_user = self.discourse_client.user(user.username)
                    if not update_user:
                        return True
                except:
                    if verbose:
                        print("User {} doesn't exist in Discourse!".format(user.username))
                # Display Name
                display_name = ""
                if user.first_name and user.last_name:
                    display_name = user.first_name + " " + user.last_name
                elif user.first_name:
                    display_name = user.first_name
                elif user.last_name:
                    display_name = user.last_name

                if not discourse_user:
                    # Create User
                    # Random Password
                    random_pwd = '%010x' % random.randrange(16 ** 10)

                    # Creating Discourse User
                    discourse_user = self.discourse_client.create_user(
                        display_name,
                        user.username,
                        user.email,
                        random_pwd,
                        active=True)
                    if verbose:
                        print("User {} is created!".format(user.username))
                else:
                    # Update user
                    # TODO Email will not be updated here. Need to change some API or User admin settings
                    discourse_user = self.discourse_client.update_user(
                        user.username,
                        name=display_name,
                        email=user.email,
                    )
                return True
            except Exception as error:
                print(error)
                raise error
                return False
        return False

    def remove_user(self, userid):
        if userid:
            self.discourse_client.delete_user(userid)
            return True
        else:
            return False

    # Categories

    def get_all_categories(self):
        categories = self.discourse_client.get_site_info()
        return categories

    def get_categories_in_dict(self):
        discourse_categories = self.get_all_categories()
        existing_discourse_categories = {}
        for category in discourse_categories["categories"]:
            existing_discourse_categories[category["name"]] = category["id"]

        return existing_discourse_categories

    def update_category(self, source_group, permissions, is_child=False, verbose=False):
        if verbose:
            print("[Discourse-Sync] Updating category {}".format(source_group.group_name))
        if source_group:
            group_name = ((source_group.path[1:]).replace("/", "-")).replace(" ", "")
            if len(group_name) > 50:
                # if contains - use the last part and the first part
                if verbose:
                    print(f"[Discourse-Sync] Group name of {group_name} is too long.")
                    print(f"[Discourse-Sync] Shortening group name to {group_name.split('-')[0]}-{group_name.split('-')[-1]}")
                group_name = group_name.split("-")[0] + "--" + group_name.split("-")[-1]

            perm_dict = self.get_permissions_as_dict(permissions)
            if verbose:
                print(f"[Discourse-Sync] Updated Permissions for category {group_name}: {perm_dict}")
            updated_discourse_categories = self.get_categories_in_dict()
            category_id = updated_discourse_categories[group_name]
            if is_child:
                # need parent_id
                # example path: /napkon/hap/Group 2
                parent_group_name = (source_group.path[1:].split(source_group.group_name))[0].replace("/", "-").replace(" ", "")[:-1]
                parent_id = updated_discourse_categories[parent_group_name]
                # self, name, color, text_color="FFFFFF", permissions=None, parent=None, **kwargs
                category = self.discourse_client.update_category(
                    category_id,
                    name=group_name,    # name
                    color="49d9e9",               # color
                    text_color="f0fcfd",               # text_color
                    permissions=perm_dict,            # permissions
                    parent_category_id=parent_id                  # parent
                )
                if verbose:
                    print("[Discourse-Sync] Updated sub-category {}".format(source_group.group_name))

                return True
            else:
                # self, name, color, text_color="FFFFFF", permissions=None, parent=None, **kwargs
                category = self.discourse_client.update_category(
                    category_id,
                    name=group_name,    # name
                    color="49d9e9",               # color
                    text_color="f0fcfd",               # text_color
                    permissions=perm_dict             # permissions
                )
                if verbose:
                    print("[Discourse-Sync] Updated category {}".format(source_group.group_name))

                return True
        return False

    def create_category(self, source_group, permissions, is_child=False, verbose=False):
        if source_group:
            group_name = ((source_group.path[1:]).replace("/", "-")).replace(" ", "")
            if len(group_name) > 50:
                # if contains - use the last part and the first part
                if verbose:
                    print(f"[Discourse-Sync] Group name of {group_name} is too long.")
                    print(f"[Discourse-Sync] Shortening group name to {group_name.split('-')[0]}-{group_name.split('-')[-1]}")
                group_name = group_name.split("-")[0] + "--" + group_name.split("-")[-1]

            perm_dict = self.get_permissions_as_dict(permissions)
            print(f"[Discourse-Sync] Creation Permissions for category {group_name}: {perm_dict}")

            if is_child:
                # need parent_id
                # example path: /napkon/hap/Group 2
                parent_group_name = (source_group.path[1:].split(source_group.group_name))[0].replace("/", "-").replace(" ", "")[:-1]
                print(parent_group_name)
                updated_discourse_categories = self.get_categories_in_dict()
                # self, name, color, text_color="FFFFFF", permissions=None, parent=None, **kwargs
                category = self.discourse_client.create_category(
                    group_name,    # name
                    "49d9e9",               # color
                    "f0fcfd",               # text_color
                    perm_dict,            # permissions
                    parent_group_name                  # parent
                )
                if verbose:
                    print("[Discourse-Sync] Created sub-category {}".format(source_group.group_name))

                return True
            else:
                # self, name, color, text_color="FFFFFF", permissions=None, parent=None, **kwargs
                category = self.discourse_client.create_category(
                    group_name,    # name
                    "49d9e9",               # color
                    "f0fcfd",               # text_color
                    perm_dict             # permissions
                )
                if verbose:
                    print("[Discourse-Sync] Created category {}".format(source_group.group_name))

                return True
        return False
    
    def get_permissions_as_dict(self, permissions):
        #simple_permissions = {
        #    "see": 3,
        #    "see_reply": 2,
        #    "see_reply_create": 1
        #}

        perm_dict = dict()

        guest_groups = permissions["guest_groups"]
        member_groups = permissions["member_groups"]
        management_groups = permissions["management_groups"]
        guest_roles = permissions["guest_roles"]
        member_roles = permissions["member_roles"]
        management_roles = permissions["management_roles"]

        # for every group inside member_groups, add the group to the permissions dict with the value 2
        for group in guest_groups:
            full_group_name = ((group.path[1:]).replace("/", "-")).replace(" ", "")
            perm_dict[full_group_name] = 3

        for group in member_groups:
            full_group_name = ((group.path[1:]).replace("/", "-")).replace(" ", "")
            perm_dict[full_group_name] = 1

        for group in management_groups:
            full_group_name = ((group.path[1:]).replace("/", "-")).replace(" ", "")
            perm_dict[full_group_name] = 1

        for role in guest_roles:
            perm_dict[role] = 3

        for role in member_roles:
            perm_dict[role] = 1

        for role in management_roles:
            perm_dict[role] = 1
        
        return perm_dict



def get_discourse_groups_map(discourse_client):
    # Return Discourse groups as dict in form of {name:id}
    discourse_groups = discourse_client.groups()
    return reduce(
        lambda m, g: {**m, **{g['name']: g['id']}}, discourse_groups, dict())


def get_members_from_discourse_group(discourse_client, group_name):
    discourse_members = dict()
    offset = 0
    r = {'not': 'empty'}
    while len(r) > 0:
        # discourse_group_name
        r = discourse_client.group_members(group_name, offset=offset)
        # print(r)
        discourse_members = reduce(
            lambda m, g: {**m, **{g['username']: g['id']}}, r, discourse_members)
        offset = len(discourse_members)

    return discourse_members


def get_all_discourse_users(discourse_client):
    discourse_users = dict()
    r = {'not': 'empty'}
    i = 1
    while len(r) > 0:
        r = discourse_client.users(filter='all', page=i)
        discourse_users = reduce(
            lambda m, g: {**m, **{g['username']: g['id']}}, r, discourse_users)
        i += 1
    return discourse_users

def set_discourse_group(discourse_client, group_config):
    try:
        group_name = group_config["name"]
    except:
        print("name missing in group config", group_config)
    group_title = group_config.get("title", group_name)
    try:
        discourse_client.create_group(name=group_name)
        print("create group {} in discourse".format(group_name))
        discourse_group = discourse_client.group(group_name)
        group_id = discourse_group['group']["id"]
        return group_id
    except:
        print("failed to create group {} in discourse".format(group_name))


def set_members_to_discourse_group(discourse_client, group_name, group_id, other_service_members):
    discourse_members = set(get_members_from_discourse_group(
        discourse_client, group_name).keys())

    members_to_add = other_service_members - discourse_members
    members_to_remove = discourse_members - other_service_members

    print("{} members to add to discourse group {}".format(
        len(members_to_add), group_name))
    print("{} members to delete to discourse group {}".format(
        len(members_to_remove), group_name))

    # import in chunks of 1000
    # for members_to_add in zip(*[iter(members_to_add)]*1000):
    for members_to_add in zip(*[iter(members_to_add)]*1):
        if len(members_to_add) > 0:
            discourse_client.add_group_members(
                groupid=group_id,
                usernames=list(members_to_add)
            )

    for member_to_remove in members_to_remove:
        user = discourse_client.user(member_to_remove)
        user_id = user["id"]
        discourse_client.delete_group_member(
            groupid=group_id,
            userid=user_id
        )

    return (len(members_to_add), len(members_to_remove))
