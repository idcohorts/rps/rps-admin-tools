import random
import xml.etree.ElementTree as ET
import owncloud
from owncloud import ResponseError
from .data_model import Group, User, Role
from .bridge_base import Bridge



# Userd for groupfolder api
OCS_SERVICE_APPS = '/apps'

class NextcloudBridge(Bridge):
    """This Bridge is used to access the Nextcloud API
    API Docs: https://docs.nextcloud.com/server/latest/developer_manual/client_apis/index.html"""
    
    def __init__(self, config: dict|None = None):

        # Init BridgeBase
        super().__init__(config)

        self.service_name = "Nextcloud"

        nextcloud_url: str = self.config['NEXTCLOUD_URL']
        self.nextcloud_username = self.config['NEXTCLOUD_ADMIN_USER']
        self.nextcloud_password = self.config['NEXTCLOUD_ADMIN_PASS']
        
        # Used for caching
        self.user_cache: list[User] = []
        self.group_cache: list[Group] = []
        self.folder_cache: list[dict] = []
        self.use_cache = self.config.get('USE_CACHE', 'True').lower() in ('true', '1')
        
        
        # Init Nextcloud Client
        nextcloud_client = owncloud.Client(nextcloud_url)
        try:
            nextcloud_client.login(self.nextcloud_username, self.nextcloud_password)
            self.nextcloud_client = nextcloud_client
        except Exception as e:
            raise ConnectionError(e)

        # Check if groupfolders app is installed
        self.group_folders_enabled = False
            
        apps = self.get_app_data()
        if apps:
            if 'groupfolders' in apps.keys():
                self.group_folders_enabled = apps.get('groupfolders', False)
            else:
                self.group_folders_enabled = False
            
        if not self.group_folders_enabled:
            self.logger.info("The groupfolders app is not installed or deactivated."+
                             " Groupfolders won't be created")
        

        self.server_version = self.nextcloud_client.get_version()
        self.logger.info("Initialized Bridge and established connection with Nextcloud server")
        self.logger.info("Nextcloud server version is : %s" ,self.server_version)


    def __group_cache_append(self, group: Group):
        """Append a group to the group_cache

        Args:
            group (Group): The Group which will be added to the cache
        """
        
        if not self.use_cache:
            return

        cached_groups = [cached_group for cached_group in self.group_cache 
                         if cached_group.path == group.path]
        if cached_groups:
            self.group_cache.remove(cached_groups[0])
        self.group_cache.append(group)


    def __user_cache_append(self, user: User):
        """Append a user to the user_cache

        Args:
            user (User): The User which will be added to the cache
        """
        
        if not self.use_cache:
            return
        
        cached_users = [cached_user for cached_user in self.user_cache 
                        if cached_user.username == user.username]
        if cached_users:
            self.user_cache.remove(cached_users[0])
        self.user_cache.append(user)


    def __folder_cache_append(self, folder: dict):
        """Append a user to the user_cache

        Args:
            user (User): The User which will be added to the cache
        """
        
        if not folder:
            return
        
        if not self.use_cache:
            return
        
        cached_folders = [cached_folder for cached_folder in self.folder_cache 
                        if cached_folder.get('mount_point', None) == folder.get('mount_point','')]
        if cached_folders:
            self.folder_cache.remove(cached_folders[0])
        self.folder_cache.append(folder)

    
    # Refresh Nextcloud Client Session
    def refresh_session(self):
        """This refreshes the nextcloud session (prevents session timeout)"""
        self.nextcloud_client.login(self.nextcloud_username, self.nextcloud_password)


    def get_app_data(self) -> dict:
        """Returns a dict of all of the installed apps and if they are enabled

        Returns:
            dict: all installed apps
        """
        return self.nextcloud_client.get_apps()

    ### Group Operations ###

    def get_groups(self):
        
        self.logger.debug('Getting groups')
        
        self.refresh_session()
        nextcloud_groups = self.nextcloud_client.get_groups()
        groups = []
        for groupname in nextcloud_groups:
            if not groupname:
                continue
            
            members = self.get_group_members(groupname)
            group: Group | None = self.convert_to_group({"groupname": groupname,'members': members})
            if not group:
                continue
            groups.append(group)
            self.__group_cache_append(group)
             
        return groups


    def get_group(self, group_name: str) -> Group | None:
        
        # This executes if the group name is given
        path = self.__get_group_path_from_group_name(group_name)
            
        if self.use_cache:
            cached_groups = [group for group in self.group_cache if group.path == path]
            if cached_groups:
                return cached_groups[0]
        nextcloud_groups = self.__ocs_get_group(group_name)

        groups = []
        for groupname in nextcloud_groups:
            
            if not groupname:
                continue
            if groupname in self.ignore_groups:
                continue
            
            members = self.get_group_members(groupname)
            group: Group | None = self.convert_to_group({"groupname" : groupname, 'members': members})
            groups.append(group)

        searched_groups = [group for group in groups if group.path == path]
        if searched_groups:
            self.__group_cache_append(searched_groups[0])
            return searched_groups[0]
        
        return None

    def get_group_members(self, groupname: str) -> list[User]:
        """ Get members of a given Group

        Args:
            groupname (str): name of a Group

        Raises:
            ValueError: if the groupname is not a str

        Returns:
            list[User]: The members of the group
        """
        if not isinstance(groupname, str):
            raise ValueError("groupname can only be of type str, but received %s", type(groupname))

        nextcloud_members = self.nextcloud_client.get_group_members(groupname)


        members = []
        for member in nextcloud_members:
            user = self.get_user(member)
            
            if not user:
                continue
            
            members.append(user)
        return members


    def check_group(self, group: Group) -> bool:
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group, but received %s", type(group))
            
        group_name = self.__get_group_name_from_group(group)
        
        if not group_name:
            raise ValueError("group has no group_name attribute. group: %s", group)
        
        return self.nextcloud_client.group_exists(group_name)


    def remove_group(self, group: Group) -> bool:
        
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group, but received %s", type(group))

        group_name = self.__get_group_name_from_group(group)
        
        group_folder: dict | None = None
        if self.group_folders_enabled and group.path:
            group_folder = self.__get_group_folder(group.path)
        
        if group_folder:
            self.__remove_all_groups_from_groupfolder(group_folder)
        
        self.logger.info("removing group: %s", group_name)
        if not group_name:
            return False
        
        if not self.check_group(group):
            return True

        return self.nextcloud_client.delete_group(group_name)


    def convert_to_user(self, user: dict) -> User | None:
        
        username = user.get('id')
        if not username:
            return None
        
        email = user.get('email')
        
        enabled = user.get('enabled', '0') == '1'
        
        user_obj = User(username=username, email=email, enabled=enabled)
        return user_obj
   
    def convert_to_group(self, group: dict) -> Group | None:
        if not group:
            return None
        if not 'groupname' in group.keys():
            return None
        
        groupname = group['groupname']
        if not groupname:
            return None
        
        if '[Role]' in groupname:
            return None
        
        path = self.__get_group_path_from_group_name(groupname)
        normal_name = path.split('/')[-1]
        members = group.get('members',[])
        
        if path in self.ignore_groups:

            self.logger.info("skipping group %s", path)
            return None
    
        if groupname in self.ignore_groups or normal_name in self.ignore_groups:

            self.logger.info("skipping group %s", path)
            return None
        
        

        return Group(group_name=normal_name, path=path, members=members)

    
    def convert_to_role(self, role):
        rolename = role.get('name', None)
        
        if not rolename:
            return None
        
        members = role.get('members', None)
        
        return Role(rolename,members,None)

    
    def create_group(self, group: Group) -> None:
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group, but received %s", type(group))
        
        group_name = self.__get_group_name_from_group(group)
        self.logger.info("Creating group %s", group_name)
        self.nextcloud_client.create_group(group_name=group_name)


    def sync_group(self, group: Group) -> None:
        
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group, but received %s", type(group))
        if not group.path:
            raise ValueError("group must have path attribute. group: %s", group)
        
        self.logger.debug("Syncing Group %s", group.group_name)

        permissions = group.get_permissions()
        base_path = group.path
        group_name = self.__get_group_name_from_group(group)
        
        self.refresh_session()
        group_exist = self.check_group(group)
        
        if not group_exist:
            self.logger.debug("Group does not exist: %s", group.path)
            self.create_group(group)
        else:
            self.logger.debug("Group exists: %s", group.path)
        group_exist = self.check_group(group)

        if not isinstance(group.members, list):
            raise TypeError("Members of group can only be of type list, but received %s", type(group.members))
        
        # get current members of group
        current_members = self.nextcloud_client.get_group_members(group_name)

        # add new members to group
        for member in group.members:
            if member.username in current_members:
                current_members.remove(member.username)
            else:
                
                
                
                user_exists = self.check_user(member)
                
                if not user_exists:
                    self.logger.info("User %s does not exist. Skipping for now. This user will be synced in the next run",
                                     member.username)
                    continue
                
                self.logger.info("Adding %s to group %s", member.username, group_name)
                self.nextcloud_client.add_user_to_group(member.username, group_name)
        # remove old members from group
        if current_members:
            for member in current_members:
                self.logger.info("Removing %s from group %s", member, group_name)
                self.nextcloud_client.remove_user_from_group(member, group_name)
        if group_exist and self.group_folders_enabled and not "management" in group_name:
            self.__create_group_folder(permissions, base_path=base_path, group=group)


    def __get_group_path_from_group_name(self, group_name: str):
        group_path = group_name
        group_path = group_path.replace("_.", "/").replace("__", " ")
        if not group_path[0] == "/":
            group_path = "/"+group_path
        return group_path


    def __get_group_name_from_group_path(self, group_path: str):
        group_name = group_path
        if group_name[0] == "/":
            group_name = group_name[1:]
        group_name = group_name.replace("/", "_.").replace(" ", "__")
        return group_name


    def __get_group_name_from_group(self, group: Group):
        if not group.path:
            raise ValueError("group must have path attribute. group: %s", group)
        return self.__get_group_name_from_group_path(group.path)


    def __get_group_folder(self, name: str) -> dict | None:
        
        # If group_folders app is not enabled do nothing
        if not self.group_folders_enabled:
            return None
        
        cached_folders = [cached_folder for cached_folder in self.folder_cache 
                        if cached_folder.get('mount_point', '') == name]
        if cached_folders:
            return cached_folders[0]
        
        self.refresh_session()
        folders = self.__ocs_get_group_folders()
        if not folders:
            return None
        
        for folder in folders:
            self.__folder_cache_append(folder)
        
        for folder in folders:
            if folder["mount_point"] == name:
                return folder
        return None


    def __create_group_folder(self, permissions, base_path: str, group: Group) -> None:
        
        # If group_folders app is not enabled do nothing
        if not self.group_folders_enabled:
            return
        if not base_path:
            raise ValueError('base_path can not be None')
        if not isinstance(group, Group):
            raise TypeError("group can only be of type Group, but received %s", type(Group))
        
        path = base_path
        
        self.refresh_session()
        
        group_folder = self.__get_group_folder(path)
        if not group_folder:
            self.__ocs_create_group_folder(path)
            group_folder = self.__get_group_folder(path)
        if group_folder:
            groups_with_perm = group_folder.get('groups',[])
            perm_dict = self.__get_permissions_as_dict(permissions)
            for group_name, permission in perm_dict.items():
                self.logger.info("Set permission %s for %s in groupfolder %s",
                                    permission, group_name, group_folder['id'])
                
                group_in_group_folder = None
                
                if groups_with_perm:
                    for group_perm in groups_with_perm:
                        if group_perm['group_id'] == group_name:
                            group_in_group_folder = group_perm
                            groups_with_perm.remove(group_perm)
                
                if not group_in_group_folder:
                    # give group access to folder with basic permission    
                    self.__ocs_set_group_folder_group(group_folder["id"], group_name)
                    group_in_group_folder = {'group_id' : group_name, 'permissions': '31'}
                
                
                # modify permissions for group
                if group_in_group_folder['permissions'] != str(permission):
                    self.logger.info("Permission for folder : %s  Group : %s " +
                                        "changed from %s to %s",
                                        group_folder['mount_point'], group_name, 
                                        group_in_group_folder['permissions'], permission)
                    
                    self.__ocs_set_group_folder_group_permission(group_folder["id"], 
                                                                group_name, permission)
                else:
                    self.logger.info("Permission for folder : %s Group : %s " +
                                        "stayed the same",
                                        group_folder['mount_point'], group_name)                        

            if groups_with_perm:
                for group in groups_with_perm:
                    self.logger.info("Removing %s from %s", 
                                        group['group_id'], group_folder['id'])
                        
                    self.__ocs_remove_group_from_groupfolder(folder_id=group_folder['id'], 
                                                            group_name=group['group_id'])


    def __get_permissions_as_dict(self, permissions) -> dict:
        #   simple_permissions = {
        #    "group-1": 3,
        #    "see_reply": 2,
        #    "see_reply_create": 1
        #   }

        perm_dict = {}
        if not permissions:
            return perm_dict
        
        guest_groups = permissions.get("guest_groups",[])
        member_groups = permissions.get("member_groups",[])
        management_groups = permissions.get("management_groups",[])
        guest_roles = permissions.get("guest_roles",[])
        member_roles = permissions.get("member_roles",[])
        management_roles = permissions.get("management_roles", [])

        # for every group inside member_groups, add the group to the permissions dict with the value 2
        for group in guest_groups:
            if group:
                full_group_name = self.__get_group_name_from_group(group)
                perm_dict[full_group_name] = 1

        for group in member_groups:
            if group:
                full_group_name = self.__get_group_name_from_group(group)
                perm_dict[full_group_name] = 15

        for group in management_groups:
            if group:
                full_group_name = self.__get_group_name_from_group(group)
                perm_dict[full_group_name] = 31

        for role in guest_roles:
            if role:
                rolename = f"[Role]{role}"
                perm_dict[rolename] = 1

        for role in member_roles:
            if role:
                rolename = f"[Role]{role}"
                perm_dict[rolename] = 15

        for role in management_roles:
            if role:
                rolename = f"[Role]{role}"
                perm_dict[rolename] = 31

        return perm_dict


    ### Methods for Groupfolders App ###

    def __ocs_create_group_folder(self, folder_name):

        self.nextcloud_client.OCS_BASEPATH = ''
        res = self.nextcloud_client._make_ocs_request(
            'POST',
            OCS_SERVICE_APPS,
            'groupfolders/folders',
            data={"mountpoint": folder_name,
                  }
        )

        self.nextcloud_client.OCS_BASEPATH = 'ocs/v1.php/'

        if res.status_code == 200:
            return res


    def __ocs_set_group_folder_group(self, folder_id, group_name):

        self.nextcloud_client.OCS_BASEPATH = ''
        res = self.nextcloud_client._make_ocs_request(
            'POST',
            OCS_SERVICE_APPS,
            f'groupfolders/folders/{folder_id}/groups',
            data={"group": group_name}
        )

        self.nextcloud_client.OCS_BASEPATH = 'ocs/v1.php/'

        if res.status_code == 200:
            return res


    def __ocs_set_group_folder_group_permission(self, folder_id, group_name, permission):

        self.nextcloud_client.OCS_BASEPATH = ''
        res = self.nextcloud_client._make_ocs_request(
            'POST',
            OCS_SERVICE_APPS,
            f'groupfolders/folders/{folder_id}/groups/{group_name}',
            data={"permissions": permission}
        )

        self.nextcloud_client.OCS_BASEPATH = 'ocs/v1.php/'

        if res.status_code == 200:
            return res


    def __ocs_remove_group_from_groupfolder(self, folder_id: int, group_name: str) -> None:
        self.nextcloud_client.OCS_BASEPATH = ''
        self.nextcloud_client._make_ocs_request(
            'DELETE',
            OCS_SERVICE_APPS,
            f'groupfolders/folders/{folder_id}/groups/{group_name}'
        )
        self.nextcloud_client.OCS_BASEPATH = 'ocs/v1.php/'


    def __remove_all_groups_from_groupfolder(self, group_folder: dict):
        
        for group in group_folder.get('groups', []):
            group_id = group.get('group_id', '')
            
            if not group_id:
                continue
            self.__ocs_remove_group_from_groupfolder(group_folder['id'], group_id)


    def __ocs_get_group(self, groupname:str):
        """Get groups via provisioning API.
        If you get back an error 999, then the provisioning API is not enabled.

        :returns: list of groups
        :raises: HTTPResponseError in case an HTTP error status was returned

        """
        res = self.nextcloud_client._make_ocs_request(
            'GET',
            self.nextcloud_client.OCS_SERVICE_CLOUD,
            f'groups?search={groupname}'
        )

        if res.status_code == 200:
            tree = ET.fromstring(res.content)
            groups = [x.text for x in tree.findall('data/groups/element')]

            return groups
        return []


    def __ocs_get_group_folders(self) -> list:

        self.nextcloud_client.OCS_BASEPATH = ''
        res = self.nextcloud_client._make_ocs_request(
            'GET',
            OCS_SERVICE_APPS,
            'groupfolders/folders'
        )
        self.nextcloud_client.OCS_BASEPATH = 'ocs/v1.php/'

        if res.status_code == 200:
            tree = ET.fromstring(res.content)
            group_folders = []
            for x in tree.findall('data/element'):

                child_dict = {}

                for child in x:
                    if child.tag and child.text:
                        child_dict[child.tag] = child.text

                child_dict['groups'] = []
                groups = x.find('groups')
                
                for group in groups.findall('element'):
                    child_dict['groups'].append(group.attrib)

                group_folders.append(child_dict)
            return group_folders
        return []


    ### Role Operations ###

    def sync_role(self, role: Role):
        
        if not role:
            raise ValueError('role can not be None')
        
        if not isinstance(role, Role):
            raise TypeError('role can only be of type Role')
        
        self.refresh_session()
        
        rolename = f"[Role]{role.name}"
        role_exist = self.check_role(role)
        if not role_exist:
            self.logger.info("Creating role %s", rolename)
            role_exist = self.nextcloud_client.create_group(group_name=rolename)
            
        else:
            self.logger.debug("Role %s already exist... updating permissions", rolename)

        # get current members of group
        current_members = self.nextcloud_client.get_group_members(rolename)

        for user in role.members:
            if user.username not in current_members:
                
                user_exists = self.check_user(user)
                
                if not user_exists:
                    self.logger.info("User %s does not exist. Skipping for now. This user will be synced in the next run",
                                     user.username)
                    continue
                
                self.logger.info("Adding %s to role %s", user.username, rolename)
                self.nextcloud_client.add_user_to_group(user.username, rolename)

            else:
                self.logger.info("%s already member of role %s", user.username, rolename)

            if user.username in current_members:
                current_members.remove(user.username)

        # check if there are members left in current_members
        # if yes, remove them from the group
        if current_members:
            for member in current_members:
                self.logger.info("Removing %s from role %s", member, rolename)
                self.nextcloud_client.remove_user_from_group(member, rolename)

    
    def get_role(self, rolename: str):
        if not isinstance(rolename, str):
            raise TypeError("rolename can only be of type str")

        role_group = self.get_group(f"[Role]{rolename}")
        if not role_group:
            return None
        
        role_dict:dict = {'name':role_group.group_name.replace('[Role]','')}
        
        if role_group.members:
            role_dict['members'] = role_group.members
            
        return self.convert_to_role(role_dict)


    def get_roles(self):
        
        self.logger.debug('Getting roles')
        
        role_names = self.__ocs_get_group('[Role]')
        roles = []
        for role_name in role_names:
            if not isinstance(role_name, str):
                continue
            if '[Role]' in role_name:
                rolename = role_name.replace('[Role]', '')
                members = self.get_group_members(role_name)
                role = self.convert_to_role({'name' : rolename, 'members': members})
                roles.append(role)
                
        return roles


    def check_role(self, role: str | Role) -> bool:
        
        rolename = None
        
        if isinstance(role, Role):
            rolename = f"[Role]{role.name}"
        
        if isinstance(role, str):
            rolename = f"[Role]{role}"
        
        if not rolename:
            return False
        
        return self.nextcloud_client.group_exists(rolename)


    def remove_role(self, role: Role):
        
        if not role:
            return
        
        if not isinstance(role, Role):
            raise TypeError("role parameter can only be of type Role")
        
        rolename = f"[Role]{role.name}"
        
        self.nextcloud_client.delete_group(rolename)


    ### User Operations ###

    def check_user(self, user: User, usecache = True) -> bool:
        username = None
        if isinstance(user, User):
            username = user.username
            
        if not username:
            return False
            

        
        if self.use_cache and usecache:
            cached_users = [user for user in self.user_cache if user.username == username]
            if cached_users:
                return True
        

        return self.nextcloud_client.user_exists(username)
            
        return False


    def sync_user(self, user: User) -> None:

        self.logger.debug("Syncing User %s", user.username)

        if not isinstance(user, User):
            raise TypeError("user can only be of type User")
        
        if not user.email:
            raise ValueError("user must have email attribute. user: %s", user)
        
        self.create_user(user)
        if user.groups:
            for group_path in user.groups:
                groupname = self.__get_group_name_from_group_path(group_path)
                self.nextcloud_client.add_user_to_group(user.username, groupname)


    def get_user(self, username, usecache = True) -> User | None:
        
        if not username:
            raise ValueError("username can not be None")
        
        self.logger.debug('Getting user %s', username)
        
        if self.check_ignore_user(username):
            self.logger.info("skipping user %s", username)
            return
        
        if self.use_cache and usecache:
            cached_users = [user for user in self.user_cache if user.username == username]
            if cached_users:
                return cached_users[0]
        
        self.refresh_session()
        try:
            user = self.nextcloud_client.get_user(username)
            if user:                
                user_obj = self.convert_to_user(user)
                if not user_obj:
                    return
                
                self.__user_cache_append(user_obj)
                return user_obj
        except ResponseError as e:
            if e.status_code == 404:
                self.logger.debug('Got 404 for user %s', username)
                return None
            
            raise e
    
    def get_users(self):
        
        self.logger.debug('Getting users')
        
        self.refresh_session()
        usernames = self.nextcloud_client.get_users()
        
        users = []
        for username in usernames:
            user_obj = self.get_user(username)
            if user_obj:
                users.append(user_obj)
                
        return users
    
    def create_user(self, user):

        if not isinstance(user, User):
            raise TypeError("user can only be of type User")
        
        if self.nextcloud_client.user_exists(user.username):
            self.logger.debug("User already exists: %s", user.username)
            return True
        
        self.logger.info("Creating user: %s", user.username)
        
        random_pwd = '%010x' % random.randrange(16 ** 10)
        self.nextcloud_client.create_user(user.username, random_pwd)
        if user.email:
            self.nextcloud_client.set_user_attribute(user.username, "email", user.email)
        

    def remove_user(self, user: User | str):
        
        if not user:
            raise ValueError("user can not be None")
        
        username: str = ""

        if isinstance(user, User):
            username = user.username
        if isinstance(user, str):
            username = user

        user_exist = self.nextcloud_client.user_exists(username)
        if not user_exist:
            return False
        
        return self.nextcloud_client.delete_user(username)
    