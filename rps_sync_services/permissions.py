import json
from rps_sync_services.data_model import Group, User, Role
from rps_sync_services.keycloak import KeycloakBridge
from rps_sync_services.bridge_base import Bridge
from .custom_keycloak_admin import CustomKeycloakAdmin


class PermissionBridge(KeycloakBridge):
    """This Bridge is used to access the Keycloak authorization API
    API Docs: https://www.keycloak.org/docs-api/24.0.1/rest-api/index.html#Authorization"""
    def __init__(self, config: dict|None = None):
        super(PermissionBridge, self).__init__(config)
        
        
        keycloak_url: str = self.config['KEYCLOAK_URL']
        keycloak_realm: str = self.config['KEYCLOAK_REALM']
        keycloak_client_id: str = self.config['KEYCLOAK_CLIENT_ID']
        keycloak_secret: str = self.config['KEYCLOAK_SECRET']
        
        self.keycloak_client = CustomKeycloakAdmin(server_url=keycloak_url,
                                        client_id=keycloak_client_id,
                                        realm_name=keycloak_realm,
                                        client_secret_key=keycloak_secret)
        
        self.authz_client = {}
        
        # Setting authz client
        auth_client_name: str | None = self.config.get('KEYCLOAK_AUTHZ_CLIENT', None)
        if not auth_client_name:
            raise ValueError("KEYCLOAK_AUTHZ_CLIENT is not defined in env")
        self.switch_authz_client(auth_client_name)

        
        # Set authz settings
        policy_enforcement = self.config.get("KEYCLOAK_POLICY_ENFORCEMENT_MODE","ENFORCING")
        decision_strategy = self.config.get("KEYCLOAK_DECISION_STRATEGY","AFFIRMATIVE")
        self.logger.info("Setting authz setting POLICY_ENFORCEMENT_MODE: %s, DECISION_STRATEGY: %s",
                         policy_enforcement, decision_strategy)
        self.set_authz_settings(policy_enforcement, decision_strategy)
        
    def sync_with_bridge(self, bridge: 'Bridge', cleanup=False) -> None:
        self.logger.warning("This Bridge will only sync groups")
        self.logger.info("---> Sync Groups <---")
        self.sync_groups_with_bridge(bridge, cleanup)

        self.send_alert(severity = "ok",
                        status = "closed",
                        text_field = f"{self.service_name} <-> {bridge.service_name} sync finished."
                        )
    
    def cleanup_groups(self,groups_to_keep):
        self.logger.warning("cleanup groups was called, this Bridge should never cleanup groups")
    
    def sync_user(self, user: User, ignore_roles=True, ignore_groups=False, use_cache=True):
        raise RuntimeError("The PermissionBridge should never sync a user")
    
    def sync_role(self, role: Role) -> Role | None:
        raise RuntimeError("The PermissionBridge should never sync a role")

    def sync_group(self, group, create_if_not_found=True):
        self.logger.info("Current group: %s", group.path)
        
        # Check for attributes and create the corresponding permissions and policies
        inherit = False

        if 'inherit' in group.attributes.keys():
            inherit = group.attributes.inherit.lower() == 'true'
        else:
            if group.path.count('/') > 1:
                inherit = True

        
        member_groups = []
        if 'member_groups' in group.attributes.keys():
            member_group_names = group.attributes.member_groups
            
            if not isinstance(member_group_names, list):
                member_group_names = [member_group_names]
            
            for member_group_name in member_group_names:
                if not member_group_name:
                    continue
                if member_group_name[:1] == "/":
                    member_group = self.get_group_by_path(path=member_group_name)
                    member_groups.append(member_group)
                else:
                    member_group = self.get_group_by_name(name=member_group_name, parent=group)
                    member_groups.append(member_group)


        if member_groups:
            scope = ['view-members', 'view']
            # Group-to-Group means that,
            # if you're in perm_groups you can access group_to_apply but only the specified scope
            # In this case we give permission to the member_groups to view the group
            self.create_group_to_group_rule(group_to_apply=group,perm_type='member_groups', perm_groups=member_groups, scopes=scope, inherit = False)
        else:
            name = f'[member_groups]-access-[{group.path}]-perm'
            self.remove_polices_from_permission(name)


        if 'member_roles' in group.attributes.keys():
            member_roles = group.attributes.member_roles
            roles = []
            if isinstance(member_roles, list):
                for member_role in member_roles:
                    role = self.get_role_by_name(member_role)
                    roles.append(role)
            else:
                role = self.get_role_by_name(member_roles)
                roles = [role]
            if roles:
                scope = ['view-members', 'view']

                self.create_role_to_group_rule(group, roles, 
                                               scope, role_type='member_roles')

        else:
            name = f'[member_roles]-access-[{group.path}]-perm'
            self.remove_polices_from_permission(name)
        roles = []
        if 'manage_roles' in group.attributes.keys():
            manage_roles = group.attributes.manage_roles
            if isinstance(manage_roles,list):
                for manage_role in manage_roles:
                    role = self.get_role_by_name(manage_role)
                    roles.append(role)
            else:
                role = self.get_role_by_name(manage_roles)
                roles = [role]

        if roles or inherit:
            scope = ['view-members', 'manage-members', 'manage-membership', 'manage', 'view']
            # Role-to-Group means that,
            # if you're in one the roles given to the function you will have access to the group,
            # but only the specified scope
            self.create_role_to_group_rule(group, roles, scope,
                                           role_type='manage_roles', 
                                           inherit = inherit)
        else:
            name = f'[manage_roles]-access-[{group.path}]-perm'
            self.remove_polices_from_permission(name)
        # Get the management group
        management_groups = []

        if 'managed_by' in group.attributes.keys():
            management_group_names = group.attributes.managed_by
            
            if not isinstance(management_group_names, list):
                management_group_names = [management_group_names]
            
            for management_group_name in management_group_names:
                if not management_group_name:
                    continue
                
                if management_group_name[:1] == "/":
                    management_group = self.get_group_by_path(path=management_group_name)
                    management_groups.append(management_group)
                else:
                    management_group = self.get_group_by_name(name=management_group_name, 
                                                              parent=group)
                    management_groups.append(management_group)


        if management_groups or inherit:
            scope = ['view-members', 'manage-members', 'manage-membership', 'manage', 'view']
            # Group-to-Group means that,
            # if you're in perm_groups you can access group_to_apply but only the specified scope
            # In this case we give permission to the management_group to access the group
            self.create_group_to_group_rule(group_to_apply=group, perm_groups=management_groups,
                                            scopes=scope, inherit = inherit)
        else:
            name = f'[management_group]-access-[{group.path}]-perm'
            self.remove_polices_from_permission(name)

            


        # Group-to-Group
        # if the user is a member of the Group (or in one of its child_groups),
        # they can access the Group itself

        scope = ['view-members', 'view']
        self.create_group_to_group_rule(group_to_apply=group, perm_groups=[group],
                                        scopes=scope, perm_type='self')
    


    # Set authz_client, which is needed for almost every operation in this class
    def switch_authz_client(self, client_name:str) -> bool:
        """Set the authz client, which will have all polices / permissions

        Args:
            client_name (str): the name of client used for authz endpoints

        Raises:
            ValueError: if client_name is None

        Returns:
            bool: True if successfull, False if not
        """
        if not client_name:
            raise ValueError("Cant switch authz client if client_name is None")
        
        clients = self.keycloak_client.get_clients()
        for client in clients:
            if not client:
                continue
            if not isinstance(client, dict):
                continue
            
            if client.get('clientId') == client_name:
                self.authz_client = client
                return True
        return False

    def set_authz_settings(self, policy_enforcement: str, decision_strategy:str):
        """Set the authz setting for the selected client
            Docs: https://www.keycloak.org/docs/latest/authorization_services/index.html#_resource_server_overview

        Args:
            policy_enforcement (str): Policy enforcement setting
            decision_strategy (str): Decision strategy setting

        Returns:
            _type_: _description_
        """
        if not self.authz_client:
            return False
        client_id = self.authz_client.get('id')

        payload = \
            {"policyEnforcementMode": policy_enforcement,
             "decisionStrategy": decision_strategy,
             "allowRemoteResourceManagement": False}

        self.keycloak_client.update_resource_server(client_id, payload)

    # Not used, but still useful
    def enable_authorization(self):
        """Enable authorization for client"""
        
        if not self.authz_client:
            return
        
        client_id = self.authz_client.get('id')
        enabled = self.authz_client.get('authorizationServicesEnabled')
        
        # If already enabled skip next steps
        if enabled:
            return
    
        self.authz_client['authorizationServicesEnabled'] = True
        self.authz_client['serviceAccountsEnabled'] = True
        self.authz_client['publicClient'] = False
        self.keycloak_client.update_client(client_id, self.authz_client)

    def get_resource_by_name(self, name: str, only_return_id: bool = False) -> dict | str | None:
        """Get a Resource from the resource-server/client by its name

        Args:
            name (str): name of the resource
            only_return_id (bool, optional): If true only its internal id will be returned. Defaults to False.

        Raises:
            ValueError: _description_

        Returns:
            str: If a resource was found and only the id should be returned (only_return_id = True)
            dict: If a resource was found and should be fully returend (only_return_id = False)
            None: If no resource was found
        """
        if not name:
            raise ValueError("Cant get Resource if name is None")
        
        client_id = self.authz_client.get('id')
        resources = self.keycloak_client.get_client_authz_resources(client_id)
        if not resources:
            return None
        
        for resource in resources:
            if not resource:
                continue
            
            if not isinstance(resource,dict):
                continue
            
            if resource.get('name') == name:
                if only_return_id:
                    return resource.get('_id')
                return resource

    def get_scope_by_name(self, name: str, only_return_id: bool=False) -> dict | str | None:
        """Get a ResourceScope by its name

        Args:
            name (str): name of the scope
            only_return_id (bool, optional): If true only its internal id will be returned. 
            Defaults to False.

        Returns:
            str: If a scope was found and only the id should be returned (only_return_id = True)
            dict: If a scope was found and should be fully returend (only_return_id = False)
            None: If no scope was found
        """
        
        if not name:
            raise ValueError("Cant get Scope if name is None")        
        client_id = self.authz_client.get('id')
        scopes = self.keycloak_client.get_client_authz_scopes(client_id)
        for scope in scopes:
            if not scope:
                continue
            if not isinstance(scope, dict):
                continue
            
            if scope.get('name') == name:
                if only_return_id:
                    return scope.get('id')
                return scope
        if not scopes:
            default_group_scopes = [{'name': 'manage'}, 
                                    {'name': 'view'}, 
                                    {'name': 'manage-membership'},
                                    {'name': 'manage-members'}, 
                                    {'name': 'view-members'}]
            for scope in default_group_scopes:
                self.keycloak_client.create_client_authz_scopes(client_id, scope)
            # retry
            return self.get_scope_by_name(name, only_return_id)
        return None


    def create_permission(self, perm_name: str, scope_ids:list, resources:list, policies:list):
        """

        Args:
            perm_name (str): Name of the permission
            scope_ids (list): The ids of the scope which will be granted
            resources (list): Resource which the scopes will be granted for
            policies (list): Policies which decide if access is granted

        Raises:
            ValueError: If policies is None
            ValueError: If perm_name is None
        """

        if not policies:
            raise ValueError("Cant create permission if policies is None or empty")
        if not perm_name:
            raise ValueError("Cant create permission if perm_name is None")
        
        permission_struct = {'decisionStrategy': 'AFFIRMATIVE',
                                'logic': 'POSITIVE',
                                'name': perm_name,
                                'type': 'scope',
                                'scopes': scope_ids,
                                'resources': resources,
                                'policies': policies}
        client_id = self.authz_client.get('id')
        self.keycloak_client.create_client_authz_scope_based_permission(client_id, 
                                                                        permission_struct)
        self.logger.info("%s | CREATED", perm_name)

    def create_group_based_policy(self, policy_name: str, groups:list, 
                                  extend_children:bool=True) -> str:
        """Create a Policy based on a Group / multiple Groups

        Args:
            policy_name (str): Name of the policy
            groups (list): Groups which will be evaluated in the policy
            extend_children (bool, optional): if children and their members will be evaluated too

        Raises:
            ValueError: If policy_name is None
            ValueError: If groups is None

        Returns:
            str: returns policy_id if the policy is created successfully
        """
        
        if not policy_name:
            raise ValueError("Cant create policy if policy_name is None")
        
        if not groups:
            raise ValueError("Cant create policy if groups is None")
        
        pol_groups = [{'id': group.keycloak_id, 'extendChildren': extend_children} for group in groups if group]
        
        policy_structure = {'groups': pol_groups,
                            'decisionStrategy': 'UNANIMOUS',
                            'description': 'autogenerated',
                            'logic': 'POSITIVE',
                            'name': policy_name,
                            'type': 'group'}
        client_id = self.authz_client.get('id')
        policy_resp = self.keycloak_client.create_client_authz_group_based_policy(client_id, 
                                                                                  policy_structure,
                                                                                  skip_exists=True)
        
        self.logger.info("%s | CREATED", policy_name)

        policy_id = policy_resp.get("id")
        if not policy_id:
            raise ValueError('Could not retrieve policy_id after creating the policy')
        
        return policy_id

    def create_role_based_policy(self, policy_name: str, roles: list) -> str:
        """ Create a Policy based on a Roles / multiple Roles

        Args:
            policy_name (str): Name of the policy
            roles (list): Role which will be evaluated in the policy

        Raises:
            ValueError: If policy_name is None
            ValueError: If roles is None

        Returns:
            str: returns policy_id if the policy is created successfully
        """
        
        if not policy_name:
            raise ValueError("Cant create policy if policy_name is None")
        
        if not roles:
            raise ValueError("Cant create policy if roles is None")
        pol_roles = [{'id': role.get('id')} for role in roles]
        policy_structure = {'roles': pol_roles,
                            'decisionStrategy': 'UNANIMOUS', 
                            'description': 'autogenerated', 
                            'logic': 'POSITIVE',
                            'name': policy_name, 'type': 'role'}
        client_id = self.authz_client.get('id')
        policy_resp = self.keycloak_client.create_client_authz_role_based_policy(client_id,
                                                                                policy_structure,
                                                                                skip_exists=True)
        self.logger.info("%s | CREATED", policy_name)
        
        policy_id = policy_resp.get("id")
        if not policy_id:
            raise ValueError('Could not retrieve policy_id after creating the policy')
        return policy_id

    def create_role_to_group_rule(self, group_to_apply: Group, roles: list=[], 
                                  scopes: list=[], role_type: str="", inherit: bool=True):
        """Creates a policy based on roles and creates a Permissions with said policy 
            and applies it to a specified group

        Args:
            group_to_apply (Group): Group which the Permission will be applied for
            roles (list, optional): Roles which will be evaluated in the policy. Defaults to [].
            scopes (list, optional): Scopes which will be granted. Defaults to [].
            role_type (str, optional): is only used for inheritance and naming. Defaults to "".
            inherit (bool, optional): If True the group will inherit manage_roles from its parent. 
            Defaults to True.

        Raises:
            ValueError: If scopes is None
            ValueError: role_type is None
            ValueError: If Group has no path Attribute
        """
        if not scopes:
            raise ValueError("Cant create a role to Group rule if scopes is None")
        if not role_type:
            raise ValueError("Cant create a role to Group if rule_type is None")
        
        # Activate fine-grained-permissions for Group
        # This will create the Group resource
        self.keycloak_client.group_set_permissions(group_to_apply.keycloak_id)
        
        authz_scopes = []
        for scope in scopes:
            authz_scope = self.get_scope_by_name(scope)
            if authz_scope:
                authz_scopes.append(authz_scope)
        scope_ids = [authz_scope.get("id") for authz_scope in authz_scopes]
        group_id = group_to_apply.keycloak_id

        # check if resource exist
        resource_id = self.get_resource_by_name(f'group.resource.{group_id}', only_return_id=True)

        # if not create resource
        if not resource_id:
            resource_name = f'group.resource.{group_id}'
            resource_id = self.create_resource(resource_name, 'Group', scopes, full_scope=True)
        policy_name = f'[{role_type}]-access-[{group_to_apply.path}]-policy'

        policies = []

        policy = self.get_policy_by_name(policy_name)
        if policy:
            policy_id = policy.get('id')
            policies.append(policy_id)
            if roles:
                self.update_role_policy(policy_id, policy_name, roles, policy)
        else:
            if roles:
                policy_id = self.create_role_based_policy(policy_name, roles)
                policies.append(policy_id)

        # Check if group will inherit from its parent
        if role_type == 'manage_roles' and inherit:
            # Split path into pieces to recreate the path step by step
            # By doing this were going through the different levels of the group tree
            if not group_to_apply.path:
                raise ValueError("Cant inherit from parent if group_to_apply has no path")
            
            path_pieces = group_to_apply.path.split('/')
            group_path = ''
            for path_piece in path_pieces:
                policy = {}
                if path_piece:
                    # If policy exist in parent path, then apply it to the child
                    group_path = f'{group_path}/{path_piece}'
                    policy_name = f'[{role_type}]-access-[{group_path}]-policy'
                    policy = self.get_policy_by_name(policy_name)

                if policy:
                    policies.append(policy.get('id'))

        perm_name = f'[{role_type}]-access-[{group_to_apply.path}]-perm'
        permission = self.get_permission_by_name(perm_name)
        if permission:
            permission_id = permission.get('id')
            self.update_permission(permission_id, perm_name, scope_ids, [resource_id], policies, permission)
            return
        if policies:
            self.create_permission(perm_name, scope_ids, [resource_id], policies)

    def create_resource(self, name:str, resource_type:str,
                        scopes:list, full_scope:bool = False) -> str:
        """Creates a Resource in the resource-server/ client

        Args:
            name (str): Name of the resource
            resource_type (str): Type of resource used for Grouping and 
            Permission that apply to resource_types
            
            scopes (list): Scopes which are available for the resource
            full_scope (bool, optional): If all possible scopes should be applied to the Resource.
            Defaults to False.

        Raises:
            ValueError: If name is None
            ValueError: If resource_type is None
            ValueError: If scopes is None and full_scope is False
            ValueError: If resource_id could not be retrieved after creating the resource

        Returns:
            str: the id of the new Resource
        """
        if not name:
            raise ValueError("Cant create Resource if name is None")
        if not resource_type:
            raise ValueError("Cant create Resource if resource_type is None")
        if not scopes and not full_scope:
            raise ValueError("Cant create Resource if scopes is None and full_scope is False")
        
        client_id = self.authz_client.get('id')

        if full_scope:
            scopes = self.keycloak_client.get_client_authz_scopes(client_id)

        resource_struct = {'name': name, 'type': resource_type,
                            'owner': {'id': client_id, 'name': 'testing-realm'},
                            'ownerManagedAccess': False, 'attributes': {},
                            'uris': [], 'scopes': scopes}
        resource_resp = self.keycloak_client.create_client_authz_resource(client_id, 
                                                                          resource_struct,
                                                                          skip_exists=True)
        self.logger.info("%s | CREATED", name)
        resource_id = resource_resp.get("_id")
        if not resource_id:
            raise ValueError("Could not get resource_id after creating it")
        return resource_id

    def get_permission_by_name(self, name:str) -> dict | None:
        """Get a permission from keycloak by its name

        Args:
            name (str): Name of the permission

        Raises:
            ValueError: If name is None

        Returns:
            dict: Returns the permission if found
            None: Returns None if no permission was found
        """
        if not name:
            raise ValueError("Cant get permission by its name if name is None")
        
        client_id = self.authz_client.get('id')
        permissions = self.keycloak_client.get_client_authz_permissions(client_id)
        for permission in permissions:
            if not isinstance(permission, dict):
                continue
            if permission.get('name') == name:
                permission_id = permission.get('id')
                
                scopes = self.keycloak_client.get_client_authz_permission_scopes(client_id,permission_id)
                permission['scopes'] = [scope.get('id') for scope in scopes]
                
                resources = self.keycloak_client.get_client_authz_permission_resources(client_id,permission_id)
                permission['resources'] = [resource.get('_id') for resource in resources]
                
                return permission

    def get_policy_by_name(self, name:str) -> dict | None:
        """Get a policy from keycloak by its name

        Args:
            name (str): Name of the policy

        Raises:
            ValueError: If name is None

        Returns:
            dict: Returns the policy if found
            None: Returns None if no policy was found
        """
        client_id = self.authz_client.get('id')
        if not name:
            raise ValueError("Cant get policy by its name if name is None")
        
        policies = self.keycloak_client.get_client_authz_policies(client_id)
        for policy in policies:
            if not isinstance(policy, dict):
                continue
            if policy.get('name') == name:
                return policy


    def update_group_policy(self, policy_id:str, policy_name:str, 
                            groups:list, extend_children: bool=True , server_state:dict = {}):
        """Updates a Policy based on a Group / multiple Groups

        Args:
            policy_id (str): ID of the policy
            policy_name(str): Name of the policy
            groups (list): Groups which will be evaluated in the policy
            extend_children (bool, optional): if children and their members will be evaluated too

        Raises:
            ValueError: If policy_name is None
        """
        
        if not policy_name:
            raise ValueError("Cant update policy if policy_name is None")
        
        client_id = self.authz_client.get('id')
        if not groups:
            pol_groups = []
        else:
            pol_groups = [{'id': group.keycloak_id, 'extendChildren': extend_children} for group in groups if group]
        pol = {"id": policy_id,
                "name": policy_name,
                "description": "autogenerated",
                "type": "group",
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "groupsClaim": "",
                "groups": pol_groups,
                }
        
        no_change = self.compare_state(server_state, pol, 'policy')
        if no_change:
            self.logger.info("%s | SKIPPED (No change)", policy_name)
            return
        self.keycloak_client.update_client_authz_group_based_policy(client_id, policy_id, pol)
        self.logger.info("%s | UPDATED", policy_name)
        
    def update_role_policy(self, policy_id:str, policy_name:str, roles:list, server_state:dict = {}):
        """Updates a Policy based on a role / multiple role

        Args:
            policy_id (str): ID of the policy
            policy_name(str): Name of the policy
            roles (list): Roles which will be evaluated in the policy

        Raises:
            ValueError: If policy_name is None
        """
        
        if not policy_name:
            raise ValueError("Cant update policy if policy_name is None")

        client_id = self.authz_client.get('id')
        pol_roles = [{'id': role.get('id')} for role in roles if role]
        
        # Delete policy if no roles are defined
        if not pol_roles:
            self.keycloak_client.delete_client_authz_policy(client_id, policy_id)
            self.logger.info("%s | DELETED", policy_name)
            return
        
        pol = { "id": policy_id, 
                "name": policy_name,
                "description": "autogenerated", 
                "type": "role", 
                "logic": "POSITIVE",
                "decisionStrategy": "UNANIMOUS",
                "roles": pol_roles}
        
        no_change = self.compare_state(server_state, pol, 'policy')
        if no_change:
            self.logger.info("%s | SKIPPED (No change)", policy_name)
            return
        self.keycloak_client.update_client_authz_role_based_policy(client_id, policy_id, pol)
        self.logger.info("%s | UPDATED", policy_name)

    def update_permission(self, permission_id:str, perm_name:str,
                          scope_ids:list, resources:list, policies:list, server_state:dict = {}):
        """Updates a Permisson in Keycloak

        Args:
            permission_id (str): ID of the permission
            perm_name (str): Name of the permission
            scope_ids (list): The ids of the scope which will be granted
            resources (list): Resource which the scopes will be granted for
            policies (list): Policies which decide if access is granted

        Raises:
            ValueError: If permission_id is None
            ValueError: If perm_name is None
        """
        if not permission_id:
            raise ValueError("Cant update permission if permission_id is None")
        
        if not perm_name:
            raise ValueError("Cant update permission if perm_name is None")
        
        perm = {
            "id": permission_id,
            "name": perm_name,
            "type": "scope",
            "logic": "POSITIVE",
            "decisionStrategy": "AFFIRMATIVE",
            "resources": resources,
            "scopes": scope_ids,
            "policies": policies,
        }
        
        no_change = self.compare_state(server_state, perm, 'permission')
        if no_change:
            self.logger.info("%s | SKIPPED (No change)", perm_name)
            return       
        client_id = self.authz_client.get('id')
        self.keycloak_client.update_client_authz_scope_permission(client_id=client_id, 
                                                                  scope_id=permission_id,
                                                                  payload=perm)
        self.logger.info("%s | UPDATED", perm_name)

    def remove_policy_by_name(self, policy_name:str):
        """Deletes a policy based on its name

        Args:
            policy_name (str): Name of the policy which will be deleted
        """
        policy = self.get_policy_by_name(policy_name)
        if not policy:
            return
        
        policy_id = policy.get('id')
        client_id = self.authz_client.get('id')
        self.keycloak_client.delete_client_authz_policy(client_id, policy_id)
        self.logger.info("%s | DELETED", policy_name)
    def remove_polices_from_permission(self, permission_name: str):
        """Removes all polices, scopes and resource from a permission

        Args:
            permission_name (str): Name of the permission
        """
        permission = self.get_permission_by_name(permission_name)
        if not permission:
            return
        self.update_permission(permission['id'], permission['name'], [], [], [], permission)
        

    def create_group_to_group_rule(self, group_to_apply: Group, perm_groups: list,
                                   scopes: list, perm_type:str ='management_group',
                                   inherit:bool =True):
        """Creates a policy based on a group and creates a Permissions with said policy 
            and applies it to a specified group

        Args:
            group_to_apply (Group): Group which the Permission will be applied for
            perm_groups (list): Group which will be evaluated in the policy. Defaults to [].
            scopes (list, optional): Scopes which will be granted. Defaults to [].
            perm_type (str, optional): is only used for inheritance and naming. 
            Defaults to "management_group".
            inherit (bool, optional): If True the group will inherit manage_roles from its parent. 
            Defaults to True.

        Raises:
            ValueError: If scopes is None
            ValueError: role_type is None
            ValueError: If Group has no path Attribute
        
        """

        # Basic checks to avoid runtime exceptions
        if not perm_groups and not inherit:
            raise ValueError("Cant create a group to Group rule if perm_groups is None")
        if not scopes:
            raise ValueError("Cant create a group to Group rule if scopes is None")
        if not group_to_apply:
            raise ValueError("Cant create a group to Group if group_to_apply is None")
        
        self.keycloak_client.group_set_permissions(group_to_apply.keycloak_id)

        # Remove old policy if no perm_groups defined to ensure security
        # and to minimize redundant policies
        if not perm_groups and perm_type == 'management_group':
            policy_name = f'[{perm_type}]-access-[{group_to_apply.path}]-policy'
            self.remove_policy_by_name(policy_name)

        authz_scopes = []
        # Get internal keycloak scope id's
        for scope in scopes:
            authz_scope = self.get_scope_by_name(scope)
            if authz_scope:
                authz_scopes.append(authz_scope)

        # Get group and scope id's
        group_id = group_to_apply.keycloak_id
        scope_ids = [authz_scope.get("id") for authz_scope in authz_scopes]

        # check if resource exist
        resource_id = self.get_resource_by_name(f'group.resource.{group_id}', only_return_id=True)

        # if not create resource
        if not resource_id:
            resource_name = f'group.resource.{group_id}'
            resource_id = self.create_resource(resource_name, 'Group', scopes, full_scope=True)
        policy_name = f'[{perm_type}]-access-[{group_to_apply.path}]-policy'
        policy_id = ''

        policies = []
        # If perm_groups is defined add it
        if perm_groups:
            # Check if policy exist
            # -> if True update existing policy
            # -> if False create new policy
            policy = self.get_policy_by_name(policy_name)
            if policy:
                policy_id = policy.get('id')
                self.update_group_policy(policy_id, policy_name, perm_groups, extend_children=True, server_state=policy)

            else:
                policy_id = self.create_group_based_policy(policy_name, perm_groups,
                                                           extend_children=True)

            if policy_id:
                policies.append(policy_id)

        if policy_id or (perm_type == 'management_group' and inherit):
            # if it's a management_group check if the parents have a policy
            # If they do, apply it to its children
            if perm_type == 'management_group' and inherit:
                # Split path into pieces to recreate the path step by step
                # By doing this were going through the different levels of the group tree
                
                if not group_to_apply.path:
                    raise ValueError("Cant inherit from parent if group_to_apply has no path")
                
                path_pieces = group_to_apply.path.split('/')
                group_path = ''
                for path_piece in path_pieces:
                    policy = {}
                    if path_piece:
                        # If policy exist in parent path, then apply it to the child
                        group_path = f'{group_path}/{path_piece}'
                        policy_name = f'[{perm_type}]-access-[{group_path}]-policy'
                        policy = self.get_policy_by_name(policy_name)

                    if policy:
                        policies.append(policy.get('id'))

        # Check if permission exists
        # -> if True update existing permission
        # -> if False create new permission
        client_id = self.authz_client.get('id')
        perm_name = f'[{perm_type}]-access-[{group_to_apply.path}]-perm'
        permission = self.get_permission_by_name(perm_name)
        if permission:
            permission_id = permission.get('id')
            self.update_permission(permission_id, perm_name, scope_ids, [resource_id], policies, permission)
            return
        
        if policies:
            self.create_permission(perm_name, scope_ids, [resource_id], policies)
    

    def compare_state(self, server_state:dict, new_sate:dict, state_type:str) -> bool:
        """compare the state of the Server and the local changes

        Args:
            server_state (dict): a dict containing the server_state
            new_sate (dict): a dict containing the new_state
            state_type (str): defines the input type (policy or permission)

        Raises:
            ValueError: if server_state is None
            ValueError: if new_sate is None
            ValueError: if state_type is None
            TypeError: if server_state is not of type dict
            TypeError: if new_sate is not of type dict
            TypeError: if state_type is not of type str

        Returns:
            bool: True if the state matches. False if not
        """
        if not server_state:
            raise ValueError("Cant compare state if server_state is None")
        if not new_sate:
            raise ValueError("Cant compare state if new_sate is None")
        if not state_type:
            raise ValueError("Cant compare state if state_type is None")
        
        if not isinstance(server_state, dict):
            raise TypeError("Cant compare state if server_state is not of type dict")
        if not isinstance(new_sate, dict):
            raise TypeError("Cant compare state if new_sate is not of type dict")
        if not isinstance(state_type, str):
            raise TypeError("Cant compare state if state_type is not of type str")
        
        
        
        match(state_type):
        
            case("permission"):
                
                
                # Get the assosiated policies
                client_id = self.authz_client.get('id')
                
                policies = self.keycloak_client.get_client_authz_permission_associated_policies(client_id, server_state.get('id'))
                
                
                server_state['policies'] = [policy.get('id') for policy in policies]
                
                for key in new_sate.keys():
                    if key == 'id':
                        continue
                    
                    server_val = server_state.get(key, None)
                    if server_val == None :
                        return False
                    
                    new_val = new_sate.get(key)
                    
                    if isinstance(new_val, list):
                        new_val.sort()
                        
                    if isinstance(server_val, list):
                        server_val.sort()
                    
                    if not server_val == new_val:
                        return False
            
            case("policy"):
                
                for key in new_sate.keys():
                    
                    if key == 'id':
                        continue
                    
                    if key == 'groupsClaim':
                        continue
                    
                    if key == 'groups' or key == 'roles':
                        config = server_state.get('config', None)
                        if not config:
                            return False
                        groups_str = config.get(key, None)
                        if not groups_str:
                            return False
                        server_val = json.loads(groups_str)

                        new_val = new_sate.get(key)
                        
                        if isinstance(new_val, list):
                            if new_val:
                                if isinstance(new_val[0], dict):
                                    new_val.sort(key=lambda x: x['id'], reverse=False)
                                else:    
                                    new_val.sort()
                        
                        if isinstance(server_val, list):
                            if server_val:
                                if isinstance(server_val[0], dict):
                                    server_val.sort(key=lambda x: x['id'], reverse=False)
                                else:
                                    server_val.sort()
                        
                        if key == 'roles':
                            role: dict
                            for role in server_val:
                                role.pop('required', None)
                        
                        if not server_val == new_val:
                            return False
                        continue
                    
                    
                    server_val = server_state.get(key, None)
                    if server_val == None:
                        return False
                    
                    new_val = new_sate.get(key)
                    
                    if isinstance(new_val, list):
                        new_val.sort()
                        
                    if isinstance(server_val, list):
                        server_val.sort()
                    
                    if not server_val == new_val:
                        return False
            case _:
                return False
        return True
         
        
