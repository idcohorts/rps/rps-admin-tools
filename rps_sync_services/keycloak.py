import json
from typing import Any
from keycloak import KeycloakAdmin
from .data_model import Group, User, Attributes, Role
from .bridge_base import Bridge
from keycloak.exceptions import KeycloakGetError
class KeycloakBridge(Bridge):

    def __init__(self, config: dict|None = None):

        # Init BridgeBase
        super(KeycloakBridge, self).__init__(config)
        
        self.service_name = "Keycloak"
        
        keycloak_url: str = self.config['KEYCLOAK_URL']
        keycloak_realm: str = self.config['KEYCLOAK_REALM']
        keycloak_client_id: str = self.config['KEYCLOAK_CLIENT_ID']
        keycloak_secret: str = self.config['KEYCLOAK_SECRET']
        self.keycloak_required_actions: list = json.loads(self.config.get('KEYCLOAK_REQUIRED_ACTIONS','[]'))
        
        # Used for caching
        self.use_cache = self.config.get('USE_CACHE', 'True').lower() in ('true', '1')
        self.user_cache = []
        self.group_cache = []
        
        # Init keycloak client
        self.keycloak_client = KeycloakAdmin(server_url=keycloak_url,
                                        client_id=keycloak_client_id,
                                        realm_name=keycloak_realm,
                                        client_secret_key=keycloak_secret)
        self.server_version = None
        server_info = self.keycloak_client.get_server_info()
        system_info = server_info.get('systemInfo', {})
        if isinstance(system_info, dict):
            self.server_version = system_info.get('version')
            
        self.logger.info("Initialized Bridge and established connection with Keycloak server")

        if server_info:
            self.logger.info("Keycloak server version is : %s" ,self.server_version)
        else:
            self.logger.info("Could not retrieve Keycloak server version")

    ### Cache Operations ###
    
    def group_cache_append(self, group: Group):
        """Append a group to the group_cache

        Args:
            group (Group): The Group which will be added to the cache
        """
        
        if not self.use_cache:
            return
        
        cached_groups = [cached_group for cached_group in self.group_cache 
                         if cached_group.path == group.path]
        if cached_groups:
            self.group_cache.remove(cached_groups[0])
        self.group_cache.append(group)
        
    def user_cache_append(self, user: User):
        """Append a user to the user_cache

        Args:
            user (User): The User which will be added to the cache
        """
        
        if not self.use_cache:
            return
        
        cached_users = [cached_user for cached_user in self.user_cache 
                        if cached_user.username == user.username]
        if cached_users:
            self.user_cache.remove(cached_users[0])
        self.user_cache.append(user)
        
    
    def clear_cache(self, only_groups = False):
        """Clears the user/group cache"""
        if not only_groups:
            self.user_cache = []
        self.group_cache = []
    
    ### Bridge Overrides ###

    def get_groups(self, ignore_members=False, ignore_children=False):
        self.keycloak_client.connection.refresh_token()
        kc_groups:list[dict] | dict = self.keycloak_client.get_groups({"max": -1, "briefRepresentation": False})
        groups = []
        
        if not isinstance(kc_groups, list):
            raise TypeError("Could not get Keycloak groups")
        kc_group: dict[str,Any]
        for kc_group in kc_groups:
            members = []
            
            if kc_group['name'] in self.ignore_groups:

                self.logger.info("skipping group %s and all its childgroups", kc_group['name'])
                continue
            
            if kc_group['path'] in self.ignore_groups:

                self.logger.info("skipping group %s and all its childgroups", kc_group['name'])
                continue
            
            
            if 'attributes' in kc_group.keys():
                if 'sync_ignore' in kc_group['attributes']:
                    if isinstance(kc_group['attributes']['sync_ignore'],list):
                        if kc_group['attributes']['sync_ignore'][0].lower() == 'true':
                            self.logger.info("skipping group %s and all children",
                                                kc_group['name'])
                            continue
                    else:
                        if kc_group['attributes']['sync_ignore'].lower() == 'true':
                            self.logger.info("skipping group %s and all children",
                                                kc_group['name'])
                            continue
            
            if not ignore_children:
                group = self.__generate_group_tree_from_dict(kc_group, ignore_members)
                if group:
                    groups.append(group)
                continue

            if not ignore_members:
                members = self.get_members_from_group(kc_group['id'])

            attributes: Attributes | None = None
            if isinstance(attributes, dict):
                attributes = Attributes(kc_group.get('attributes'))

            group = Group(kc_group["name"], members, kc_group['id'], 
                            attributes, [], kc_group['path'])

            groups.append(group)
            
        for group in groups:
            self.group_cache_append(group)
        
        return self.flatten_groups(groups)


    def check_group(self, group: Group, use_cache=True):
        if group:
            try:

                if not isinstance(group, Group):
                    raise TypeError("group can only be type Group")

                if group.path:
                    return self.check_group_path_exist(group.path)

                return self.check_group_name_exist(group.group_name, use_cache)
            except KeycloakGetError as e:
                if e.response_code == 404:
                    self.logger.info('Group %s does not exist', group)
                    return False
                else: 
                    raise(e)

            
    def create_group(self, group: Group, use_cache=True):
        if not group:
            return None
        
        self.keycloak_client.connection.refresh_token()
        group_name = group.group_name
        # Get parent_id which is needed for child Groups
        # If there is no path defined then just create the group as 'root' group
        parent = None
        if group.path:
            if group.path.count("/") > 1:
                parent_path = group.path.replace(f'/{group_name}', '')
                parent = self.get_group_by_path(parent_path, use_cache)
            
                if not parent:
                    raise ValueError(f"Could not get parent, with path {parent_path}, from Keycloak")
            
        
        if parent:
            parent_id = parent.keycloak_id
            group_id = self.keycloak_client.create_group({'name': group_name},
                                                            parent_id, skip_exists=True)
        else:
            group_id = self.keycloak_client.create_group({'name': group_name},
                                                            skip_exists=True)

        # Return server version of Group with the keycloak_id
        server_group = self.get_group_by_id(group_id, use_cache=False)
        if server_group:
            self.group_cache_append(server_group)
        return server_group
        
    def remove_group(self, group : Group):
        
        if not group:
            raise ValueError('group can not be None')

        if not isinstance(group, Group):
            raise TypeError("Can only delete type Group")

        group_id = group.keycloak_id
        self.keycloak_client.delete_group(group_id)

    def sync_group(self, group, create_if_not_found=True):

        if not group:
            raise ValueError('group can not be None')
        
        self.logger.info("Syncing group: %s",group)
        
        if not isinstance(group, Group):
            raise TypeError("Can only sync group of type Group")

        server_group = None
        if group.path:
            server_group = self.get_group_by_path(group.path)

        if group.keycloak_id and not server_group:
            server_group = self.get_group_by_id(group.keycloak_id)
            
        if not server_group and create_if_not_found:
            self.logger.info("No Group found on server... creating new one")
            server_group = self.create_group(group)
            if server_group:
                group.keycloak_id = server_group.keycloak_id
            else:
                return None

        self.keycloak_client.connection.refresh_token()

        attributes = None
        if group.attributes:
            attributes = group.attributes.to_dict()
            
        if server_group:
            kc_group = {"name": group.group_name,
                        "id": server_group.keycloak_id,
                        "attributes": attributes}
            
            self.keycloak_client.update_group(server_group.keycloak_id, kc_group)

            diff = server_group.compare_members(group)

            for member in diff["Add"]:
                user = self.get_user_by_name(member)
                if user:
                    self.keycloak_client.group_user_add(user.keycloak_id,
                                                        server_group.keycloak_id)

            for member in diff["Delete"]:
                user = self.get_user_by_name(member)
                if user:
                    self.keycloak_client.group_user_remove(user.keycloak_id,
                                                            server_group.keycloak_id)

            return self.get_group_by_id(server_group.keycloak_id)
        
    def get_users(self, ignore_roles=False, ignore_groups=True):
        
        self.keycloak_client.connection.refresh_token()

        kc_users = self.keycloak_client.get_users({"briefRepresentation": False})

        return self.transform_kc_users_to_users(kc_users, ignore_groups, ignore_roles)
    
    def check_user(self, user: User):
        if user:
            try:
                if not isinstance(user, User):
                    raise TypeError("user can only be type User")
                query = {"email": user.email, "username": user.username, "exact": True}

                users = self.keycloak_client.get_users(query)
                if users:
                    return True
                return False
            except Exception as e:
                self.logger.exception(e)
                return False
    
    def create_user(self, user: User):
        
        if not user:
            raise ValueError('user can not be None')
        
        if not isinstance(user,User):
            raise TypeError("Can only create user from User class")

        kc_payload = user.to_keycloak_dict(ignore_groups=True, ignore_roles=False)

        # Remove useless Keys
        kc_payload.pop("id")

        kc_user_id = self.keycloak_client.create_user(kc_payload)
        new_user = self.get_user_by_id(kc_user_id)
        return new_user
        
    def remove_user(self, user):
        
        if not user:
            raise ValueError('user can not be None')
        
        if not isinstance(user,User):
            raise TypeError("Can only delete users of type User")

        self.keycloak_client.connection.refresh_token()
        if not user.keycloak_id:
            
            user = self.get_user_by_name(user.username)
            
            if not user:
                return

        self.keycloak_client.delete_user(user.keycloak_id)
    
    def sync_user(self, user: User, ignore_roles=True, ignore_groups=False, use_cache = True):
        """Synchronises User object with Keycloak

        :param ignore_roles: Ignore the users roles.
        :param ignore_groups: Ignore the users groups.
        :returns: Returns the synchronised version from the Keycloak-Server
        """
        if not user:
            return None

        self.logger.info("Syncing user: %s", user)
        
        self.keycloak_client.connection.refresh_token()

        if not isinstance(user, User):
            raise TypeError("Can only sync user of type User")

        server_user: User | None = None


        server_user = self.get_user_by_name(user.username)
        if server_user:
            user.keycloak_id = server_user.keycloak_id


        if not server_user:
            server_user = self.create_user(user)

            if server_user:
                user.keycloak_id = server_user.keycloak_id
            else:
                raise RuntimeError(f"User not found and could not create new one User: {user}")
            # Add default required actions
            for required_action in self.keycloak_required_actions:
                user.add_required_action(required_action)

        
        if server_user:

            if not ignore_roles and isinstance(user.roles, list):
                server_roles = self.get_user_roles(user)
                
                roles_to_remove = list(set(server_roles) - set(user.roles))
                roles_to_add = list(set(user.roles) - set(server_roles))
                self.logger.debug(user.roles)
                self.logger.debug(roles_to_add)
                self.logger.debug(roles_to_remove)
                if roles_to_add:
                    self.assign_roles(server_user, roles_to_add)
                if roles_to_remove:
                    self.__remove_roles(server_user, roles_to_remove)

            if not ignore_groups and isinstance(user.groups, list):
                server_groups = self.get_user_groups(user, as_string_list=True)
                
                groups_to_add = set(user.groups) - set(server_groups)
                self.logger.debug(user.groups)
                self.logger.debug(groups_to_add)
                for group_name in groups_to_add:
                    self.logger.debug("adding %s to %s ",user.username ,group_name)
                    group = self.get_group_by_name(group_name, use_cache=use_cache, ignore_children=True)
                    if group:
                        self.keycloak_client.group_user_add(user_id=user.keycloak_id, 
                                                            group_id=group.keycloak_id)

                groups_to_remove = set(server_groups) - set(user.groups)
                self.logger.debug(server_groups)
                self.logger.debug(groups_to_remove)
                for group_name in groups_to_remove:
                    self.logger.debug("removing %s from group %s", user.username, group_name)
                    group = self.get_group_by_name(group_name, use_cache=use_cache, ignore_children=True)
                    if group:
                        self.keycloak_client.group_user_remove(user_id=user.keycloak_id, 
                                                               group_id=group.keycloak_id)

            is_synced = self.__update_user(user, ignore_groups, ignore_roles)
            if not is_synced:
                raise RuntimeError(f"Could not update User : {user}")
            if server_user.keycloak_id:
                server_user = self.get_user_by_id(server_user.keycloak_id, 
                                                  ignore_groups, ignore_roles)

        return server_user
    
    ### Additional methods ###
    
    def check_group_name_exist(self, group_name:str, use_cache = True):
        """Check if a Groupname is present in Keycloak

        Args:
            group_name (str): the name of the group

        Returns:
            bool: True if found / False if not
        """
        if not group_name:
            raise ValueError('group_name can not be None')
        
        if not isinstance(group_name, str):
            raise TypeError("groupname can only be type str")
        
        group = self.get_group_by_name(group_name,ignore_children=True, ignore_members=True, use_cache=use_cache)
        
        if group:
            return True
        
        return False

    def check_group_path_exist(self, group_path="", use_cache = True):
        """Check if a Group-path is present in Keycloak

        Args:
            group_path (str): the path of the group

        Returns:
            bool: True if found / False if not
        """
        if not group_path:
            raise ValueError('group_path can not be None')
        if not isinstance(group_path,str):
            raise TypeError("groupname can only be type str")
        group = self.get_group_by_path(group_path, use_cache)
        
        if group:
            return True
        
        return False

    def check_email_exists(self, email: str):
        """Check if a user with a specific email exist

        Args:
            email (str): Email which will be searched for

        Returns:
            bool: True if found/ False if not
        """
        
        if not email:
            raise ValueError('email can not be None')
        

        if not isinstance(email,str):
            raise TypeError("email can only be type str")
        query = {"email": email, "exact": True}

        users = self.keycloak_client.get_users(query)
        
        if not isinstance(users, list):
            return False
        
        if not users:
            return False
        
        if not isinstance(users[0],dict):
            return False
        
        emails: list = [user.get('email') for user in users if user.get('email')]
        
        if email in emails:
            return True

        return False

    


    def get_token(self) -> None | str:
        """This method is used to get the Keycloak bearer token of the service account

        Returns:
            str: Bearer token
        """

        self.keycloak_client.connection.refresh_token()
        return self.keycloak_client.connection.token

    def __generate_group_tree_from_dict(self, kc_group, ignore_members=False, ignore_permissions= False) -> Group | None:
        if not kc_group:
            return None
        
        members = []
        if not ignore_members:
            members = self.get_members_from_group(kc_group['id'])

        attributes = Attributes(kc_group.get('attributes'))

        children = []
        if not kc_group.get('subGroups'):
            if kc_group.get('subGroupCount', 0) > 0:
                kc_group['subGroups'] = self.keycloak_client.get_group_children(kc_group['id'])
            
            else:
                kc_group['subGroups'] = []
            
        for kc_child in kc_group.get("subGroups"):
            child_group = self.__generate_group_tree_from_dict(kc_child, ignore_members)
            if child_group:
                children.append(child_group)
                
        group = Group(kc_group["name"], members, kc_group['id'], 
                        attributes, children, kc_group.get("path"))
        
        if ignore_permissions:
            return group
        
        permissions = self.get_permission_types_from_group(group)
        group.set_permissions(permissions)
        return group

        
    
        
    def recursive_flatten(self, group: Group) -> list[Group]:
        flat_groups = []
        flat_groups.append(group)
        if group.child_groups:         
            for group in group.child_groups:
                flat_groups.extend(self.recursive_flatten(group))
        

        return flat_groups
        
    def flatten_groups(self, groups: list[Group]) -> list[Group]:
        flat_groups = []
        for group in groups:
            flat_groups.extend(self.recursive_flatten(group))
        return flat_groups
            

    def group_tree_to_list(self, group) -> list[Group] | None:
        groups = []
        if group.child_groups is None:
            self.logger.debug("no children found for group %s ",group.group_name)
            return None
        for child in group.child_groups:
            groups.append(child)
            self.logger.debug("child %s added to group %s",child.group_name, group.group_name)
            children_of_child = self.group_tree_to_list(child)
            if children_of_child is None:
                self.logger.debug("no children found for group %s ",child.group_name)
                continue

            groups.extend(children_of_child)
        return groups

    # TODO : make attribute keys configurable
    # Return users and their permission types in a dict (mainly used in sync scripts)
    def get_permission_types_from_group(self, group: Group, use_cache = True):
        """Returns a dict with user sorted by their permission specific to this group.

        return example::

          perm_dict ={
         'member_groups': [Group object, ...],
         'management_groups': [Group object, ...],
         'guest_roles': [role_name],
         'member_roles': [role_name],
         'management_roles': [role_name]
        }

        :param group: The Group object, which will be used to get the corresponding user permission
        :returns: Returns a dict with user sorted by their permission specific to this group.


        """
        perm_dict = {'member_groups': [],
                     'management_groups': [],
                     'guest_groups': [],
                     'guest_roles': [],
                     'member_roles': [],
                     'management_roles': []

                     }
        perm_dict['member_groups'].append(group)

        if not group:
            raise ValueError("group can't be of type None")

        if not group.child_groups:
            # Get group with children
            
            kc_group = self.keycloak_client.get_group(group.keycloak_id)

            if kc_group.get('subGroupCount', 0) > 0:
                group_with_children = self.get_group_by_path(group.path, use_cache)

                if group_with_children is None:

                    raise ValueError("Could not get Group and children from Keycloak")
                
                group = group_with_children


        if group.child_groups:
            children = self.group_tree_to_list(group)
            
            if children:
                perm_dict['guest_groups'].extend(children)
        if group.attributes:
            if 'guest_roles' in group.attributes.keys():
                guest_roles: list | None = group.attributes.guest_roles

                if isinstance(guest_roles,list):
                    for guest_role in guest_roles:
                        # Check if exist in Keycloak
                        role = self.get_role_by_name(guest_role)
                        if role:
                            perm_dict['guest_roles'].append(guest_role)
                elif isinstance(guest_roles,str):
                    # Check if exist in Keycloak
                    role = self.get_role_by_name(guest_roles)
                    if role:
                        perm_dict['guest_roles'].append(guest_roles)

            if 'manage_roles' in group.attributes.keys():
                manage_roles = group.attributes.manage_roles
                if isinstance(manage_roles, list):
                    for manage_role in manage_roles:
                        # Check if exist in Keycloak
                        role = self.get_role_by_name(manage_role)
                        if role:
                            perm_dict['management_roles'].append(manage_role)
                elif isinstance(manage_roles, str):
                    # Check if exist in Keycloak
                    role = self.get_role_by_name(manage_roles)
                    if role:
                        perm_dict['management_roles'].append(manage_roles)

            if 'member_roles' in group.attributes.keys():
                member_roles = group.attributes.member_roles
                if isinstance(member_roles, list):
                    for member_role in member_roles:
                        # Check if exist in Keycloak
                        role = self.get_role_by_name(member_role)
                        if role:
                            perm_dict['member_roles'].append(member_role)
                elif isinstance(member_roles,str):
                    # Check if exist in Keycloak
                    role = self.get_role_by_name(member_roles)
                    if role:
                        perm_dict['member_roles'].append(member_roles)

            if 'managed_by' in group.attributes.keys():
                management_groups = group.attributes.managed_by
                if not isinstance(management_groups,list):
                    management_groups = [management_groups]
                    
                for management_group_name in management_groups:
                    management_group = None
                    if management_group_name:
                        if management_group_name[:1] == "/":
                            management_group = self.get_group_by_path(path=management_group_name, ignore_children=True, 
                                                                      ignore_members=True, use_cache=use_cache)
                        else:
                            management_group = self.get_group_by_name(name=management_group_name, parent=group,
                                                                      ignore_members=True, use_cache=use_cache)
                    if management_group:
                        perm_dict['management_groups'].append(management_group)            
            
            inherit = False  # Determines if the group inherits managed_by and manage_roles
            if group.path:
                if 'inherit' in group.attributes.keys():
                    inherit_attr: str | None = group.attributes.inherit
                    
                    if inherit_attr:
                        inherit = inherit_attr.lower() == 'true'
                        
                else:
                    if group.path.count('/') > 1:
                        inherit = True

            if group.path and inherit:
                # Split path into pieces to recreate the path step by step
                path_pieces = group.path.split('/')
                group_path = ''
                for path_piece in path_pieces:
                    if path_piece:
                        # If attribute exist in parent path, then add group to management
                        group_path = f'{group_path}/{path_piece}'
                        if group_path == group.path:
                            continue
                        parent_group = self.get_group_by_path(path=group_path, ignore_children=True, ignore_members=True, use_cache=use_cache)

                        
                        # inherit manage_roles
                        if parent_group and parent_group.attributes:
                            if 'manage_roles' in parent_group.attributes.keys():
                                manage_roles = parent_group.attributes.manage_roles
                                if isinstance(manage_roles, list):
                                    for manage_role in manage_roles:
                                        role = self.get_role_by_name(manage_role)
                                        if role:
                                            perm_dict['management_roles'].append(manage_role)
                                if isinstance(manage_roles, str):
                                    role = self.get_role_by_name(manage_roles)
                                    if role:
                                        perm_dict['management_roles'].append(manage_roles)

                            if 'managed_by' in parent_group.attributes.keys():
                                management_groups = parent_group.attributes.managed_by
                                if not isinstance(management_groups, list):
                                    management_groups = [management_groups]
                                    # Check if exist in Keycloak
                                for management_group_name in management_groups:
                                    management_group = None
                                    if management_group_name:
                                        if management_group_name[:1] == "/":
                                            management_group = self.get_group_by_path(path=management_group_name, ignore_children=True, ignore_members=True, use_cache=use_cache, ignore_permissions=True)
                                        else:
                                            management_group = self.get_group_by_name(name=management_group_name, parent=parent_group, ignore_members=True, use_cache=use_cache, ignore_permissions=True)
                                    if management_group:
                                        perm_dict['management_groups'].append(management_group)

        return perm_dict

    def get_group_by_path(self, path, ignore_children = False, ignore_members = False, use_cache = True, ignore_permissions=False) -> Group | None:
        """Get a group by its Keycloak group path

        Args:
            path (str): path of the group. Example: /Path/to/My/Group
            ignore_children (bool, optional): Dont return the children (faster). Defaults to False.
            ignore_members (bool, optional): Dont return the members (faster). Defaults to False.
            use_cache (bool, optional): Use the builtin group_cache. Defaults to True.

        Returns:
            Group | None: Return the Group if found. None if not
        """
        
        if not path:
            raise ValueError('path can not be None')
        
        try:
            
            if use_cache:
                cached_groups = [group for group in self.group_cache if group.path == path]
                if cached_groups:
                    return cached_groups[0]
                
            # Temporary Fix
            kc_group_brief = self.keycloak_client.get_group_by_path(path)
            kc_group:dict = self.keycloak_client.get_group(kc_group_brief['id'],full_hierarchy=True)
            
            if kc_group:
                if not ignore_children:
                        group = self.__generate_group_tree_from_dict(kc_group, ignore_members, ignore_permissions)
                else:
                    
                    members = []
                    if not ignore_members:
                        members = self.get_members_from_group(kc_group['id'])
                    
                    attributes_dict = kc_group.get('attributes')
                    attributes = None
                    if isinstance(attributes_dict, dict):
                        attributes = Attributes(attributes_dict)
                    group = Group(kc_group["name"], members, kc_group['id'], 
                                  attributes, [], kc_group.get("path"))
                
                if group:
                    if not ignore_children and not ignore_members:
                        self.group_cache_append(group)
                return group
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Group %s does not exist', path)
                return None
            else: 
                raise(e)




    def send_verify_email(self, user: User):
        """Send a verify email to User via Keycloak

        Args:
            user (User): user which will receive the E-Mail

        Returns:
            bool: True if succesfull / False if not
        """
        if not user:
            raise ValueError('user can not be None')
        
        self.keycloak_client.send_verify_email(user.keycloak_id)
        return True

    def __update_user(self, user, ignore_groups=False, ignore_roles=True, use_cache = True):
        
        if not user:
            raise ValueError('user can not be None')


        if not isinstance(user, User):
            raise TypeError("Can only update user of type User")

        keycloak_dict = user.to_keycloak_dict(ignore_groups, ignore_roles)
        if not ignore_groups and user.groups:
            for group in keycloak_dict["groups"]:
                if not self.check_group_name_exist(group, use_cache):
                    keycloak_dict["groups"].remove(group)

        self.logger.debug(keycloak_dict)
        self.keycloak_client.update_user(user.keycloak_id, keycloak_dict)
        return True
    
    def get_members_from_group(self, group_id: str, ignore_sub_group_members=True) -> list:
        """Get the mebers of a Group

        Args:
            group_id (str): Id of the Keycloak Group
            ignore_sub_group_members (bool, optional): Dont include members of child_groups (faster). Defaults to True

        Returns:
            list: List of Users with all of the Group Members
        """
        
        if not group_id:
            raise ValueError('group_id cant be None or empty')
        
        self.keycloak_client.connection.refresh_token()

        kc_group_members = self.keycloak_client.get_group_members(group_id)

        if not kc_group_members or not isinstance(kc_group_members, list):
            return []
        
        
        group_members = []
        
        kc_user : dict
        for kc_user in kc_group_members:
            
            if self.check_ignore_user(kc_user["username"]):
                self.logger.info("Ignoring user %s", kc_user["username"])
                continue
            
            user = User(kc_user["username"], kc_user.get("email"), 
                        kc_user["id"], kc_user.get("firstName"),
                        kc_user.get("lastName"), Attributes(kc_user.get('attributes')),
                        kc_user.get("createdTimestamp"), bool(kc_user.get("enabled")), 
                        bool(kc_user.get("emailVerified")))
            user.set_required_actions(kc_user.get("requiredActions"))
            group_members.append(user)
        if not ignore_sub_group_members:
            
            # Get the Group to get child_groups
            group: Group | None = None
            group = self.get_group_by_id(group_id)
            
            if not group:
                return group_members
            
            if group.child_groups:
                child_group : Group
                for child_group in group.child_groups:
                    child_members = self.get_members_from_group(str(child_group.keycloak_id),
                                                                False)
                    for member in child_members:
                        if member not in group_members:
                            group_members.append(member)
        return group_members
    
    def __get_roles_by_name(self, roles: list=[]):
        
        if not roles:
            raise ValueError('roles can not be None')
        
        if not isinstance(roles, list):
            raise TypeError("Can only assign role of type list[str]")
        if not isinstance(roles[0], str):
            raise TypeError("Can only assign role of type list[str]")
        keycloak_roles = []
        for role in roles:
            keycloak_role = self.get_role_by_name(role)
            keycloak_roles.append(keycloak_role)
        return keycloak_roles

    def get_role_by_name(self, role:str):
        if not role:
            raise ValueError('role can not be None')
        
        try:
            if not isinstance(role, str):
                raise TypeError("role name can only be of type str")

            keycloak_role = self.keycloak_client.get_realm_role(role)
            return keycloak_role
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Role %s does not exist', role)
            else:
                raise(e)
            return None

    def assign_roles(self, user: User, roles: list):
        """Assign a role to a User

        Args:
            user (User): User the role should be assigned to
            roles (list): role to assign

        Raises:
            TypeError: Roles should be a list
            TypeError: Role in list has to be str
            TypeError: User should be type User

        Returns:
            bool: True if succesfull / False if not
        """
        
        if not user:
            raise ValueError('User cant be None')

        if not roles:
            raise ValueError('roles cant be None')

        if not isinstance(roles,list):
            raise TypeError("Can only assign roles of type list[str]")

        if not isinstance(roles[0],str):
            raise TypeError("Can only assign roles of type list[str]")

        if not isinstance(user,User):
            raise TypeError("Can only assign role to user of type User")

        keycloak_roles = self.__get_roles_by_name(roles)

        self.keycloak_client.assign_realm_roles(user.keycloak_id, keycloak_roles)

    def __remove_roles(self, user: User, roles: list):
        if not user:
            raise ValueError('User cant be None')

        if not roles:
            raise ValueError('roles cant be None')

        if not isinstance(roles,list):
            raise TypeError("Can only remove roles of type list[str]")

        if not isinstance(roles[0],str):
            raise TypeError("Can only remove roles of type list[str]")

        if not isinstance(user,User):
            raise TypeError("Can only remove role to user of type User")

        keycloak_roles = []

        for role in roles:
            keycloak_role = self.keycloak_client.get_realm_role(role)
            keycloak_roles.append(keycloak_role)

        self.keycloak_client.delete_realm_roles_of_user(user.keycloak_id, keycloak_roles)

        return True



    

    def get_user_by_name(self, username, ignore_groups=True, ignore_roles=True, use_cache = True):
        try:
            
            
            if use_cache:
                cached_users = [user for user in self.user_cache if user.username is username]
                if cached_users:
                    return cached_users[0]
            
            keycloak_id = self.keycloak_client.get_user_id(username)
            
            if not keycloak_id:
                return None
            
            
            user = self.get_user_by_id(keycloak_id, ignore_groups, ignore_roles)
            if user:
                self.user_cache_append(user)
            return user
        
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('User %s does not exist', username)
            else: 
                raise(e)
            return None

    def get_users_by_attribute(self, attribute_name, attribute_value, ignore_roles=True, ignore_groups=True):
        if not attribute_name or not attribute_value:
            return []
        try:
            kc_users = self.get_all_users(ignore_roles, ignore_groups)
            users = []
            kc_user: dict
            for kc_user in kc_users:
                self.logger.debug(f"checking user {kc_user.username}")
                if kc_user.attributes:
                    user_attributes_dict = kc_user.attributes.to_dict()
                    if attribute_name in user_attributes_dict.keys():
                        if attribute_value in user_attributes_dict[attribute_name]:
                            self.logger.debug(f"attribute {attribute_name} has value {attribute_value} for user {kc_user.username}")
                            users.append(kc_user)
            return users
        except Exception as e:
            self.logger.debug(e)
            return []


    def get_user_by_id(self, keycloak_id: str, ignore_groups=True, ignore_roles=True, use_cache = True):
        try:
            if use_cache:
                cached_users = [user for user in self.user_cache if user.keycloak_id is keycloak_id]
                if cached_users:
                    return cached_users[0]
            
            kc_user = self.keycloak_client.get_user(keycloak_id)
            if not kc_user:
                raise ValueError(f"Could not get user with id {keycloak_id} from keycloak")
            self.logger.debug("KC-User: %s", kc_user)
            
            user_attributes = kc_user.get('attributes')
            
            if user_attributes and isinstance(user_attributes,dict):
                user_attributes = Attributes(user_attributes)
            else:
                user_attributes = None
            
            enabled = bool(kc_user.get("enabled"))
            
            user = User(kc_user["username"], kc_user.get("email"), 
                        kc_user["id"], kc_user.get("firstName"),
                        kc_user.get("lastName"), user_attributes, kc_user.get("createdTimestamp"),
                        enabled, bool(kc_user.get("emailVerified")))
            user.set_required_actions(kc_user.get("requiredActions"))
            if not ignore_groups:
                user.groups = self.get_user_groups(user, as_string_list=True)
            if not ignore_roles:
                user.roles = self.get_user_roles(user)
            
            self.user_cache_append(user)
            return user
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('User %s does not exist', keycloak_id)
            else: 
                raise(e)
            return None
        
    def get_all_users(self, ignore_roles=False, ignore_groups=True):
        try:
            self.keycloak_client.connection.refresh_token()

            kc_users = self.keycloak_client.get_users({"briefRepresentation": False})

            return self.transform_kc_users_to_users(kc_users, ignore_groups, ignore_roles)

        except Exception as e:
            raise(e)


    def get_user_roles(self, user: User) -> list:
        """Get all roles of a specific user

        Args:
            user (User): The User which roles will be returned

        Returns:
            list: A list of roles
        """

        kc_roles = self.keycloak_client.get_realm_roles_of_user(user.keycloak_id)
        
        
        if not kc_roles or not isinstance(kc_roles, list):
            return []
        
        roles = [kc_role["name"] for kc_role in kc_roles if kc_roles]
        return roles
    
    def get_user_role(self, user) -> list | None:
        """Get the role of a specific user

        Args:
            user (User): User obj

        Returns:          
            list: Roles which the user has.
        """
        try:
            kc_roles = self.keycloak_client.get_realm_roles_of_user(user.keycloak_id)
            
            if kc_roles and type(kc_roles) is list:
                roles = [kc_role["name"] for kc_role in kc_roles if kc_roles]
                return roles
            
        except:
            return None
        pass

    def get_users_by_role(self, role_name:str, ignore_groups=True, ignore_roles=True) -> list | None:
        """Get all users with a certain role

        Args:
            role_name (str): Name of the role
            ignore_groups (bool, optional): Dont return the users with their groups (faster). Defaults to True.
            ignore_roles (bool, optional): Dont return the users with their roles (faster). Defaults to True.

        Returns:
            _type_: _description_
        """
        if not role_name:
            return []

        try:
            kc_users = self.keycloak_client.get_realm_role_members(role_name)
            self.logger.debug("KC-Users: %s", kc_users)
            return self.transform_kc_users_to_users(kc_users, ignore_groups, ignore_roles)

        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Role %s does not exist', role_name)
                return None
            else:
                raise(e)

    # TODO: this is the convert to user function it just has to be restructured
    def transform_kc_users_to_users(self, kc_users, ignore_groups=True, ignore_roles=True):
        users = []
        for kc_user in kc_users:
        
                
            if self.check_ignore_user(kc_user['username']):
                
                self.logger.info("skipping user %s", kc_user['username'])
                continue
            
            
            user = User(kc_user["username"], kc_user.get("email"), 
                        kc_user["id"], kc_user.get("firstName"),
                        kc_user.get("lastName"), Attributes(kc_user.get('attributes')),
                        kc_user.get("createdTimestamp"), kc_user.get("enabled"), 
                        kc_user.get("emailVerified"))
            user.set_required_actions(kc_user.get("requiredActions"))
            if not ignore_groups:
                groups = self.get_user_groups(user, as_string_list=True)
                user.groups = groups
            if not ignore_roles:
                user_roles = self.get_user_roles(user)
                user.set_roles(user_roles)
            self.user_cache_append(user)
            users.append(user)
        return users

    def get_approved_users(self):
        """This is a helper function for the RPS-Admin-Interface

        Returns:
            list: Users which have the approved role.
        """
        users = self.get_users_by_role("approved")
        return users

    def get_new_users(self):
        """This is a helper function for the RPS-Admin-Interface

        Returns:
            list: Users which has not the approved role.
        """
        path = self.config.get("NEW_USERS_GROUP", "/NewUsers")
        new_users_group = self.get_group_by_path(path, ignore_children=True, use_cache=False)
        
        if not new_users_group:
            return []
        
        users = new_users_group.members

        non_approved_users = []
        
        if not users:
            return non_approved_users
        
        for user in users:
            if "VERIFY_EMAIL" in user.required_actions:
                continue
            user.roles = self.get_user_roles(user)
            if not user:
                continue
            if not user.roles:
                non_approved_users.append(user)
                self.logger.debug("found non-approved user %s", user.username)
                continue
            if 'approved' not in user.roles:
                non_approved_users.append(user)
                self.logger.debug("found non-approved user %s with existing roles",user.username)

        if not non_approved_users:
            print("no non-approved users found")
            return []

        print("found %s non-approved users", len(non_approved_users))

        return non_approved_users


    def get_group_by_name(self, name:str, parent:Group | None = None, ignore_members=False, ignore_children = False, use_cache = True, ignore_permissions=False) -> Group | None:
        """Get a Keycloak group by its name and parent

        Args:
            name (str): _description_
            parent (Group, optional): The parent of the Group.If this is none it is asumed that the Group has no parent. Defaults to None.
            ignore_members (bool, optional): Dont return the members (faster). Defaults to False.
            ignore_children (bool, optional): Dont return the child_groups (faster). Defaults to False.
            use_cache (bool, optional): Use the builtin group_cache. Defaults to True.

        Raises:
            TypeError: Parent group has to be type Group

        Returns:
            Group: The Keycloak group
            None: If no group was found
        """
        if not name:
            return None
        
        try:
            path = ""
            if parent:
                if type(parent) is not Group:
                    raise TypeError("Parent can only be type Group")
                path = f"{parent.path}"

            path = path + f"/{name}"
            
            kc_group = self.get_group_by_path(path, ignore_children= ignore_children, 
                                              ignore_members=ignore_members, use_cache = use_cache, ignore_permissions = ignore_permissions)
            return kc_group
            
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Group %s does not exist', name)
            else: 
                raise(e)
            return None

    def get_group_by_id(self, id, ignore_children = False, use_cache = True) -> Group | None:
        """Get a Keycloak group by its keycloak_id

        Args:
            id (str): the keycloak_id of the group. Format: ########-####-####-####-############
            ignore_children (bool, optional): Dont return the child_groups (faster). Defaults to False.

        Returns:
            Group: The Keycloak Group with the specified id.
            None: If no group was found.
        """
        try:
            if use_cache:
                cached_groups = [group for group in self.group_cache if group.keycloak_id is id]
                if cached_groups:
                    return cached_groups[0]
            
            kc_group: dict = self.keycloak_client.get_group(id)
            if not ignore_children:
                kc_group['subGroups']  = self.keycloak_client.get_group_children(id)     
            
            group = self.__generate_group_tree_from_dict(kc_group, ignore_members=False)
            if group:
                if not ignore_children:
                    self.group_cache.append(group)
            return group
        
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Group %s does not exist', id)
            else: 
                raise e
            

    def get_user_groups(self, user, as_string_list=False) -> list:
        """Get the groups of a user

        Args:
            user (User): User which has to be a member of the Groups
            as_string_list (bool, optional): Return a list of str. Defaults to False.

        Raises:
            TypeError: user has to be type User

        Returns:
            list: List of Groups/str
        """
        if not user:
            raise ValueError('User can not be none')
        if not isinstance(user, User):
            raise TypeError("can only get groups of user with type User")
        self.keycloak_client.connection.refresh_token()
        user_id = user.keycloak_id
        kc_groups = self.keycloak_client.get_user_groups(user_id, False)
        
        # if keycloak groups is empty but HTTP RESPONSE was 200
        # then there are no groups for the user
        if not kc_groups or not isinstance(kc_groups, list):
            return []
        
        groups = []
        kc_group: dict
        for kc_group in kc_groups:
            if as_string_list:
                groups.append(kc_group.get("path"))
                continue
            attributes = Attributes(kc_group.get('attributes'))

            group = Group(kc_group["name"], [], kc_group['id'], attributes, [], kc_group["path"])

            groups.append(group)
        return groups

    def get_role(self, role):
        try:
            self.keycloak_client.connection.refresh_token()
            kc_role:dict = self.keycloak_client.get_realm_role(role)
            
            if not isinstance(kc_role, dict):
                return None
            
            members = self.get_users_by_role(role)
            kc_role['members'] = members
                
            return self.convert_to_role(kc_role)
        
        except KeycloakGetError as e:
            if e.response_code == 404:
                self.logger.info('Role %s does not exist', role)
                return None
            else: 
                raise e

    def get_roles(self):
        
        self.keycloak_client.connection.refresh_token()
        kc_roles = self.keycloak_client.get_realm_roles()
        
        if not kc_roles:
            return []
        
        roles = []
        for kc_role in kc_roles:
            
            if not isinstance(kc_role, dict):
                continue
            
            rolename = kc_role.get('name', '')
            if not rolename:
                continue
    
            members= self.get_users_by_role(rolename)
            kc_role['members'] = members
            
            role = self.convert_to_role(kc_role)
            
            if role:
                roles.append(role)
            
        return roles

    def convert_to_role(self, role: dict) -> Role | None:
        rolename = role.get('name', None)
        
        if not rolename:
            return None
        
        _id = role.get('id', None)
        
        members = role.get('members', None)
        
        return Role(rolename,members,_id)
        