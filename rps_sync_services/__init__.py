from .data_model import Group, User
from .bridge_base import Bridge
from .keycloak import KeycloakBridge
from .nextcloud import NextcloudBridge
from .matrix import MatrixBridge
from .discourse import DiscourseBridge
from .keycloak_event import KeycloakEventBridge
from .permissions import PermissionBridge