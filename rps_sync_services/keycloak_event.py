import json
from pathlib import Path
from .keycloak import KeycloakBridge
from .bridge_base import Bridge
from .data_model import Group, User, Role
import time
from datetime import datetime
class KeycloakEventBridge(KeycloakBridge):

    def __init__(self, config: dict | None = None):
        super().__init__(config)
        if not self.config:
            raise ValueError("Config can not be None")
        
        # Get time of initalization
        self.init_time:int = int(time.time() * 1000)  
        
        timestamp_path = self.config.get("TIMESTAMP_PATH","./timestamp.json")
        self.file_path = Path(timestamp_path)
        self.check_file()
        self.timestamp = self.get_timestamp()

    
    def __del__(self):
        self.logger.debug("Bridge destructor is called saving timestamp...")
        save = self.config.get("EVENTS_SAVE_TIMESTAMP", "true").lower() == "true"
        if not save:
            self.logger.debug("Timestamp-saving is turned off in the env")
            
        self.save_timestamp(self.init_time)
        
    
    
    def check_file(self):
        """Check if timestamp file exists
        """
        if self.file_path.is_file():
            return
        
        self.file_path.touch()
        self.file_path.write_text('{"timestamp": 0}', encoding="UTF-8")
        
    def save_timestamp(self, timestamp: int) -> None:
        """Saves the timestamp to file

        Args:
            timestamp (int): timestamp in Unix time
        """
        timestamp_dict = {"timestamp":timestamp}
        json_dump = json.dumps(timestamp_dict)
        self.file_path.write_text(json_dump, encoding="UTF-8")
    
    def get_timestamp(self) -> int:
        """Retrieves unix timestamp

        Returns:
            int: timestamp
        """
        content = self.file_path.read_text(encoding="UTF-8")
        last_sync:dict = json.loads(content)
        timestamp = last_sync.get("timestamp", 0)
        return timestamp

    def get_timestamp_date(self) -> str:
        """Gets the date of the saved timestamp

        Returns:
            str: Date in YYYY-MM-DD Format
        """

        date = datetime.fromtimestamp(int(self.timestamp/1000))
        return date.strftime("%Y-%m-%d")


    def get_users(self, ignore_roles=False, ignore_groups=True):
        query= {
            "resourceTypes":["USER"],
            "operationTypes": ["CREATE","UPDATE"],
            "first":0,
            "max": -1,
            "dateFrom": self.get_timestamp_date()
        }
        events:list = self.get_events(query)
        event:dict
        users_to_sync:list[User] = []
        user_ids:list[str] = []
        for event in events:
            if not isinstance(event, dict):
                continue
            str_repres:str = event.get('representation', None)
            if not str_repres:
                raise ValueError("""Event representation was None.
                                 Maybe include representation is turned off in realm settings?""")
                
            repres:dict = json.loads(str_repres)
            if not repres:
                continue
            
            if repres.get('id','') in user_ids:
                continue
            
            
            if repres.get('username','') in self.ignore_users:
                print(f"skipping {repres['username']}")
                if repres.get('id',None):
                    user_ids.append(repres['id'])
                continue
            
            keycloak_id = repres.get('id', None)
            if not keycloak_id:
                keycloak_id = event.get('resourcePath','').replace('users/', '')
            user = None
            if keycloak_id:
                user: User | None = self.get_user_by_id(keycloak_id=keycloak_id,
                                                        ignore_groups=ignore_groups, 
                                                        ignore_roles=ignore_roles)
            if user:
                if user.username in self.ignore_users:
                    continue
                users_to_sync.append(user)
        return users_to_sync
    
    
    def get_roles(self):
        query= {
            "resourceTypes":["REALM_ROLE_MAPPING"],
            "operationTypes": ["CREATE","DELETE","UPDATE"],
            "first":0,
            "max": -1,
            "dateFrom": self.get_timestamp_date()
        }
        events:list = self.get_events(query)
        event:dict
        roles_to_sync:list[Role] = []
        role_names:list[str] = []
        for event in events:
            str_repres:str = event.get('representation', None)
            if not str_repres:
                raise ValueError("""Event representation was None.
                                 Maybe include representation is turned off in realm settings?""")

            repres:dict = json.loads(str_repres)
            if isinstance(repres, list):
                if repres:
                    repres = repres[0]  
            if not isinstance(repres, dict):
                continue
            if repres.get('name','') in role_names:
                continue
            
            role: Role | None = self.get_role(role=repres['name'])
            if role:
                role_names.append(repres['name'])
                roles_to_sync.append(role)
        return roles_to_sync
    
    def deduplicate_groups(self, groups: list[Group]) -> list[Group] :
        
        
        added_group_paths = []
        deduplicated_groups = []
        
        for group in groups:
            if group.path not in added_group_paths:
                added_group_paths.append(group.path)
                deduplicated_groups.append(group)
            continue
            
        
        return deduplicated_groups
    

    def get_groups(self, ignore_members=False, ignore_children=False):
        query= {
            "resourceTypes":["GROUP","GROUP_MEMBERSHIP"],
            "operationTypes": ["CREATE","DELETE","UPDATE"],
            "first":0,
            "max": -1,
            "dateFrom": self.get_timestamp_date()
        }
        events:list = self.get_events(query)
        event:dict
        groups_to_sync:list[Group] = []
        group_ids:list[str] = []
        events.reverse()
        for event in events:
            if not isinstance(event, dict):
                continue
            str_repres:str = event.get('representation', '{}')
            repres:dict = json.loads(str_repres)
            if not repres:
                continue
            if repres['id'] in group_ids:
                continue
            
            if repres.get('path','') in self.ignore_groups:
                print(f"skipping {repres['path']}")
                group_ids.append(repres['id'])
                continue
            
            if repres.get('name','') in self.ignore_groups:
                print(f"skipping {repres['name']}")
                group_ids.append(repres['id'])
                continue

            
            group: Group | None = self.get_group_by_id(repres['id'], ignore_children=ignore_children)
            if group:
                group_ids.append(repres['id'])
                if group.group_name in self.ignore_groups:
                    continue
                if group.path in self.ignore_groups:
                    continue
                
                if group.attributes:
                    if 'sync_ignore' in group.attributes.keys():
                        if group.attributes.sync_ignore:
                            if group.attributes.sync_ignore.lower() == 'true':
                                continue
                
                groups_to_sync.append(group)
                
                # Get the parent
                if not group.path:
                    continue
                if not group.path.count('/') > 1:
                    continue
                
                path_pieces = group.path.split('/')
                group_path = ''
                parents = []
                for path_piece in path_pieces:
                    if path_piece:
                        group_path = f'{group_path}/{path_piece}'
                    if not group_path:
                        continue
                    parent = self.get_group_by_path(group_path, ignore_children=ignore_children)
                    if not parent:
                        continue
                    
                    if parent.path in self.ignore_groups:
                        continue
                    
                    if parent.group_name in self.ignore_groups:
                        continue
                    
                    if not parent.keycloak_id:
                        continue
                    
                    if parent.keycloak_id in group_ids:
                        continue
                    
                    parents.append(parent)
                        
                        
                # Reverse so the children will get synced first. 
                # Keep in mind that parents should already be created due to the event being called first.
                # This is only used to adjust the permission for a parent if a child is created.
                parents.reverse()
                groups_to_sync.extend(parents)

        deduplicated_groups = self.deduplicate_groups(groups_to_sync)
        return deduplicated_groups
        # if last_timestamp > 0:
        #     timestamp = last_timestamp
        #     last_sync['timestamp'] = last_timestamp
        #     json_dump = json.dumps(last_sync)
        #     file_path.write_text(json_dump)
            
            
    def get_events(self, query: dict) -> list:
        """Get events from Keycloak and filter them by their timestamp

        Args:
            query (dict): query used by keycloak api to query events

        Returns:
            list: A list of filtered Events
        """
        
        events:list = self.get_admin_events(query)
        filtered_events:list = []
        for event in events:
            event_time:int = event.get('time', 0)
            # Filter events by their timestamp
            if event_time <= self.timestamp:
                continue
                
            filtered_events.append(event)
        return filtered_events
            
                
            
        
    def get_admin_events(self, query: dict) -> list:
        """Get events directly from Keycloak admin event api

        Args:
            query (dict): query used by keycloak api to query events

        Returns:
            list: Events
        """
        events = self.keycloak_client.get_admin_events(query)
        
        if not isinstance(events, list):
            return []
        
        return events
    
    
    def sync_with_bridge(self, bridge: 'Bridge',cleanup = True) -> None:
        raise NotImplementedError("""This Bridge is intended for a one-way sync
                                  (use the KeycloakBridge instead)""")        

    def sync_users_with_bridge(self, bridge: 'Bridge',cleanup = True) -> None:
        raise NotImplementedError("""This Bridge is intended for a one-way sync
                                  (use the KeycloakBridge instead)""")    
     
    def sync_groups_with_bridge(self, bridge: 'Bridge',cleanup = True) -> None:
        raise NotImplementedError("""This Bridge is intended for a one-way sync
                                  (use the KeycloakBridge instead)""")        
        
    def sync_roles_with_bridge(self, bridge: 'Bridge',cleanup = True)-> None:
        raise NotImplementedError("""This Bridge is intended for a one-way sync
                                  (use the KeycloakBridge instead)""")         
        