from typing import Self, TypeVar

# Note once we upgraded to Python 3.11 we will use the Self type from Python typing
TGroup = TypeVar("TGroup", bound="Group")


class User:
    """ This is the class User class which is used to ensure compatability between the Bridges
        Example: User("test.user","test.user@test-domain.com","c2e69578-...")"""
    def __init__(self, username, email, keycloak_id=None, first_name=None, last_name=None, 
                 attributes=None, timestamp=None, enabled=True, email_verified=True):
        self.username: str = username
        self.email: str = email
        self.email_verified: bool = email_verified
        self.keycloak_id: str | None = keycloak_id
        self.first_name: str | None = first_name
        self.last_name: str | None = last_name
        self.roles: list[str] = []
        self.timestamp: str | None = timestamp
        self.groups: list[str] = []
        self.enabled: bool = enabled
        self.required_actions: list = []

        if attributes:
            if type(attributes) is not Attributes:
                raise TypeError("attributes can only be of type Attributes")
            self.attributes = attributes
        else:
            self.attributes = Attributes()


    # make print() and str() readable
    def __str__(self):
        return str({"username": self.username,
                    "email": self.email,
                    "email_verified": self.email_verified,
                    "keycloak_id": self.keycloak_id,
                    "first_name": self.first_name,
                    "last_name": self.last_name,
                    "attributes": self.attributes,
                    "groups": self.groups,
                    "roles": self.roles,
                    "enabled": self.enabled, })

    def __repr__(self):
        return "User({})".format(self.username)

    def __eq__(self, other):
        if type(other) is not User:
            raise TypeError("can only compare the User class with a User class")
        return self.keycloak_id == other.keycloak_id and self.username == other.username

    def __iter__(self):
        yield "username", self.username
        yield "email", self.email
        yield "email_verified", self.email_verified
        yield "keycloak_id", self.keycloak_id
        yield "first_name", self.first_name
        yield "last_name", self.last_name
        yield "roles", self.roles
        yield "groups", self.groups
        yield "attributes", self.attributes.to_dict() if self.attributes else []
        yield "required_actions", self.required_actions if self.required_actions else []
        yield "enabled", self.enabled

    def add_required_action(self, action):
        if action in self.required_actions:
            return
        self.required_actions.append(action)

    def remove_required_action(self, action):
        if action in self.required_actions:
            self.required_actions.remove(action)

    def set_required_actions(self, actions):
        if actions:
            self.required_actions = actions

    def clear_required_actions(self):
        self.required_actions = []

    def add_role(self, role):
        self.roles.append(role)

    def remove_role(self, role):
        if role in self.roles:
            self.roles.remove(role)

    def set_roles(self, roles):
        self.roles = roles
        
    def set_groups(self, groups):
        self.groups = groups

    def add_group(self, group):
        if not group:
            return
        if type(group) is str:
            self.groups.append(group)
        elif type(group) is Group:
            if group.path: 
                self.groups.append(group.path)

    def remove_group(self, group):
        group_name: str = ""

        if type(group) is str:
            group_name = group

        elif type(group) is Group:
            group_name = group.group_name

        if group_name in self.groups:
            self.groups.remove(group_name)

    def disable(self):
        self.enabled = False

    def enable(self):
        self.enabled = True

    def to_keycloak_dict(self, ignore_groups=False, ignore_roles=True):
        kc_dict: dict = dict(self)

        # Remove useless Keys
        kc_dict["lastName"] = kc_dict.pop("last_name")
        kc_dict["firstName"] = kc_dict.pop("first_name")
        kc_dict["enabled"] = kc_dict.pop("enabled")
        kc_dict["emailVerified"] = kc_dict.pop("email_verified")
        kc_dict["id"] = kc_dict.pop("keycloak_id")
        if self.required_actions:
            kc_dict["requiredActions"] = kc_dict.pop("required_actions")
        else:
            kc_dict.pop("required_actions")

        if ignore_roles:
            kc_dict.pop("roles")
        else:
            kc_dict["realmRoles"] = kc_dict.pop("roles")
        if ignore_groups or not self.groups:
            kc_dict.pop("groups")
        else:
            groups = []
            for group in self.groups:
                if type(group) is str:
                    groups.append(group)
                elif type(group) is Group:
                    groups.append(group.group_name)
            kc_dict["groups"] = groups

        return kc_dict


class Group:
    """This is the class Group class which is used to ensure compatability between the Bridges
       Example: Group("test_group",[User(First Member)],"94514bf9-...", Attributes(...) )"""
    def __init__(self, group_name, members: list[User] = [], keycloak_id=None, attributes=None,
                 child_groups: list[TGroup] = [], path = None):
        if attributes:
            if type(attributes) is not Attributes:
                raise TypeError("attributes can only be of type Attributes")
            
        if type(members) is not list:
            raise TypeError("members of group must be a list of Users")
        
        if len(members) > 0:
            if type(members[0]) is not User:
                raise TypeError("members of group must be a list of Users")

        if type(child_groups) is not list:
            raise TypeError("child_groups of group must be a list of Groups")
        
        if len(child_groups) > 0:
            if type(child_groups[0]) is not Group:
                raise TypeError("child_groups of group must be a list of Groups")

        self.path: str | None = path
        self.child_groups: list[TGroup] = child_groups

        self.keycloak_id: str | None = keycloak_id
        self.group_name: str = group_name
        self.members: list[User] = members
        self.permissions: dict = {}

        self.attributes: Attributes | None = attributes
        
        if not attributes:
            self.attributes = Attributes()
            self.membership_source = None
            self.group_hierarchy_type = None
            return
        
        if attributes.membership_source:
            self.membership_source = attributes.membership_source
        else:
            self.membership_source = None

        if attributes.group_hierarchy_type:
            self.group_hierarchy_type = attributes.group_hierarchy_type
        else:
            self.group_hierarchy_type = None

    # make print() and str() readable
    def __str__(self):
        return str({"keycloak_id": self.keycloak_id,
                    "group_name": self.group_name,
                    "members": [str(member) for member in self.members],
                    "attributes": str(self.attributes)})

    def __repr__(self):
        return "Group({})".format(self.group_name)

    def __iter__(self):
        yield "keycloak_id", self.keycloak_id
        yield "group_name", self.group_name
        yield "members", [dict(member) for member in self.members]

        if self.attributes:
            yield "attributes", self.attributes.to_dict()

    def to_keycloak_dict(self, ignore_members):
        kc_dict: dict = dict(self)
        kc_dict["id"] = kc_dict.pop("keycloak_id")
        kc_dict["name"] = kc_dict.pop("group_name")
        if ignore_members:
            kc_dict.pop("members")

        return kc_dict

    def set_permissions(self, permissions: dict):
        if permissions:
            self.permissions = permissions
    
    
    def get_permissions(self):
        if self.permissions:
            return self.permissions
    
    def get_membership_source(self):
        # Return membership source if not given then accounts
        if self.membership_source:
            return self.membership_source
        else:
            return "accounts"

    def _get_children(self) -> list:
        # From list of Group Objects:
        # return group's children Group Objects
        if self.child_groups:
            return self.child_groups
        else:
            return []

    def get_members(self, username_id_pair=True) -> dict | list:
        # Get all members from the group
        if username_id_pair:
            # Return Username and ID pair (dict)
            return {member.username: member.keycloak_id for member in self.members}
        # Return members in User Object
        return self.members

    def set_members(self, members):
        if members is not None:
            self.members = members
        return self

    def add_member(self, member):
        if member:
            self.members.append(member)
        return self

    def compare_members(self, other_group: Self):
        if not isinstance(other_group, Group):
            raise TypeError("Can only compare Group class with Group class")
        group_members = self.get_members(username_id_pair=True)
        other_members = other_group.get_members(username_id_pair=True)
        members_to_add = {k: v for k, v in other_members.items() if k not in group_members}
        members_to_remove = {k: v for k, v in group_members.items() if k not in other_members}
        return {
            "Add": members_to_add,
            "Delete": members_to_remove
        }

    def __and__(self, other):
        if type(other) is not Group:
            raise TypeError("Can only compare a Group class with a Group class")

        if len(self.members) <= 0:
            return Group("New intersect group", [])

        if len(other.members) <= 0:
            return Group("New intersect group", [])

        intersect = []

        for member in self.members:
            if member in other.members:
                intersect.append(member)

        return Group("New intersect group", intersect)

    def __or__(self, other):
        if type(other) is not Group:
            raise TypeError("Can only compare a Group class with a Group class")

        if len(self.members) <= 0:
            return Group("New union Group", other.members)

        if len(other.members) <= 0:
            return Group("New union Group", self.members)

        union = []
        union.extend(self.members)

        for member in other.members:
            if member not in union:
                union.append(member)

        return Group("New union Group", union)

class Role:
    """This class represents a role. This role class is inspired by Keycloaks roleRepresentation
        Docs : https://www.keycloak.org/docs-api/21.1.1/rest-api/#_rolerepresentation
    """
    def __init__(self, rolename: str, members: list[User] | None = None, id:str | None = None):
        if not isinstance(rolename, str):
            raise TypeError("rolename can only be of type str")
        
        self.name = rolename
        
        if members:
            if not isinstance(members,list):
                raise TypeError("members can only be of type list[User]")
            self.members = members
        else:
            self.members = []
            
        if id:
            if not isinstance(id,str):
                raise TypeError("id can only be of type str")
            
        self.id = id

    def __repr__(self):
        return "Role({})".format(self.name)
        

class Attributes:
    def __init__(self, attributes: dict | None = None):
        # Check needed if all required extra attributes are available
        if not attributes:
            return None
        for key in attributes:
            attribute = attributes[key]
            if type(attribute) is list and len(attribute) == 1:
                attribute = attribute[0]
            self.__dict__[key] = attribute

    def keys(self):
        return self.__dict__.keys()

    def __contains__(self, item):
        return item in self.__dict__

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self.__dict__)

    def __setattr__(self, key, value):
        self.__dict__[key] = value

    def __getattr__(self, key):
        if key in self.__dict__:
            return self.__dict__[key]
        else:
            return None
        
    def __getitem__ (self, key):
        if key in self.__dict__:
            return self.__dict__[key]
        else:
            return None
        

    def to_dict(self) -> dict:
        self_dict = {}
        for key in self.__dict__:
            if type(self.__dict__[key]) is not list:
                self_dict[key] = [self.__dict__[key]]
                continue
            self_dict[key] = self.__dict__[key]
        return self_dict

    def setdefault(self, key: str, default=None):
        if key in self.__dict__:
            return self.__dict__[key]
        else:
            self.__dict__[key] = default
            return default
