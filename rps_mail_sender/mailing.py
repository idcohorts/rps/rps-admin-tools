import smtplib, os
from os.path import exists, isfile
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import mimetypes
import jinja2 # import Environment, PackageLoader, select_autoescape, FileSystemLoader


from dataclasses import dataclass


@dataclass
class MailServerConfig:
    """Class for keeping track of an item in inventory."""
    host: str = "localhost"
    port: int = 1025
    ssl: bool = False
    start_tls: bool = False
    auth: bool = False
    username: str = None
    password: str = None
    retry_amount: int = 5



def read_text_file(filepath) -> str:
    """ Liest eine Textdatei ein und gibt den Inhalt als String zurück """
    text_file_content = ""
    if isfile(filepath) == True and exists(filepath) == True:
        with open(filepath, encoding='utf-8') as f:
            text_file_content = f.read()
            f.close()
    else:
        print(f"Die Datei {filepath} existiert nicht oder ist keine Datei")
    return text_file_content

    
def send_mail(config,
                from_email = "",          # Absenderadresse
                to_email = [],           # rcp = ["Bernd Franke <be...@..>","Bernd Franke <....>"]
                cc_emails = [],            # Liste der CC ["Bernd Franke <be...@..>","Bernd Franke <....>"]
                bc_emails = [],            # Liste der BCC ["Bernd Franke <be...@..>","Bernd Franke <....>"]
                attached_file = [],    # lstattachfiles = ["D:/../..txt","F:/../../..doc"]
                subject = "",            # Betreff/Thema
                body_file_path = "",
                **kwargs):     # eMail-Body-file als plain Text
    """ Sending an e-mail with attachement """
    
    # prepare email addresses
    if isinstance(to_email, str):
        to_email = to_email
    else:
        to_email = ', '.join(to_email)

    message = MIMEMultipart()                                                   # Instanz der MIMEMultipart
    message['From'] = from_email                                                  # Absender Adresse
    message['To'] = to_email                                          # Empfänger Adress-Liste
    recepients_string = to_email
    if cc_emails:
        message['CC'] = ", ".join(cc_emails)
        recepients_string += ", " + ", ".join(cc_emails)
    if bc_emails:
        message['BCC'] = ", ".join(bc_emails)
        recepients_string += ", " + ", ".join(bc_emails)
    
    message['Subject'] = subject
        
    if "html" in body_file_path:
         # sends an email using a template
        template_folder = os.path.dirname(os.path.abspath(body_file_path))
        file_name = os.path.basename(body_file_path)

        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(template_folder),
            autoescape=jinja2.select_autoescape(['html', 'xml'])
        )
        template = env.get_template(file_name)
        body_text = template.render(**kwargs)
        message.attach(MIMEText(body_text, "html"))
    else:
        body_text = read_text_file(body_file_path)
        message.attach(MIMEText(body_text, 'plain', 'utf-8'))
    
    # raise error if body_text is empty
    if body_text == "":
        print(f"body_text: {body_text}")
        raise Exception(f"Message body is empty")
        
    for attachement in attached_file:
        try:
            """ Anhang einladen """
            if os.stat(attachement):                                            # ist die Datei existent
                file_name = os.path.basename(attachement).split('/')[-1]         # Dateiname eines Anhangs
                # detect mimetype of file
                mime_type, encoding = mimetypes.guess_type(attachement)
                if mime_type is None:
                    mime_type = 'application/octet-stream'
                mime_type = mime_type.split('/', 1)
                print(f"'{file_name} with mimetype {mime_type}' attached")
                attachment = open(attachement, "rb") 
                
                """ Anhang als MIME-Objekt einladen """
                message_part = MIMEBase(mime_type[0],mime_type[1])                   # Instanz der MIMEBase als p
                message_part.set_payload((attachment).read())                      # Anhang als Mime einladen
                encoders.encode_base64(message_part)                               # nach base64 kodieren
                message_part.add_header('Content-Disposition', 'attachment', filename=('utf-8', '', file_name))
                message.attach(message_part)                                           # Anhang als p der msg anhängen
        except:
            raise Exception(f"Anhang {attachement} nicht gefunden")

    # retry sending email if failed
    for i in range(config.retry_amount):            
        try: 
            if config.ssl:                                                          # SSL
                smtp_client = smtplib.SMTP_SSL(config.host, config.port)                      # MTP Session eröffnen mit comline-pars
            else:
                smtp_client = smtplib.SMTP(config.host, config.port)                          # MTP Session eröffnen mit comline-pars
            
            if config.start_tls:                                                     # StartTLS
                smtp_client.starttls()                                                      

            print("authentification")
            if config.auth:                                                         # Authentifikation
                smtp_client.login(config.username, config.password)                           # Authentifikation/Login

            email_message = message.as_string()                                                  # Multipart msg als String

            error_status = smtp_client.sendmail(from_email, recepients_string, email_message)      # Mail senden
            smtp_client.quit()   
            if error_status is  None:                                              # Session beenden
                print(f"Try {i} - Mail sent {error_status}")
                raise Exception(f"Mail not sent {error_status}")      
            else:
                print("Mail sent")
                break
        except Exception as e:
            print(f"Try {i} - Mail not sent {e}")
            if i == config.retry_amount -1:                        # mögliche Fehlermeldung ausgeben
                raise Exception(f"Final Mail not sent {error_status}")
             

        