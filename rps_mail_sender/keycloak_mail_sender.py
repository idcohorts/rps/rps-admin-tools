import time
from . import mailing
from rps_sync_services.keycloak import KeycloakBridge
import os


def strtobool(string:str) -> bool:
    return string.lower() == 'true'

class KeycloakMailSender:
    def __init__(self, dry_run, verbose):
        
        
        self.config = dict(os.environ)
        self.dry_run = dry_run
        self.verbose = verbose
        self.project_id = self.config.get("PROJECT_ID")
        self.mail_subject_rejected = self.config.get("MAIL_SUBJECT_REJECTED")
        self.mail_subject_accepted = self.config.get("MAIL_SUBJECT_ACCEPTED")
        self.mail_from = self.config.get("MAIL_FROM")
        self.mail_login_link = self.config.get("MAIL_LOGIN_LINK")
        self.email_template_accept_file = self.config.get("MAIL_TEMPLATE_ACCEPT_PATH",
                                                          "./rps_mail_sender/templates/emails/default/default-accepted.html")
        self.email_template_reject_file = self.config.get("MAIL_TEMPLATE_REJECT_PATH",
                                                          "./rps_mail_sender/templates/emails/default/default-reject.html")
        self.mail_config = mailing.MailServerConfig(
            host = self.config.get("MAIL_SERVER_CONFIG_HOST"),
            port = self.config.get("MAIL_SERVER_CONFIG_PORT"),
            ssl = strtobool(self.config.get("MAIL_SERVER_CONFIG_SSL")),
            start_tls = strtobool(self.config.get("MAIL_SERVER_CONFIG_STARTTLS")),
            auth = strtobool(self.config.get("MAIL_SERVER_CONFIG_AUTH")),
            username = self.config.get("MAIL_SERVER_CONFIG_USERNAME"),
            password = self.config.get("MAIL_SERVER_CONFIG_PASSWORD")
        )

    def send_accepted_registration_mail(self, user):
        if self.verbose:
            print("[Keycloak Registration-Sync] Send accepted registration mail to {}".format(user.email))
        try:
            mailing.send_mail(
                config=self.mail_config,
                from_email=self.mail_from,
                to_email=user.email,
                subject=f"{self.mail_subject_accepted}",
                body_file_path=self.email_template_accept_file, 
                user=user,
                mail_login_link=self.mail_login_link)
            
            return True
        except Exception as e:
            print("[Keycloak Registration-Sync] Error while sending accepted registration mail to {}".format(user.email))
            if self.verbose:
                print("[Keycloak Registration-Sync] Error: {}".format(e))
            return False
        
    def send_rejected_registration_mail(self, user):
        if self.verbose:
            print("[Keycloak Registration-Sync] Send rejected registration mail to {}".format(user.email))
        try:
            reason_for_decline = getattr(user.attributes,"reason_for_decline", "")
            mailing.send_mail(
                config=self.mail_config,
                from_email=self.mail_from,
                to_email=user.email,
                subject=f"{self.mail_subject_rejected}",
                body_file_path=self.email_template_reject_file, 
                user=user,
                reason_for_decline=reason_for_decline)
            return True
        except Exception as e:
            print("[Keycloak Registration-Sync] Error while sending rejected registration mail to {}".format(user.email))
            if self.verbose:
                print("[Keycloak Registration-Sync] Error: {}".format(e))
            return False
