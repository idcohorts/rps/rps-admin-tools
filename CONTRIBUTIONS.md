We thank all contributors of this project!

# Developers
- Chin Lee (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne)
- Elias Hamacher (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne)
- Markus Katharina Brechtel (Institute for Digital Medicine and Clinical Data Sciences; University Frankfurt)
- Nick Schulze (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne / University Hospital Frankfurt)
- Vasily Tulsky (Institute for Digital Medicine and Clinical Data Sciences; University Frankfurt)
- Sarah Shah (Institute for Digital Medicine and Clinical Data Sciences; University Frankfurt)

# AI Coding Support
The code and documentation was created with the help of AI Coding assistants:
- GitHub Copilot
- OpenAI ChatGPT
